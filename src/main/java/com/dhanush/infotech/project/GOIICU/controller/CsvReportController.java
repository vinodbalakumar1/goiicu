package com.dhanush.infotech.project.GOIICU.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.dhanush.infotech.project.GOIICU.model.CsvReport;
import com.dhanush.infotech.project.GOIICU.repository.CsvReportRepository;

@RestController
@RequestMapping("/api")
public class CsvReportController {
	
	@Autowired
	CsvReportRepository csvReportRepository;
	
	 
	 @CrossOrigin
	 @RequestMapping(value = "getCSVDownload", method = RequestMethod.GET )
	 private void getCSVDownload(@RequestParam("state_code") String state_code,@RequestParam ("district_code") String district_code
			 ,@RequestParam ("block_code") String block_code,@RequestParam ("facility_code") String facility_code,@RequestParam ("facility_type_id") String facility_type_id,@RequestParam("input_year") String input_year,@RequestParam("input_month") String input_month,HttpServletResponse response) throws Exception
	{

		 
		 
      	
		 String state= state_code ;                                                                  	
		String district=district_code.equalsIgnoreCase("ALL") ?"" :district_code;           	
		String block=block_code.equalsIgnoreCase("ALL") ?"" :block_code ;                   	
		String facility=facility_code.equalsIgnoreCase("ALL") ?"" :facility_code ;          	
		String facilityType=facility_type_id.equalsIgnoreCase("ALL") ?"" :facility_type_id ;	 
	    String year=input_year.equalsIgnoreCase("ALL") ?"%" :input_year+"-" ;                          
	    String  month=input_month.equalsIgnoreCase("ALL")?"":"0"+input_month+"-";                       
	    String date=year+""+month;                                                                                                       
	    List<CsvReport> li=null;
	    
	    
	     if(state.length()>0 && district.length()==0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)             
	     {		 		                                                                                                                  
	      li=csvReportRepository.getStateDetails(state,date);                                                                              
	     }                                                                                                                                 
		if(state.length()>0 && district.length()>0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)                                                                                                                   
	     {		 		                                                                                                                  
         	 li=csvReportRepository.getDistrictDetails(state,district,date);		                                                      
         }                                                                                                                                 
         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()==0 && facility.length()==0)               
         {		 		                                                                                                                  
         	 li=csvReportRepository.getBlockDetails(state,district,block,date);                                                           
         }                                                                                                                                 
         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()==0)                
         {                                                                                                                                 
	     	                                                                                                                              
         li=csvReportRepository.getFacilityTypeDetails(state,district,block,facilityType,date);                                            
                                                                                                                                           
         }                                                                                                                                 
         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()>0)                 
         {                                                                                                                                 
         	                                                                                                                              
          li=csvReportRepository.getFacilityDetails(state,district,block,facilityType,facility,date);                                      
		                                                                                                                                       
         }  
         
                                                                                                                            
            String csvFileName = "csvReport.csv";                                                                           
                                                                                                                            
               response.setContentType("text/csv");                                                                         
                                                                                                                            
               String headerKey = "Content-Disposition";                                                                    
               String headerValue = String.format("attachment; filename=\"%s\"",csvFileName);                               
               response.setHeader(headerKey, headerValue);                                                                  
                                                                                                                            
                                                                                                                            
                ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),CsvPreference.STANDARD_PREFERENCE);    
                
                       String[] header = {"registration_number","mctsrchnumber","aadharnumber","admissiondate","admissiontime",
                    		   "patient_name","age","wo","religion","caste","BPLholder","address","contactnumber","live_still","sex","birthweight","deliverydate",
                    		   "deliverytime","NameandsignatureofSN"};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
         
                 csvWriter.writeHeader(header);                          
                                                                         
                 for (CsvReport data : li) {                             
                 	try {                                               
                 	                                                    
                 	                                                    
                     csvWriter.write(data,header);                       
                 	}catch(Exception e)                                 
                 	{                                                   
                 		e.printStackTrace();                            
                 		                                                
                 	}                                                   
                 }                                                       
                                                                         
                 csvWriter.close();
			                                
		
	}

}
