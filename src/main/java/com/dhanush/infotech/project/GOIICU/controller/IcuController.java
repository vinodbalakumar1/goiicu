package com.dhanush.infotech.project.GOIICU.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.repository.LocationMastersRepository;
import com.dhanush.infotech.project.GOIICU.model.LocationMasters;
import com.dhanush.infotech.project.GOIICU.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIICU.model.GoiIcu;
import com.dhanush.infotech.project.GOIICU.model.Laboratory;
import com.dhanush.infotech.project.GOIICU.repository.IcuRepository;
import com.dhanush.infotech.project.GOIICU.repository.LaboratoryRepository;

/**
 * @author Vinod.Panchaparvala
 *
 */

@RestController
@RequestMapping("/api")
@CrossOrigin
public class IcuController {

	@Autowired
	IcuRepository icuRepository;
	
	@Autowired
	LaboratoryRepository laboratoryRepository;
	
	@Autowired
	LocationMastersRepository locationmastersRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "/icu", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> createUser(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("admissionId");
		HashMap<String, Object> status = null;
		if(id.equalsIgnoreCase(""))
		{
			status = insertAdmission(userJsonReq);
		String statu = status.get("data") == "Beneficary Added Successfull" ? "OK" :  "ERROR";
		userMap.put("status",statu);
		userMap.put("data", status.get("data"));
		userMap.put("uniqueId",status.get("unique_id"));
		userMap.put("admissionId", status.get("admissionId"));
		}
		else 
		{
			status = updateAdmission(userJsonReq, Long.valueOf(id));
			String statu = status.get("data") == "Beneficary Updated Successfull" ? "OK" :  "ERROR";
			userMap.put("status",statu);
			userMap.put("data", status.get("data"));
			userMap.put("uniqueId",status.get("unique_id"));
			userMap.put("admissionId", status.get("admissionId"));
		}
		return userMap;
	
	}
	
	
	public HashMap<String, Object> insertAdmission(String userJsonReq) {
		HashMap<String, Object> userMap = new HashMap<>();
		System.out.println("insert");
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String state         = openJson.getString("state");
		String district      = openJson.getString("district");
		String block         = openJson.getString("block");
		String facility      = openJson.getString("facility");
		String facilityType  = openJson.getString("facilityType");
		
		                                          
		org.json.JSONObject admission = openJson.getJSONObject("admission");                                         
		String  mcts_rch_number               = admission.getString("mcts_rch_number");
	//	String  name_of_facility              = admission.getString("name_of_facility");
		String  obs_icu_reg_number            = admission.getString("obs_icu_reg_number");
		String bedNo = admission.getString("bedNo");
	//	String  block_name                    = admission.getString("block_name");
		String  bpl_status                    = admission.getString("bpl_status");
	//	String  district_name                 = admission.getString("district_name");
		String  aadhar_number                 = admission.getString("aadhar_number");
		String  contact_number                = admission.getString("contact_number");                                    
		String  name                          = admission.getString("name");
		String  age                           = admission.getString("age");
		String  w_o                           = admission.getString("w_o");
		String  address                       = admission.getString("address");
		String  patient_contact_no            = admission.getString("patient_contact_no");
		String  caste                         = admission.getString("caste");
		String  religion                      = admission.getString("religion");
		String  admission_date                = admission.getString("admission_date");
		String  admission_time                = admission.getString("admission_time");
		String referredIn 					= admission.getString("referredIn");
		String nameOfFacility1 					= admission.getString("nameOfFacility1");
		String reasonforReferral 					= admission.getString("reasonforReferral");
		String  delivery_date                 = admission.getString("delivery_date");
		String  obstetric_etails_at_admission = admission.getString("obstetric_etails_at_admission");
		String  delivery_time                 = admission.getString("delivery_time"); 
		String provisionalDiagnosis			 = admission.getString("provisionalDiagnosis"); 
		String placeOfDelivery			= 		admission.getString("placeOfDelivery");
		String  gravida                       = admission.getString("gravida");
		String  parity                        = admission.getString("parity");
		String  abortion                      = admission.getString("abortion");
		String  living_children               = admission.getString("living_children");
		String  lmp                           = admission.getString("lmp");
		String  edd                           = admission.getString("edd");
		String  number_of_anc                 = admission.getString("number_of_anc");
		String  gestational_age               = admission.getString("gestational_age");
		String  labour                        = admission.getString("labour");
		String birthDate 					  = admission.getString("birthDate");
		String birthTime					  = admission.getString("birthTime");
		String  presentation                  = admission.getString("presentation");
		String  uterine_tenderness            = admission.getString("uterine_tenderness");
		String  fhr                           = admission.getString("fhr");
		String  if_fhr_yes                    = admission.getString("if_fhr_yes");
		String  amniotic_fluid                = admission.getString("amniotic_fluid");
		String  course_of_labour              = admission.getString("course_of_labour");
		String  type_of_delivery              = admission.getString("type_of_delivery");
		String  aph                           = admission.getString("aph");
		String  pph                           = admission.getString("pph");
		String  p_v_foul_smelling             = admission.getString("p_v_foul_smelling");
		String  indi_for_caesarean            = admission.getString("indi_for_caesarean");
		String  deliv_attended_by             = admission.getString("deliv_attended_by");
		String  antenatal_steroids            = admission.getString("antenatal_steroids");
		String  if_yes                        = admission.getString("if_yes");
		String  no_of_doses                   = admission.getString("no_of_doses");
		String  magnesium_sulphate            = admission.getString("magnesium_sulphate");
		String  if_yes_dose                   = admission.getString("if_yes_dose");
		String  time_of_last_doses            = admission.getString("time_of_last_doses");
		
		   org.json.JSONObject indication = openJson.getJSONObject("indication");
		   
		String  icu_admission                   =     indication.getString("icu_admission"); 
		String  hdu_admission                   =     indication.getString("hdu_admission");
		String  hemorrhage_hdu                  =     indication.getString("hemorrhage_hdu");   
		String  hemorrhage_icu                  =     indication.getString("hemorrhage_icu");   
		String  hemorrhage_others               =     indication.getString("hemorrhage_others");   
		String  hypertensive_disorders_hdu      =     indication.getString("hypertensive_disorders_hdu");   
		String  hypertensive_disorders_icu      =     indication.getString("hypertensive_disorders_icu");   
		String  sepsis_hdu                      =     indication.getString("sepsis_hdu");   
		String  sepsis_icu                      =     indication.getString("sepsis_icu");   
		String  sepsis_others                   =     indication.getString("sepsis_others");   
		String  renal_dysfunction_hdu           =     indication.getString("renal_dysfunction_hdu");   
		String  renal_dysfunction_icu           =     indication.getString("renal_dysfunction_icu");   
		String  jaundice_in_pregnancy_hdu       =     indication.getString("jaundice_in_pregnancy_hdu");   
		String  jaundice_in_pregnancy_icu       =     indication.getString("jaundice_in_pregnancy_icu");   
		String  jaundice_in_pregnancy_others    =     indication.getString("jaundice_in_pregnancy_others");   
		String  coagulation_system_hdu          =     indication.getString("coagulation_system_hdu");   
		String  coagulation_system_icu          =     indication.getString("coagulation_system_icu");   
		String  abnormal_vitals_hdu             =     indication.getString("abnormal_vitals_hdu");   
		String  abnormal_vitals_icu              =    indication.getString("abnormal_vitals_icu");
		String  abg_abnormalities_hdu            =    indication.getString("abg_abnormalities_hdu");
		String  abg_abnormalities_icu            =    indication.getString("abg_abnormalities_icu");
		String  electrolyte_disturbances_hdu     =    indication.getString("electrolyte_disturbances_hdu");
		String  electrolyte_disturbances_icu     =    indication.getString("electrolyte_disturbances_icu");
		String  medical_disorders_hdu            =    indication.getString("medical_disorders_hdu");
		String  medical_disorders_icu           =    indication.getString("medical_disorders_icu");
		String  medical_disorders_others10      =    indication.getString("medical_disorders_others10");
		String  cause_of_admission              =    indication.getString("cause_of_admission");
		                                          
		     org.json.JSONObject history = openJson.getJSONObject("history"); 
		                                          
		String  past_medical_history     =  history.getString("past_medical_history");        
		String  past_surgical_history    =  history.getString("past_surgical_history");        
		String  family_history           =  history.getString("family_history");
		                                 
		   org.json.JSONObject baby = openJson.getJSONObject("baby_information_at_birth");  
		   
		String  outcome_of_birth     =  baby.getString("outcome_of_birth");     
		String  birth_weight         =  baby.getString("birth_weight");     
		String  multiple_births      =  baby.getString("multiple_births");     
		String  if_yes_number        =  baby.getString("if_yes_number");     
		String  sexofbaby            =  baby.getString("sexofbaby");     
		String  resu_required        =  baby.getString("resu_required");     
		String  weeks_of_gestation   =  baby.getString("weeks_of_gestation");     
		String  sncu_admission       =  baby.getString("sncu_admission");     
		String  vitamin_K_given      =  baby.getString("vitamin_K_given");     
		String  b_fed_within_1hour   =  baby.getString("b_fed_within_1hour");
		
		
		   org.json.JSONObject general = openJson.getJSONObject("general_examination");  
		
		
		String  general_condition    =   general.getString("general_condition");
		String  height               =   general.getString("height");
		String  weight               =   general.getString("weight");
		String  bmi                  =   general.getString("bmi");
		String  heart_rate           =   general.getString("heart_rate");
		String  blood_pressure       =   general.getString("blood_pressure");
		String  spo2                 =   general.getString("spo2");
		String  respiratory_rate     =   general.getString("respiratory_rate");
		String  temperature          =   general.getString("temperature");
		String  jvp                  =   general.getString("jvp");
		String  pallor               =   general.getString("pallor");
		String  icterus              =   general.getString("icterus");
		String  cyanosis             =   general.getString("cyanosis");
		String  oedema               =   general.getString("oedema");
		String  g_ex_others          =   general.getString("g_ex_others");
		                                          
		    org.json.JSONObject systemic = openJson.getJSONObject("systemic_examination");
		
		String  respiratory_system                =  systemic.getString("respiratory_system");
		String  cardio_vascular_system            =  systemic.getString("cardio_vascular_system");
		String  central_nervous_system            =  systemic.getString("central_nervous_system");
		String  per_abdomen_examination           =  systemic.getString("per_abdomen_examination");
		String  per_speculum_vaginal              =  systemic.getString("per_speculum_vaginal");
		String  any_others                        =  systemic.getString("any_others");
		String  respiratory_system_text           =  systemic.getString("respiratory_system_text");
		String  cardio_vascular_system_text       =  systemic.getString("cardio_vascular_system_text");
		String  central_nervous_system_text       =  systemic.getString("central_nervous_system_text");
		String  per_abdomen_examination_text      =  systemic.getString("per_abdomen_examination_text");
		String  per_speculum_vaginal_text         =  systemic.getString("per_speculum_vaginal_text");
		                                          
		         org.json.JSONObject orders = openJson.getJSONObject("orders");
		                                          
		String  nutrition              = orders.getString("nutrition");  
		String  investigations         = orders.getString("investigations");  
		String  medications            = orders.getString("medications");  
		String  procedures             = orders.getString("procedures");  
		String  monitoring_schedule    = orders.getString("monitoring_schedule");  
		String  multidiscipl_review    = orders.getString("multidiscipl_review");  
		String  plan_for_next_24_hours = orders.getString("plan_for_next_24_hours");  
		String  doctor_name_and_sign   = orders.getString("doctor_name_and_sign");
		
		           org.json.JSONObject sequentialorgan  = openJson.getJSONObject("sequentialorgan");  
		
		String  respiration         = sequentialorgan.getString("respiration");           
		String  coagulation         = sequentialorgan.getString("coagulation");           
		String  liver               = sequentialorgan.getString("liver");           
		String  cardiovascular      = sequentialorgan.getString("cardiovascular");           
		String  cns                 = sequentialorgan.getString("cns");           
		String  renal               = sequentialorgan.getString("renal");
		
		
		  org.json.JSONObject laboratory = openJson.getJSONObject("laboratory_investigation_sheet"); 
		  
		String  day1_hb                          = laboratory.getString("day1_hb");
		String  day1_total_wbc_count             = laboratory.getString("day1_total_wbc_count");
		String  day1_differential_wbc_count      = laboratory.getString("day1_differential_wbc_count");
		String  day1_platelet_count              = laboratory.getString("day1_platelet_count");
		String  day1_peripheral_smear            = laboratory.getString("day1_peripheral_smear");
		String  day1_bt_ct_crt_                  = laboratory.getString("day1_bt_ct_crt_");
		String  day1_pt_aptt_inr                 = laboratory.getString("day1_pt_aptt_inr");
		String  day1_blood_group_rh_type         = laboratory.getString("day1_blood_group_rh_type");
		String  day1_sickling_test               = laboratory.getString("day1_sickling_test");
		String  day1_random_blood_glucose        = laboratory.getString("day1_random_blood_glucose");
		String  day1_blood_urea_nitrogen         = laboratory.getString("day1_blood_urea_nitrogen");
		String  day1_serum_creatinine            = laboratory.getString("day1_serum_creatinine");
		String  day1_serum_calcium               = laboratory.getString("day1_serum_calcium");
		String  day1_serum_sodium                = laboratory.getString("day1_serum_sodium");
		String  day1_serum_potassium             = laboratory.getString("day1_serum_potassium");
		String  day1_total_protein_serum_albumin = laboratory.getString("day1_total_protein_serum_albumin");
		String  day1_serum_biliorubin            = laboratory.getString("day1_serum_biliorubin");
		String  day1_sgpt_sgpt_ldh               = laboratory.getString("day1_sgpt_sgpt_ldh");
		String  day1_serum_fibrinogen            = laboratory.getString("day1_serum_fibrinogen");
		String  day1_d_dimer                     = laboratory.getString("day1_d_dimer");
		String  day1_urine_rm                    = laboratory.getString("day1_urine_rm");
		String  day1_stool_for_occult_blood      = laboratory.getString("day1_stool_for_occult_blood");
		String  day1_blood_gas_time              = laboratory.getString("day1_blood_gas_time");
		String  day1_blood_gas_fio2              = laboratory.getString("day1_blood_gas_fio2");
		String  day1_blood_gas_ph                = laboratory.getString("day1_blood_gas_ph");
		String  day1_blood_gas_pco2              = laboratory.getString("day1_blood_gas_pco2");
		String  day1_blood_gas_po2               = laboratory.getString("day1_blood_gas_po2");
		String  day1_blood_gas_hco3              = laboratory.getString("day1_blood_gas_hco3");
		String  day1_blood_gas_satn              = laboratory.getString("day1_blood_gas_satn");
		String  day2_hb                          = laboratory.getString("day2_hb");
		String  day2_total_wbc_count             = laboratory.getString("day2_total_wbc_count");
		String  day2_differential_wbc_count      = laboratory.getString("day2_differential_wbc_count");
		String  day2_platelet_count              = laboratory.getString("day2_platelet_count");
		String  day2_peripheral_smear            = laboratory.getString("day2_peripheral_smear");
		String  day2_bt_ct_crt_                  = laboratory.getString("day2_bt_ct_crt_");
		String  day2_pt_aptt_inr                 = laboratory.getString("day2_pt_aptt_inr");
		String  day2_blood_group_rh_type         = laboratory.getString("day2_blood_group_rh_type");
		String  day2_sickling_test               = laboratory.getString("day2_sickling_test");
		String  day2_random_blood_glucose        = laboratory.getString("day2_random_blood_glucose");
		String  day2_blood_urea_nitrogen         = laboratory.getString("day2_blood_urea_nitrogen");
		String  day2_serum_creatinine            = laboratory.getString("day2_serum_creatinine");
		String  day2_serum_calcium               = laboratory.getString("day2_serum_calcium");
		String  day2_serum_sodium                = laboratory.getString("day2_serum_sodium");
		String  day2_serum_potassium             = laboratory.getString("day2_serum_potassium");
		String  day2_total_protein_serum_albumin = laboratory.getString("day2_total_protein_serum_albumin");
		String  day2_serum_biliorubin            = laboratory.getString("day2_serum_biliorubin");
		String  day2_sgpt_sgpt_ldh               = laboratory.getString("day2_sgpt_sgpt_ldh");
		String  day2_serum_fibrinogen            = laboratory.getString("day2_serum_fibrinogen");
		String  day2_d_dimer                     = laboratory.getString("day2_d_dimer");
		String  day2_urine_rm                    = laboratory.getString("day2_urine_rm");
		String  day2_stool_for_occult_blood      = laboratory.getString("day2_stool_for_occult_blood");
		String  day2_blood_gas_time              = laboratory.getString("day2_blood_gas_time");
		String  day2_blood_gas_fio2              = laboratory.getString("day2_blood_gas_fio2");
		String  day2_blood_gas_ph                = laboratory.getString("day2_blood_gas_ph");
		String  day2_blood_gas_pco2              = laboratory.getString("day2_blood_gas_pco2");
		String  day2_blood_gas_po2               = laboratory.getString("day2_blood_gas_po2");
		String  day2_blood_gas_hco3              = laboratory.getString("day2_blood_gas_hco3");
		String  day2_blood_gas_satn              = laboratory.getString("day2_blood_gas_satn");
		String  day3_hb                          = laboratory.getString("day3_hb");
		String  day3_total_wbc_count             = laboratory.getString("day3_total_wbc_count");
		String  day3_differential_wbc_count      = laboratory.getString("day3_differential_wbc_count");
		String  day3_platelet_count              = laboratory.getString("day3_platelet_count");
		String  day3_peripheral_smear            = laboratory.getString("day3_peripheral_smear");
		String  day3_bt_ct_crt_                  = laboratory.getString("day3_bt_ct_crt_");
		String  day3_pt_aptt_inr                 = laboratory.getString("day3_pt_aptt_inr");
		String  day3_blood_group_rh_type         = laboratory.getString("day3_blood_group_rh_type");
		String  day3_sickling_test               = laboratory.getString("day3_sickling_test");
		String  day3_random_blood_glucose        = laboratory.getString("day3_random_blood_glucose");
		String  day3_blood_urea_nitrogen         = laboratory.getString("day3_blood_urea_nitrogen");
		String  day3_serum_creatinine            = laboratory.getString("day3_serum_creatinine");
		String  day3_serum_calcium               = laboratory.getString("day3_serum_calcium");
		String  day3_serum_sodium                = laboratory.getString("day3_serum_sodium");
		String  day3_serum_potassium             = laboratory.getString("day3_serum_potassium");
		String  day3_total_protein_serum_albumin = laboratory.getString("day3_total_protein_serum_albumin");
		String  day3_serum_biliorubin            = laboratory.getString("day3_serum_biliorubin");
		String  day3_sgpt_sgpt_ldh               = laboratory.getString("day3_sgpt_sgpt_ldh");
		String  day3_serum_fibrinogen            = laboratory.getString("day3_serum_fibrinogen");
		String  day3_d_dimer                     = laboratory.getString("day3_d_dimer");
		String  day3_urine_rm                    = laboratory.getString("day3_urine_rm");
		String  day3_stool_for_occult_blood      = laboratory.getString("day3_stool_for_occult_blood");
		String  day3_blood_gas_time              = laboratory.getString("day3_blood_gas_time");
		String  day3_blood_gas_fio2              = laboratory.getString("day3_blood_gas_fio2");
		String  day3_blood_gas_ph                = laboratory.getString("day3_blood_gas_ph");
		String  day3_blood_gas_pco2              = laboratory.getString("day3_blood_gas_pco2");
		String  day3_blood_gas_po2               = laboratory.getString("day3_blood_gas_po2");
		String  day3_blood_gas_hco3              = laboratory.getString("day3_blood_gas_hco3");
		String  day3_blood_gas_satn              = laboratory.getString("day3_blood_gas_satn");
		String  patient_name                     = laboratory.getString("patient_name");
		String  reg_no                           = laboratory.getString("reg_no");
		String  doa                              = laboratory.getString("doa");
		String  doctor_in_charge                 = laboratory.getString("doctor_in_charge");
		String  urine_culture                    = laboratory.getString("urine_culture");
		String  blood_culture                    = laboratory.getString("blood_culture");
		String  cervical_vaginal_swab            = laboratory.getString("cervical_vaginal_swab");
		String  any_other                        = laboratory.getString("any_other");
		String  hiv                              = laboratory.getString("hiv");
		String  hbsag                            = laboratory.getString("hbsag");
		String  vdrl                             = laboratory.getString("vdrl");
		
		String state_name="";
		String district_name="";
		String block_name="";
		String facility_name="";
		String facility_type_name="";
		
		try {
			
		   
		   
				List<LocationMasters> locations=null;
				   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
					for (Iterator<LocationMasters> iterator = locations.iterator(); iterator.hasNext();)
					{
						LocationMasters locationMasters = (LocationMasters) iterator.next();
						 state_name=locationMasters.getState();
						 district_name=locationMasters.getDistrictCode();
						 block_name=locationMasters.getBlock();
						 facility_name=locationMasters.getFacility();
						 facility_type_name=locationMasters.getFacilityType();
						 
						
						
					}
					}catch (Exception e) {
						userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
						userMap.put("data", e.getMessage());
						userMap.put("admissionId","");
						userMap.put("unique_id", "");
						userMap.put("baby", "");
						return userMap;
					}
		
		
		   String uniqueNo = "icu" + System.currentTimeMillis();
		GoiIcu icu = new GoiIcu();
		icu.setBedNo(bedNo);
		icu.setProvisionalDiagnosis(provisionalDiagnosis);
		icu.setReferredIn(referredIn);
		icu.setNameOfFacility1(nameOfFacility1);
		icu.setReasonforReferral(reasonforReferral);
		icu.setPlaceOfDelivery(placeOfDelivery);
		icu.setBirthDate(birthDate);
		icu.setBirthTime(birthTime);
		icu.setUniqueid(uniqueNo);             
		icu.setState(state);                
		icu.setDistrict(district);            
		icu.setBlock(block);                
		icu.setFacility(facility);             
		icu.setFacilityType(facilityType);   
		icu.setStateName       ( state_name);
		icu.setFacilityTypeName    ( facility_type_name);
		icu.setMctsrchnumber       ( mcts_rch_number               );
		icu.setFacilityName      ( facility_name              );
		icu.setObsicuregnumber   ( obs_icu_reg_number            );
		icu.setBlockname          ( block_name                    );
		icu.setBplstatus          ( bpl_status                    );
		icu.setDistrictname       ( district_name                 );
		icu.setAadharnumber       ( aadhar_number                 );
		icu.setContactnumber      ( contact_number                );
		icu.setName               ( name                          );
		icu.setAge                ( age                           );
		icu.setWo                 ( w_o                           );
		icu.setAddress            ( address                       );
		icu.setPatientcontactno   ( patient_contact_no            );
		icu.setCaste              ( caste                         );
		icu.setReligion           ( religion                      );
		icu.setAdmissiondate     ( admission_date                );
		icu.setAdmissiontime      ( admission_time                );
		icu.setDeliverydate      ( delivery_date                 );
		icu.setObstetricdetails   ( obstetric_etails_at_admission );
		icu.setDeliverytime       ( delivery_time                 );
		icu.setGravida            ( gravida                       );
		icu.setParity            ( parity                        );
		icu.setAbortion          ( abortion                      );
		icu.setLivingchildren    ( living_children               );
		icu.setLmp                ( lmp                           );
		icu.setEdd               ( edd                           );
		icu.setNumberofanc        ( number_of_anc                 );
		icu.setGestationalage     ( gestational_age               );
		icu.setLabour             ( labour                        );
		icu.setPresentation       ( presentation                  );
		icu.setUterinetenderness  ( uterine_tenderness            );
		icu.setFhr                ( fhr                           );
		icu.setIffhryes           ( if_fhr_yes                    );
		icu.setAmnioticfluid      ( amniotic_fluid                );
		icu.setCourseoflabour     ( course_of_labour              );
		icu.setTypeofdelivery     ( type_of_delivery              );
		icu.setAph                ( aph                           );
		icu.setPph                ( pph                           );
		icu.setPvfoulsmelling     ( p_v_foul_smelling             );
		icu.setIndiforcaesarean   ( indi_for_caesarean            );
		icu.setDelivattendedby    ( deliv_attended_by             );
		icu.setAntenatalsteroids ( antenatal_steroids            );
		icu.setIfyes             ( if_yes                        );
		icu.setNoofdoses          ( no_of_doses                   );
		icu.setMagnesiumsulphate  ( magnesium_sulphate            );
		icu.setIfyesdose          ( if_yes_dose                   );
		icu.setTimeoflastdoses    ( time_of_last_doses            );
		
		
		
		icu.setIcu_admission                   (icu_admission                   );
		icu.setHdu_admission(hdu_admission);
		icu.setHemorrhage_hdu                  (hemorrhage_hdu                  );
		icu.setHemorrhage_icu                  (hemorrhage_icu                  );
		icu.setHemorrhage_others               (hemorrhage_others               );
		icu.setHypertensive_disorders_hdu      (hypertensive_disorders_hdu      );
		icu.setHypertensive_disorders_icu      (hypertensive_disorders_icu      );
		icu.setSepsis_hdu                      (sepsis_hdu                      );
		icu.setSepsis_icu                      (sepsis_icu                      );
		icu.setSepsis_others                   (sepsis_others                   );
		icu.setRenal_dysfunction_hdu           (renal_dysfunction_hdu           );
		icu.setRenal_dysfunction_icu           (renal_dysfunction_icu          );
		icu.setJaundice_in_pregnancy_hdu       (jaundice_in_pregnancy_hdu       );
		icu.setJaundice_in_pregnancy_icu       (jaundice_in_pregnancy_icu       );
		icu.setJaundice_in_pregnancy_others    (jaundice_in_pregnancy_others    );
		icu.setCoagulation_system_hdu          (coagulation_system_hdu          );
		icu.setCoagulation_system_icu          (coagulation_system_icu          );
		icu.setAbnormal_vitals_hdu             (abnormal_vitals_hdu           );
		icu.setAbnormal_vitals_icu             (abnormal_vitals_icu           );
		icu.setAbg_abnormalities_hdu           (abg_abnormalities_hdu         );
		icu.setAbg_abnormalities_icu           (abg_abnormalities_icu         );
		icu.setElectrolyte_disturbances_hdu    (electrolyte_disturbances_hdu  );
		icu.setElectrolyte_disturbances_icu    (electrolyte_disturbances_icu  );
		icu.setMedical_disorders_hdu           (medical_disorders_hdu         );
		icu.setMedical_disorders_icu           (medical_disorders_icu         );
		icu.setMedical_disorders_others10      (medical_disorders_others10    );
		icu.setCause_of_admission              (cause_of_admission            );
		
		
		
		
		
		
		
		
		
		icu.setCauseofadmission    (cause_of_admission );
		icu.setPastmedicalhistory      (past_medical_history  );
		icu.setPastsurgicalhistory    (past_surgical_history );
		icu.setFamilyhistory          (family_history        );
		icu.setOutcomeofbirth         (outcome_of_birth   );
		icu.setBirthweight            (birth_weight       );
		icu.setMultiplebirths         (multiple_births    );
		icu.setIfyesnumber            (if_yes_number      );
		icu.setSexofbaby              (sexofbaby          );
		icu.setResurequired           (resu_required      );
		icu.setWeeksofgestation       (weeks_of_gestation );
		icu.setSncuadmission          (sncu_admission     );
		icu.setVitaminKgiven          (vitamin_K_given    );
		icu.setBfedwithin1hour        (b_fed_within_1hour );
		icu.setGeneralcondition            ( general_condition );
		icu.setHeight                     ( height            );
		icu.setWeight                     ( weight            );
		icu.setBmi                        ( bmi               );
		icu.setHeartrate                  ( heart_rate        );
		icu.setBloodpressure              ( blood_pressure    );
		icu.setSpo2                       ( spo2              );
		icu.setRespiratoryrate            ( respiratory_rate  );
		icu.setTemperature                ( temperature       );
		icu.setJvp                        ( jvp               );
		icu.setPallor                    ( pallor            );
		icu.setIcterus                    ( icterus           );
		icu.setCyanosis                  ( cyanosis          );
		icu.setOedema                     ( oedema            );
		icu.setGexothers                  ( g_ex_others       );
		icu.setRespiratorysystem          (respiratory_system          );
		icu.setCardiovascularsystem       (cardio_vascular_system      );
		icu.setCentralnervoussystem       (central_nervous_system      );
		icu.setPerabdomenexamination      (per_abdomen_examination     );
		icu.setPerspeculumvaginal	       (per_speculum_vaginal        );
		icu.setAnyothers		          (any_others                  );
		icu.setRespiratorysystemtext      (respiratory_system_text     );
		icu.setCardiosystemtext           (cardio_vascular_system_text );
		icu.setCentralsystemtext          (central_nervous_system_text );
		icu.setPerabdomentext             (per_abdomen_examination_text);
		icu.setPerspeculumtext            (per_speculum_vaginal_text    );
		icu.setNutrition                   (nutrition             );
		icu.setInvestigations              (investigations        );
		icu.setMedications                 (medications           );
		icu.setProcedures                  (procedures            );
		icu.setMonitoringschedule          (monitoring_schedule   );
		icu.setMultidisciplreview          (multidiscipl_review   );
		icu.setPlanfor24hours              (plan_for_next_24_hours);
		icu.setDoctornamesign              (doctor_name_and_sign  );
		icu.setRespiration                 (respiration   );
		icu.setCoagulation                 (coagulation   );
		icu.setLiver                       (liver         );
		icu.setCardiovascular              (cardiovascular);
		icu.setCns                         (cns           );
		icu.setRenal                       (renal         );
		 
		Laboratory  lab = new Laboratory();
		
		lab.setDay1hb                     (  day1_hb                          );
		lab.setDay1totalwbccount          (  day1_total_wbc_count             );
		lab.setDay1wbccount               (  day1_differential_wbc_count      );
		lab.setDay1plateletcount          (  day1_platelet_count              );
		lab.setDay1peripheralsmear        (  day1_peripheral_smear            );
		lab.setDay1btctcrt                (  day1_bt_ct_crt_                  );
		lab.setDay1ptapttinr              (  day1_pt_aptt_inr                 );
		lab.setDay1bloodgrouprhtype       (  day1_blood_group_rh_type         );
		lab.setDay1sicklingtest           (  day1_sickling_test               );
		lab.setDay1randombloodglucose     (  day1_random_blood_glucose        );
		lab.setDay1bloodureanitrogen      (  day1_blood_urea_nitrogen         );
		lab.setDay1serumcreatinine        (  day1_serum_creatinine            );
		lab.setDay1serumcalcium           (  day1_serum_calcium               );
		lab.setDay1serumsodium            (  day1_serum_sodium                );
		lab.setDay1serumpotassium         (  day1_serum_potassium             );
		lab.setDay1serumalbumin           (  day1_total_protein_serum_albumin );
		lab.setDay1serumbiliorubin        (  day1_serum_biliorubin            );
		lab.setDay1sgptsgptldh            (  day1_sgpt_sgpt_ldh               );
		lab.setDay1serumfibrinogen        (  day1_serum_fibrinogen            );
		lab.setDay1ddimer                 (  day1_d_dimer                     );
		lab.setDay1urinerm                (  day1_urine_rm                    );
		lab.setDay1occultblood            (  day1_stool_for_occult_blood      );
		lab.setDay1bloodgastime           (  day1_blood_gas_time              );
		lab.setDay1bloodgasfio2           (  day1_blood_gas_fio2              );
		lab.setDay1bloodgasph             (  day1_blood_gas_ph                );
		lab.setDay1bloodgaspco2           (  day1_blood_gas_pco2              );
		lab.setDay1bloodgaspo2            (  day1_blood_gas_po2               );
		lab.setDay1bloodgashco3           (  day1_blood_gas_hco3              );
		lab.setDay1bloodgassatn           (  day1_blood_gas_satn              );
		lab.setDay2hb                     (  day2_hb                          );
		lab.setDay2totalwbccount          (  day2_total_wbc_count             );
		lab.setDay2wbccount               (  day2_differential_wbc_count      );
		lab.setDay2plateletcount          (  day2_platelet_count              );
		lab.setDay2peripheralsmear        (  day2_peripheral_smear            );
		lab.setDay2btctcrt                (  day2_bt_ct_crt_                  );
		lab.setDay2ptapttinr              (  day2_pt_aptt_inr                 );
		lab.setDay2bloodgrouprhtype       (  day2_blood_group_rh_type         );
		lab.setDay2sicklingtest           (  day2_sickling_test               );
		lab.setDay2randombloodglucose     (  day2_random_blood_glucose        );
		lab.setDay2bloodureanitrogen      (  day2_blood_urea_nitrogen         );
		lab.setDay2serumcreatinine        (  day2_serum_creatinine            );
		lab.setDay2serumcalcium           (  day2_serum_calcium               );
		lab.setDay2serumsodium            (  day2_serum_sodium                );
		lab.setDay2serumpotassium         (  day2_serum_potassium             );
		lab.setDay2serumalbumin           (  day2_total_protein_serum_albumin );
		lab.setDay2serumbiliorubin        (  day2_serum_biliorubin            );
		lab.setDay2sgptsgptldh            (  day2_sgpt_sgpt_ldh               );
		lab.setDay2serumfibrinogen        (  day2_serum_fibrinogen            );
		lab.setDay2ddimer                (  day2_d_dimer                     );
		lab.setDay2urinerm               (  day2_urine_rm                    );
		lab.setDay2occultblood           (  day2_stool_for_occult_blood      );
		lab.setDay2bloodgastime          (  day2_blood_gas_time              );
		lab.setDay2bloodgasfio2         (  day2_blood_gas_fio2              );
		lab.setDay2bloodgasph            (  day2_blood_gas_ph                );
		lab.setDay2bloodgaspco2          (  day2_blood_gas_pco2              );
		lab.setDay2bloodgaspo2           (  day2_blood_gas_po2               );
		lab.setDay2bloodgashco3          (  day2_blood_gas_hco3              );
		lab.setDay2bloodgassatn          (  day2_blood_gas_satn              );
		lab.setDay3hb                    (  day3_hb                          );
		lab.setDay3totalwbccount         (  day3_total_wbc_count             );
		lab.setDay3wbccount              (  day3_differential_wbc_count      );
		lab.setDay3plateletcount         (  day3_platelet_count              );
		lab.setDay3peripheralsmear       (  day3_peripheral_smear            );
		lab.setDay3btctcrt               (  day3_bt_ct_crt_                  );
		lab.setDay3ptapttinr             (  day3_pt_aptt_inr                 );
		lab.setDay3rhtype                (  day3_blood_group_rh_type         );
		lab.setDay3sicklingtest          (  day3_sickling_test               );
		lab.setDay3randombloodglucose   (  day3_random_blood_glucose        );
		lab.setDay3bloodureanitrogen     (  day3_blood_urea_nitrogen         );
		lab.setDay3serumcreatinine       (  day3_serum_creatinine            );
		lab.setDay3serumcalcium          (  day3_serum_calcium               );
		lab.setDay3serumsodium           (  day3_serum_sodium                );
		lab.setDay3serumpotassium        (  day3_serum_potassium             );
		lab.setDay3serumalbumin          (  day3_total_protein_serum_albumin );
		lab.setDay3serumbiliorubin       (  day3_serum_biliorubin            );
		lab.setDay3sgptsgptldh           (  day3_sgpt_sgpt_ldh               );
		lab.setDay3serumfibrinogen       (  day3_serum_fibrinogen            );
		lab.setDay3ddimer                (  day3_d_dimer                     );
		lab.setDay3urinerm               (  day3_urine_rm                    );
		lab.setDay3occultblood           (  day3_stool_for_occult_blood      );
		lab.setDay3bloodgastime          (  day3_blood_gas_time              );
		lab.setDay3bloodgasfio2          (  day3_blood_gas_fio2              );
		lab.setDay3bloodgasph            (  day3_blood_gas_ph                );
		lab.setDay3bloodgaspco2          (  day3_blood_gas_pco2              );
		lab.setDay3bloodgaspo2           (  day3_blood_gas_po2               );
		lab.setDay3bloodgashco3          (  day3_blood_gas_hco3              );
		lab.setDay3bloodgassatn          (  day3_blood_gas_satn              );
		lab.setPatientname               (  patient_name                     );
		lab.setRegno                     (  reg_no                           );
		lab.setDoa                       (  doa                              );
		lab.setDoctorincharge            (  doctor_in_charge                 );
		lab.setUrineculture              (  urine_culture                    );
		lab.setBloodculture              (  blood_culture                    );
		lab.setCervicalvaginalswab       (  cervical_vaginal_swab            );
		lab.setAnyother                  (  any_other                        );
		lab.setHiv                       (  hiv                              );
		lab.setHbsag                     (  hbsag                            );
		lab.setVdrl                      (  vdrl                             );
		
		icuRepository.save(icu);
		lab.setAdmission(icu);
		laboratoryRepository.save(lab);
		laboratoryRepository.save(lab);
		userMap.put("status", HttpStatus.OK);
		userMap.put("data", "Beneficary Added Successfull");
		userMap.put("admissionId",icu.getId().toString());
		userMap.put("unique_id", icu.getUniqueid());
		
		return userMap;
		
		
	}
	
	
	
	
	public HashMap<String, Object> updateAdmission(String userJsonReq,Long id) {
		HashMap<String, Object> userMap = new HashMap<>();                                                   
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);                                 
		String state         = openJson.getString("state");                                                  
		String district      = openJson.getString("district");                                               
		String block         = openJson.getString("block");                                                  
		String facility      = openJson.getString("facility");                                               
		String facilityType  = openJson.getString("facilityType");                                           
		                                                                                                     
		                                                                                                     
		org.json.JSONObject admission = openJson.getJSONObject("admission");                                 
		String  mcts_rch_number               = admission.getString("mcts_rch_number");                      
	//	String  name_of_facility              = admission.getString("name_of_facility");                     
		String  obs_icu_reg_number            = admission.getString("obs_icu_reg_number");
		String bedNo = admission.getString("bedNo");
	//	String  block_name                    = admission.getString("block_name");                           
		String  bpl_status                    = admission.getString("bpl_status");                           
	//	String  district_name                 = admission.getString("district_name");                        
		String  aadhar_number                 = admission.getString("aadhar_number");                        
		String  contact_number                = admission.getString("contact_number");                       
		String  name                          = admission.getString("name");                                 
		String  age                           = admission.getString("age");                                  
		String  w_o                           = admission.getString("w_o");                                  
		String  address                       = admission.getString("address");                              
		String  patient_contact_no            = admission.getString("patient_contact_no");                   
		String  caste                         = admission.getString("caste");                                
		String  religion                      = admission.getString("religion");                             
		String  admission_date                = admission.getString("admission_date");                       
		String  admission_time                = admission.getString("admission_time"); 
		String referredIn 					= admission.getString("referredIn");
		String nameOfFacility1 					= admission.getString("nameOfFacility1");
		String reasonforReferral 					= admission.getString("reasonforReferral");
		String  delivery_date                 = admission.getString("delivery_date");                        
		String  obstetric_etails_at_admission = admission.getString("obstetric_etails_at_admission");        
		String  delivery_time                 = admission.getString("delivery_time"); 
		String provisionalDiagnosis			 = admission.getString("provisionalDiagnosis"); 
		String placeOfDelivery			= 		admission.getString("placeOfDelivery");
		String  gravida                       = admission.getString("gravida");                              
		String  parity                        = admission.getString("parity");                               
		String  abortion                      = admission.getString("abortion");                             
		String  living_children               = admission.getString("living_children");                      
		String  lmp                           = admission.getString("lmp");                                  
		String  edd                           = admission.getString("edd");                                  
		String  number_of_anc                 = admission.getString("number_of_anc");                        
		String  gestational_age               = admission.getString("gestational_age");                      
		String  labour                        = admission.getString("labour");  
		String birthDate 					  = admission.getString("birthDate");
		String birthTime					  = admission.getString("birthTime");
		String  presentation                  = admission.getString("presentation");                         
		String  uterine_tenderness            = admission.getString("uterine_tenderness");                   
		String  fhr                           = admission.getString("fhr");                                  
		String  if_fhr_yes                    = admission.getString("if_fhr_yes");                           
		String  amniotic_fluid                = admission.getString("amniotic_fluid");                       
		String  course_of_labour              = admission.getString("course_of_labour");                     
		String  type_of_delivery              = admission.getString("type_of_delivery");                     
		String  aph                           = admission.getString("aph");                                  
		String  pph                           = admission.getString("pph");                                  
		String  p_v_foul_smelling             = admission.getString("p_v_foul_smelling");                    
		String  indi_for_caesarean            = admission.getString("indi_for_caesarean");                   
		String  deliv_attended_by             = admission.getString("deliv_attended_by");                    
		String  antenatal_steroids            = admission.getString("antenatal_steroids");                   
		String  if_yes                        = admission.getString("if_yes");                               
		String  no_of_doses                   = admission.getString("no_of_doses");                          
		String  magnesium_sulphate            = admission.getString("magnesium_sulphate");                   
		String  if_yes_dose                   = admission.getString("if_yes_dose");                          
		String  time_of_last_doses            = admission.getString("time_of_last_doses");                   
		                                                                                                     
		   org.json.JSONObject indication = openJson.getJSONObject("indication");                            
		                                                                                                     
		String  icu_admission                   =     indication.getString("icu_admission"); 
		String  hdu_admission                   =     indication.getString("hdu_admission");
		String  hemorrhage_hdu                  =     indication.getString("hemorrhage_hdu");                
		String  hemorrhage_icu                  =     indication.getString("hemorrhage_icu");                
		String  hemorrhage_others               =     indication.getString("hemorrhage_others");             
		String  hypertensive_disorders_hdu      =     indication.getString("hypertensive_disorders_hdu");    
		String  hypertensive_disorders_icu      =     indication.getString("hypertensive_disorders_icu");    
		String  sepsis_hdu                      =     indication.getString("sepsis_hdu");                    
		String  sepsis_icu                      =     indication.getString("sepsis_icu");                    
		String  sepsis_others                   =     indication.getString("sepsis_others");                 
		String  renal_dysfunction_hdu           =     indication.getString("renal_dysfunction_hdu");         
		String  renal_dysfunction_icu           =     indication.getString("renal_dysfunction_icu");         
		String  jaundice_in_pregnancy_hdu       =     indication.getString("jaundice_in_pregnancy_hdu");     
		String  jaundice_in_pregnancy_icu       =     indication.getString("jaundice_in_pregnancy_icu");     
		String  jaundice_in_pregnancy_others    =     indication.getString("jaundice_in_pregnancy_others");  
		String  coagulation_system_hdu          =     indication.getString("coagulation_system_hdu");        
		String  coagulation_system_icu          =     indication.getString("coagulation_system_icu");        
		String  abnormal_vitals_hdu             =     indication.getString("abnormal_vitals_hdu");           
		String  abnormal_vitals_icu              =    indication.getString("abnormal_vitals_icu");           
		String  abg_abnormalities_hdu            =    indication.getString("abg_abnormalities_hdu");         
		String  abg_abnormalities_icu            =    indication.getString("abg_abnormalities_icu");         
		String  electrolyte_disturbances_hdu     =    indication.getString("electrolyte_disturbances_hdu");  
		String  electrolyte_disturbances_icu     =    indication.getString("electrolyte_disturbances_icu");  
		String  medical_disorders_hdu            =    indication.getString("medical_disorders_hdu");         
		String  medical_disorders_icu           =    indication.getString("medical_disorders_icu");          
		String  medical_disorders_others10      =    indication.getString("medical_disorders_others10");     
		String  cause_of_admission              =    indication.getString("cause_of_admission");             
		                                                                                                     
		     org.json.JSONObject history = openJson.getJSONObject("history");                                
		                                                                                                     
		String  past_medical_history     =  history.getString("past_medical_history");                       
		String  past_surgical_history    =  history.getString("past_surgical_history");                      
		String  family_history           =  history.getString("family_history");                             
		                                                                                                     
		   org.json.JSONObject baby = openJson.getJSONObject("baby_information_at_birth");                   
		                                                                                                     
		String  outcome_of_birth     =  baby.getString("outcome_of_birth");                                  
		String  birth_weight         =  baby.getString("birth_weight");                                      
		String  multiple_births      =  baby.getString("multiple_births");                                   
		String  if_yes_number        =  baby.getString("if_yes_number");                                     
		String  sexofbaby            =  baby.getString("sexofbaby");                                         
		String  resu_required        =  baby.getString("resu_required");                                     
		String  weeks_of_gestation   =  baby.getString("weeks_of_gestation");                                
		String  sncu_admission       =  baby.getString("sncu_admission");                                    
		String  vitamin_K_given      =  baby.getString("vitamin_K_given");                                   
		String  b_fed_within_1hour   =  baby.getString("b_fed_within_1hour");                                
		                                                                                                     
		                                                                                                     
		   org.json.JSONObject general = openJson.getJSONObject("general_examination");                      
		                                                                                                     
		                                                                                                     
		String  general_condition    =   general.getString("general_condition");                             
		String  height               =   general.getString("height");                                        
		String  weight               =   general.getString("weight");                                        
		String  bmi                  =   general.getString("bmi");                                           
		String  heart_rate           =   general.getString("heart_rate");                                    
		String  blood_pressure       =   general.getString("blood_pressure");                                
		String  spo2                 =   general.getString("spo2");                                          
		String  respiratory_rate     =   general.getString("respiratory_rate");                              
		String  temperature          =   general.getString("temperature");                                   
		String  jvp                  =   general.getString("jvp");                                           
		String  pallor               =   general.getString("pallor");                                        
		String  icterus              =   general.getString("icterus");                                       
		String  cyanosis             =   general.getString("cyanosis");                                      
		String  oedema               =   general.getString("oedema");                                        
		String  g_ex_others          =   general.getString("g_ex_others");                                   
		                                                                                                     
		    org.json.JSONObject systemic = openJson.getJSONObject("systemic_examination");                   
		                                                                                                     
		String  respiratory_system                =  systemic.getString("respiratory_system");               
		String  cardio_vascular_system            =  systemic.getString("cardio_vascular_system");           
		String  central_nervous_system            =  systemic.getString("central_nervous_system");           
		String  per_abdomen_examination           =  systemic.getString("per_abdomen_examination");          
		String  per_speculum_vaginal              =  systemic.getString("per_speculum_vaginal");             
		String  any_others                        =  systemic.getString("any_others");                       
		String  respiratory_system_text           =  systemic.getString("respiratory_system_text");          
		String  cardio_vascular_system_text       =  systemic.getString("cardio_vascular_system_text");      
		String  central_nervous_system_text       =  systemic.getString("central_nervous_system_text");      
		String  per_abdomen_examination_text      =  systemic.getString("per_abdomen_examination_text");     
		String  per_speculum_vaginal_text         =  systemic.getString("per_speculum_vaginal_text");        
		                                                                                                     
		         org.json.JSONObject orders = openJson.getJSONObject("orders");                              
		                                                                                                     
		String  nutrition              = orders.getString("nutrition");                                      
		String  investigations         = orders.getString("investigations");                                 
		String  medications            = orders.getString("medications");                                    
		String  procedures             = orders.getString("procedures");                                     
		String  monitoring_schedule    = orders.getString("monitoring_schedule");                            
		String  multidiscipl_review    = orders.getString("multidiscipl_review");                            
		String  plan_for_next_24_hours = orders.getString("plan_for_next_24_hours");                         
		String  doctor_name_and_sign   = orders.getString("doctor_name_and_sign");                           
		                                                                                                     
		           org.json.JSONObject sequentialorgan  = openJson.getJSONObject("sequentialorgan");         
		                                                                                                     
		String  respiration         = sequentialorgan.getString("respiration");                              
		String  coagulation         = sequentialorgan.getString("coagulation");                              
		String  liver               = sequentialorgan.getString("liver");                                    
		String  cardiovascular      = sequentialorgan.getString("cardiovascular");                           
		String  cns                 = sequentialorgan.getString("cns");                                      
		String  renal               = sequentialorgan.getString("renal");                                    
		                                                                                                     
		                                                                                                     
		  org.json.JSONObject laboratory = openJson.getJSONObject("laboratory_investigation_sheet");         
		                                                                                                     
		String  day1_hb                          = laboratory.getString("day1_hb");                              
		String  day1_total_wbc_count             = laboratory.getString("day1_total_wbc_count");                 
		String  day1_differential_wbc_count      = laboratory.getString("day1_differential_wbc_count");          
		String  day1_platelet_count              = laboratory.getString("day1_platelet_count");                  
		String  day1_peripheral_smear            = laboratory.getString("day1_peripheral_smear");                
		String  day1_bt_ct_crt_                  = laboratory.getString("day1_bt_ct_crt_");                      
		String  day1_pt_aptt_inr                 = laboratory.getString("day1_pt_aptt_inr");                     
		String  day1_blood_group_rh_type         = laboratory.getString("day1_blood_group_rh_type");             
		String  day1_sickling_test               = laboratory.getString("day1_sickling_test");                   
		String  day1_random_blood_glucose        = laboratory.getString("day1_random_blood_glucose");            
		String  day1_blood_urea_nitrogen         = laboratory.getString("day1_blood_urea_nitrogen");             
		String  day1_serum_creatinine            = laboratory.getString("day1_serum_creatinine");                
		String  day1_serum_calcium               = laboratory.getString("day1_serum_calcium");                   
		String  day1_serum_sodium                = laboratory.getString("day1_serum_sodium");                    
		String  day1_serum_potassium             = laboratory.getString("day1_serum_potassium");                 
		String  day1_total_protein_serum_albumin = laboratory.getString("day1_total_protein_serum_albumin");     
		String  day1_serum_biliorubin            = laboratory.getString("day1_serum_biliorubin");                
		String  day1_sgpt_sgpt_ldh               = laboratory.getString("day1_sgpt_sgpt_ldh");                   
		String  day1_serum_fibrinogen            = laboratory.getString("day1_serum_fibrinogen");                
		String  day1_d_dimer                     = laboratory.getString("day1_d_dimer");                         
		String  day1_urine_rm                    = laboratory.getString("day1_urine_rm");                        
		String  day1_stool_for_occult_blood      = laboratory.getString("day1_stool_for_occult_blood");          
		String  day1_blood_gas_time              = laboratory.getString("day1_blood_gas_time");                  
		String  day1_blood_gas_fio2              = laboratory.getString("day1_blood_gas_fio2");                  
		String  day1_blood_gas_ph                = laboratory.getString("day1_blood_gas_ph");                    
		String  day1_blood_gas_pco2              = laboratory.getString("day1_blood_gas_pco2");                  
		String  day1_blood_gas_po2               = laboratory.getString("day1_blood_gas_po2");                   
		String  day1_blood_gas_hco3              = laboratory.getString("day1_blood_gas_hco3");                  
		String  day1_blood_gas_satn              = laboratory.getString("day1_blood_gas_satn");                  
		String  day2_hb                          = laboratory.getString("day2_hb");                              
		String  day2_total_wbc_count             = laboratory.getString("day2_total_wbc_count");                 
		String  day2_differential_wbc_count      = laboratory.getString("day2_differential_wbc_count");          
		String  day2_platelet_count              = laboratory.getString("day2_platelet_count");                  
		String  day2_peripheral_smear            = laboratory.getString("day2_peripheral_smear");                
		String  day2_bt_ct_crt_                  = laboratory.getString("day2_bt_ct_crt_");                      
		String  day2_pt_aptt_inr                 = laboratory.getString("day2_pt_aptt_inr");                     
		String  day2_blood_group_rh_type         = laboratory.getString("day2_blood_group_rh_type");             
		String  day2_sickling_test               = laboratory.getString("day2_sickling_test");                   
		String  day2_random_blood_glucose        = laboratory.getString("day2_random_blood_glucose");            
		String  day2_blood_urea_nitrogen         = laboratory.getString("day2_blood_urea_nitrogen");             
		String  day2_serum_creatinine            = laboratory.getString("day2_serum_creatinine");                
		String  day2_serum_calcium               = laboratory.getString("day2_serum_calcium");                   
		String  day2_serum_sodium                = laboratory.getString("day2_serum_sodium");                    
		String  day2_serum_potassium             = laboratory.getString("day2_serum_potassium");                 
		String  day2_total_protein_serum_albumin = laboratory.getString("day2_total_protein_serum_albumin");     
		String  day2_serum_biliorubin            = laboratory.getString("day2_serum_biliorubin");                
		String  day2_sgpt_sgpt_ldh               = laboratory.getString("day2_sgpt_sgpt_ldh");                   
		String  day2_serum_fibrinogen            = laboratory.getString("day2_serum_fibrinogen");                
		String  day2_d_dimer                     = laboratory.getString("day2_d_dimer");                         
		String  day2_urine_rm                    = laboratory.getString("day2_urine_rm");                        
		String  day2_stool_for_occult_blood      = laboratory.getString("day2_stool_for_occult_blood");          
		String  day2_blood_gas_time              = laboratory.getString("day2_blood_gas_time");                  
		String  day2_blood_gas_fio2              = laboratory.getString("day2_blood_gas_fio2");                  
		String  day2_blood_gas_ph                = laboratory.getString("day2_blood_gas_ph");                    
		String  day2_blood_gas_pco2              = laboratory.getString("day2_blood_gas_pco2");                  
		String  day2_blood_gas_po2               = laboratory.getString("day2_blood_gas_po2");                   
		String  day2_blood_gas_hco3              = laboratory.getString("day2_blood_gas_hco3");                  
		String  day2_blood_gas_satn              = laboratory.getString("day2_blood_gas_satn");                  
		String  day3_hb                          = laboratory.getString("day3_hb");                              
		String  day3_total_wbc_count             = laboratory.getString("day3_total_wbc_count");                 
		String  day3_differential_wbc_count      = laboratory.getString("day3_differential_wbc_count");          
		String  day3_platelet_count              = laboratory.getString("day3_platelet_count");                  
		String  day3_peripheral_smear            = laboratory.getString("day3_peripheral_smear");                
		String  day3_bt_ct_crt_                  = laboratory.getString("day3_bt_ct_crt_");                      
		String  day3_pt_aptt_inr                 = laboratory.getString("day3_pt_aptt_inr");                     
		String  day3_blood_group_rh_type         = laboratory.getString("day3_blood_group_rh_type");             
		String  day3_sickling_test               = laboratory.getString("day3_sickling_test");                   
		String  day3_random_blood_glucose        = laboratory.getString("day3_random_blood_glucose");            
		String  day3_blood_urea_nitrogen         = laboratory.getString("day3_blood_urea_nitrogen");             
		String  day3_serum_creatinine            = laboratory.getString("day3_serum_creatinine");                
		String  day3_serum_calcium               = laboratory.getString("day3_serum_calcium");                   
		String  day3_serum_sodium                = laboratory.getString("day3_serum_sodium");                    
		String  day3_serum_potassium             = laboratory.getString("day3_serum_potassium");                 
		String  day3_total_protein_serum_albumin = laboratory.getString("day3_total_protein_serum_albumin");     
		String  day3_serum_biliorubin            = laboratory.getString("day3_serum_biliorubin");                
		String  day3_sgpt_sgpt_ldh               = laboratory.getString("day3_sgpt_sgpt_ldh");                   
		String  day3_serum_fibrinogen            = laboratory.getString("day3_serum_fibrinogen");                
		String  day3_d_dimer                     = laboratory.getString("day3_d_dimer");                         
		String  day3_urine_rm                    = laboratory.getString("day3_urine_rm");                        
		String  day3_stool_for_occult_blood      = laboratory.getString("day3_stool_for_occult_blood");          
		String  day3_blood_gas_time              = laboratory.getString("day3_blood_gas_time");                  
		String  day3_blood_gas_fio2              = laboratory.getString("day3_blood_gas_fio2");                  
		String  day3_blood_gas_ph                = laboratory.getString("day3_blood_gas_ph");                    
		String  day3_blood_gas_pco2              = laboratory.getString("day3_blood_gas_pco2");                  
		String  day3_blood_gas_po2               = laboratory.getString("day3_blood_gas_po2");                   
		String  day3_blood_gas_hco3              = laboratory.getString("day3_blood_gas_hco3");                  
		String  day3_blood_gas_satn              = laboratory.getString("day3_blood_gas_satn");                  
		String  patient_name                     = laboratory.getString("patient_name");                         
		String  reg_no                           = laboratory.getString("reg_no");                               
		String  doa                              = laboratory.getString("doa");                                  
		String  doctor_in_charge                 = laboratory.getString("doctor_in_charge");                     
		String  urine_culture                    = laboratory.getString("urine_culture");                        
		String  blood_culture                    = laboratory.getString("blood_culture");                        
		String  cervical_vaginal_swab            = laboratory.getString("cervical_vaginal_swab");                
		String  any_other                        = laboratory.getString("any_other");                            
		String  hiv                              = laboratory.getString("hiv");                                  
		String  hbsag                            = laboratory.getString("hbsag");                                
		String  vdrl                             = laboratory.getString("vdrl");                                 
		                                                                           
		
		
		String state_name="";
		String district_name="";
		String block_name="";
		String facility_name="";
		String facility_type_name="";
		
		try {
			
		   
		   
				List<LocationMasters> locations=null;
				   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
					for (LocationMasters locationMasters : locations)
					{
						 state_name=locationMasters.getState();
						 district_name=locationMasters.getDistrictCode();
						 block_name=locationMasters.getBlock();
						 facility_name=locationMasters.getFacility();
						 facility_type_name=locationMasters.getFacilityType();
						 
						
						
					}
					}catch (Exception e) {
						userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
						userMap.put("data", e.getMessage());
						userMap.put("admissionId","");
						userMap.put("unique_id", "");
						userMap.put("baby", "");
						return userMap;
					}
		
		GoiIcu icu =  icuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", id));
		
		
		icu.setState							(state);                
		icu.setDistrict							(district);            
		icu.setBlock							(block);                
		icu.setFacility							(facility);             
		icu.setFacilityType						(facilityType); 

		icu.setStateName(state_name);
		icu.setFacilityName(facility_name);   
		icu.setBedNo(bedNo);
		icu.setProvisionalDiagnosis(provisionalDiagnosis);
		icu.setReferredIn(referredIn);
		icu.setNameOfFacility1(nameOfFacility1);
		icu.setReasonforReferral(reasonforReferral);
		icu.setPlaceOfDelivery(placeOfDelivery);
		icu.setBirthDate(birthDate);
		icu.setBirthTime(birthTime);
		icu.setFacilityTypeName	(facility_type_name);
		icu.setMctsrchnumber       ( mcts_rch_number               );
		icu.setFacilityName      ( facility_name              );
		icu.setObsicuregnumber   ( obs_icu_reg_number            );
		icu.setBlockname          ( block_name                    );
		icu.setBplstatus          ( bpl_status                    );
		icu.setDistrictname       ( district_name                 );
		icu.setAadharnumber       ( aadhar_number                 );
		icu.setContactnumber      ( contact_number                );
		icu.setName               ( name                          );
		icu.setAge                ( age                           );
		icu.setWo                 ( w_o                           );
		icu.setAddress            ( address                       );
		icu.setPatientcontactno   ( patient_contact_no            );
		icu.setCaste              ( caste                         );
		icu.setReligion           ( religion                      );
		icu.setAdmissiondate     ( admission_date                );
		icu.setAdmissiontime      ( admission_time                );
		icu.setDeliverydate      ( delivery_date                 );
		icu.setObstetricdetails   ( obstetric_etails_at_admission );
		icu.setDeliverytime       ( delivery_time                 );
		icu.setGravida            ( gravida                       );
		icu.setParity            ( parity                        );
		icu.setAbortion          ( abortion                      );
		icu.setLivingchildren    ( living_children               );
		icu.setLmp                ( lmp                           );
		icu.setEdd               ( edd                           );
		icu.setNumberofanc        ( number_of_anc                 );
		icu.setGestationalage     ( gestational_age               );
		icu.setLabour             ( labour                        );
		icu.setPresentation       ( presentation                  );
		icu.setUterinetenderness  ( uterine_tenderness            );
		icu.setFhr                ( fhr                           );
		icu.setIffhryes           ( if_fhr_yes                    );
		icu.setAmnioticfluid      ( amniotic_fluid                );
		icu.setCourseoflabour     ( course_of_labour              );
		icu.setTypeofdelivery     ( type_of_delivery              );
		icu.setAph                ( aph                           );
		icu.setPph                ( pph                           );
		icu.setPvfoulsmelling     ( p_v_foul_smelling             );
		icu.setIndiforcaesarean   ( indi_for_caesarean            );
		icu.setDelivattendedby    ( deliv_attended_by             );
		icu.setAntenatalsteroids ( antenatal_steroids            );
		icu.setIfyes             ( if_yes                        );
		icu.setNoofdoses          ( no_of_doses                   );
		icu.setMagnesiumsulphate  ( magnesium_sulphate            );
		icu.setIfyesdose          ( if_yes_dose                   );
		icu.setTimeoflastdoses    ( time_of_last_doses            );
		icu.setIcu_admission                   (icu_admission                   );
		icu.setHdu_admission(hdu_admission);
		icu.setHemorrhage_hdu                  (hemorrhage_hdu                  );
		icu.setHemorrhage_icu                  (hemorrhage_icu                  );
		icu.setHemorrhage_others               (hemorrhage_others               );
		icu.setHypertensive_disorders_hdu      (hypertensive_disorders_hdu      );
		icu.setHypertensive_disorders_icu      (hypertensive_disorders_icu      );
		icu.setSepsis_hdu                      (sepsis_hdu                      );
		icu.setSepsis_icu                      (sepsis_icu                      );
		icu.setSepsis_others                   (sepsis_others                   );
		icu.setRenal_dysfunction_hdu           (renal_dysfunction_hdu           );
		icu.setRenal_dysfunction_icu           (renal_dysfunction_icu          );
		icu.setJaundice_in_pregnancy_hdu       (jaundice_in_pregnancy_hdu       );
		icu.setJaundice_in_pregnancy_icu       (jaundice_in_pregnancy_icu       );
		icu.setJaundice_in_pregnancy_others    (jaundice_in_pregnancy_others    );
		icu.setCoagulation_system_hdu          (coagulation_system_hdu          );
		icu.setCoagulation_system_icu          (coagulation_system_icu          );
		icu.setAbnormal_vitals_hdu             (abnormal_vitals_hdu           );
		icu.setAbnormal_vitals_icu             (abnormal_vitals_icu           );
		icu.setAbg_abnormalities_hdu           (abg_abnormalities_hdu         );
		icu.setAbg_abnormalities_icu           (abg_abnormalities_icu         );
		icu.setElectrolyte_disturbances_hdu    (electrolyte_disturbances_hdu  );
		icu.setElectrolyte_disturbances_icu    (electrolyte_disturbances_icu  );
		icu.setMedical_disorders_hdu           (medical_disorders_hdu         );
		icu.setMedical_disorders_icu           (medical_disorders_icu         );
		icu.setMedical_disorders_others10      (medical_disorders_others10    );
		icu.setCause_of_admission              (cause_of_admission            );
		icu.setCauseofadmission    (cause_of_admission );
		icu.setPastmedicalhistory      (past_medical_history  );
		icu.setPastsurgicalhistory    (past_surgical_history );
		icu.setFamilyhistory          (family_history        );
		icu.setOutcomeofbirth         (outcome_of_birth   );
		icu.setBirthweight            (birth_weight       );
		icu.setMultiplebirths         (multiple_births    );
		icu.setIfyesnumber            (if_yes_number      );
		icu.setSexofbaby              (sexofbaby          );
		icu.setResurequired           (resu_required      );
		icu.setWeeksofgestation       (weeks_of_gestation );
		icu.setSncuadmission          (sncu_admission     );
		icu.setVitaminKgiven          (vitamin_K_given    );
		icu.setBfedwithin1hour        (b_fed_within_1hour );
		icu.setGeneralcondition            ( general_condition );
		icu.setHeight                     ( height            );
		icu.setWeight                     ( weight            );
		icu.setBmi                        ( bmi               );
		icu.setHeartrate                  ( heart_rate        );
		icu.setBloodpressure              ( blood_pressure    );
		icu.setSpo2                       ( spo2              );
		icu.setRespiratoryrate            ( respiratory_rate  );
		icu.setTemperature                ( temperature       );
		icu.setJvp                        ( jvp               );
		icu.setPallor                    ( pallor            );
		icu.setIcterus                    ( icterus           );
		icu.setCyanosis                  ( cyanosis          );
		icu.setOedema                     ( oedema            );
		icu.setGexothers                  ( g_ex_others       );
		icu.setRespiratorysystem          (respiratory_system          );
		icu.setCardiovascularsystem       (cardio_vascular_system      );
		icu.setCentralnervoussystem       (central_nervous_system      );
		icu.setPerabdomenexamination      (per_abdomen_examination     );
		icu.setPerspeculumvaginal	       (per_speculum_vaginal        );
		icu.setAnyothers		          (any_others                  );
		icu.setRespiratorysystemtext      (respiratory_system_text     );
		icu.setCardiosystemtext           (cardio_vascular_system_text );
		icu.setCentralsystemtext          (central_nervous_system_text );
		icu.setPerabdomentext             (per_abdomen_examination_text);
		icu.setPerspeculumtext            (per_speculum_vaginal_text    );
		icu.setNutrition                   (nutrition             );
		icu.setInvestigations              (investigations        );
		icu.setMedications                 (medications           );
		icu.setProcedures                  (procedures            );
		icu.setMonitoringschedule          (monitoring_schedule   );
		icu.setMultidisciplreview          (multidiscipl_review   );
		icu.setPlanfor24hours              (plan_for_next_24_hours);
		icu.setDoctornamesign              (doctor_name_and_sign  );
		icu.setRespiration                 (respiration   );
		icu.setCoagulation                 (coagulation   );
		icu.setLiver                       (liver         );
		icu.setCardiovascular              (cardiovascular);
		icu.setCns                         (cns           );
		icu.setRenal                       (renal         );
		 
		Laboratory  lab = laboratoryRepository.findById(icu.getLabor().getId()).orElseThrow(() -> new ResourceNotFoundException("Labouratory", "NOT FOUND", id));
		
		lab.setDay1hb                     (  day1_hb                          );
		lab.setDay1totalwbccount          (  day1_total_wbc_count             );
		lab.setDay1wbccount               (  day1_differential_wbc_count      );
		lab.setDay1plateletcount          (  day1_platelet_count              );
		lab.setDay1peripheralsmear        (  day1_peripheral_smear            );
		lab.setDay1btctcrt                (  day1_bt_ct_crt_                  );
		lab.setDay1ptapttinr              (  day1_pt_aptt_inr                 );
		lab.setDay1bloodgrouprhtype       (  day1_blood_group_rh_type         );
		lab.setDay1sicklingtest           (  day1_sickling_test               );
		lab.setDay1randombloodglucose     (  day1_random_blood_glucose        );
		lab.setDay1bloodureanitrogen      (  day1_blood_urea_nitrogen         );
		lab.setDay1serumcreatinine        (  day1_serum_creatinine            );
		lab.setDay1serumcalcium           (  day1_serum_calcium               );
		lab.setDay1serumsodium            (  day1_serum_sodium                );
		lab.setDay1serumpotassium         (  day1_serum_potassium             );
		lab.setDay1serumalbumin           (  day1_total_protein_serum_albumin );
		lab.setDay1serumbiliorubin        (  day1_serum_biliorubin            );
		lab.setDay1sgptsgptldh            (  day1_sgpt_sgpt_ldh               );
		lab.setDay1serumfibrinogen        (  day1_serum_fibrinogen            );
		lab.setDay1ddimer                 (  day1_d_dimer                     );
		lab.setDay1urinerm                (  day1_urine_rm                    );
		lab.setDay1occultblood            (  day1_stool_for_occult_blood      );
		lab.setDay1bloodgastime           (  day1_blood_gas_time              );
		lab.setDay1bloodgasfio2           (  day1_blood_gas_fio2              );
		lab.setDay1bloodgasph             (  day1_blood_gas_ph                );
		lab.setDay1bloodgaspco2           (  day1_blood_gas_pco2              );
		lab.setDay1bloodgaspo2            (  day1_blood_gas_po2               );
		lab.setDay1bloodgashco3           (  day1_blood_gas_hco3              );
		lab.setDay1bloodgassatn           (  day1_blood_gas_satn              );
		lab.setDay2hb                     (  day2_hb                          );
		lab.setDay2totalwbccount          (  day2_total_wbc_count             );
		lab.setDay2wbccount               (  day2_differential_wbc_count      );
		lab.setDay2plateletcount          (  day2_platelet_count              );
		lab.setDay2peripheralsmear        (  day2_peripheral_smear            );
		lab.setDay2btctcrt                (  day2_bt_ct_crt_                  );
		lab.setDay2ptapttinr              (  day2_pt_aptt_inr                 );
		lab.setDay2bloodgrouprhtype       (  day2_blood_group_rh_type         );
		lab.setDay2sicklingtest           (  day2_sickling_test               );
		lab.setDay2randombloodglucose     (  day2_random_blood_glucose        );
		lab.setDay2bloodureanitrogen      (  day2_blood_urea_nitrogen         );
		lab.setDay2serumcreatinine        (  day2_serum_creatinine            );
		lab.setDay2serumcalcium           (  day2_serum_calcium               );
		lab.setDay2serumsodium            (  day2_serum_sodium                );
		lab.setDay2serumpotassium         (  day2_serum_potassium             );
		lab.setDay2serumalbumin           (  day2_total_protein_serum_albumin );
		lab.setDay2serumbiliorubin        (  day2_serum_biliorubin            );
		lab.setDay2sgptsgptldh            (  day2_sgpt_sgpt_ldh               );
		lab.setDay2serumfibrinogen        (  day2_serum_fibrinogen            );
		lab.setDay2ddimer                (  day2_d_dimer                     );
		lab.setDay2urinerm               (  day2_urine_rm                    );
		lab.setDay2occultblood           (  day2_stool_for_occult_blood      );
		lab.setDay2bloodgastime          (  day2_blood_gas_time              );
		lab.setDay2bloodgasfio2         (  day2_blood_gas_fio2              );
		lab.setDay2bloodgasph            (  day2_blood_gas_ph                );
		lab.setDay2bloodgaspco2          (  day2_blood_gas_pco2              );
		lab.setDay2bloodgaspo2           (  day2_blood_gas_po2               );
		lab.setDay2bloodgashco3          (  day2_blood_gas_hco3              );
		lab.setDay2bloodgassatn          (  day2_blood_gas_satn              );
		lab.setDay3hb                    (  day3_hb                          );
		lab.setDay3totalwbccount         (  day3_total_wbc_count             );
		lab.setDay3wbccount              (  day3_differential_wbc_count      );
		lab.setDay3plateletcount         (  day3_platelet_count              );
		lab.setDay3peripheralsmear       (  day3_peripheral_smear            );
		lab.setDay3btctcrt               (  day3_bt_ct_crt_                  );
		lab.setDay3ptapttinr             (  day3_pt_aptt_inr                 );
		lab.setDay3rhtype                (  day3_blood_group_rh_type         );
		lab.setDay3sicklingtest          (  day3_sickling_test               );
		lab.setDay3randombloodglucose   (  day3_random_blood_glucose        );
		lab.setDay3bloodureanitrogen     (  day3_blood_urea_nitrogen         );
		lab.setDay3serumcreatinine       (  day3_serum_creatinine            );
		lab.setDay3serumcalcium          (  day3_serum_calcium               );
		lab.setDay3serumsodium           (  day3_serum_sodium                );
		lab.setDay3serumpotassium        (  day3_serum_potassium             );
		lab.setDay3serumalbumin          (  day3_total_protein_serum_albumin );
		lab.setDay3serumbiliorubin       (  day3_serum_biliorubin            );
		lab.setDay3sgptsgptldh           (  day3_sgpt_sgpt_ldh               );
		lab.setDay3serumfibrinogen       (  day3_serum_fibrinogen            );
		lab.setDay3ddimer                (  day3_d_dimer                     );
		lab.setDay3urinerm               (  day3_urine_rm                    );
		lab.setDay3occultblood           (  day3_stool_for_occult_blood      );
		lab.setDay3bloodgastime          (  day3_blood_gas_time              );
		lab.setDay3bloodgasfio2          (  day3_blood_gas_fio2              );
		lab.setDay3bloodgasph            (  day3_blood_gas_ph                );
		lab.setDay3bloodgaspco2          (  day3_blood_gas_pco2              );
		lab.setDay3bloodgaspo2           (  day3_blood_gas_po2               );
		lab.setDay3bloodgashco3          (  day3_blood_gas_hco3              );
		lab.setDay3bloodgassatn          (  day3_blood_gas_satn              );
		lab.setPatientname               (  patient_name                     );
		lab.setRegno                     (  reg_no                           );
		lab.setDoa                       (  doa                              );
		lab.setDoctorincharge            (  doctor_in_charge                 );
		lab.setUrineculture              (  urine_culture                    );
		lab.setBloodculture              (  blood_culture                    );
		lab.setCervicalvaginalswab       (  cervical_vaginal_swab            );
		lab.setAnyother                  (  any_other                        );
		lab.setHiv                       (  hiv                              );
		lab.setHbsag                     (  hbsag                            );
		lab.setVdrl                      (  vdrl                             );
		
		icuRepository.save(icu);
		lab.setAdmission(icu);
		laboratoryRepository.save(lab);
		userMap.put("status", HttpStatus.OK);
		userMap.put("data", "Beneficary Updated Successfull");
		userMap.put("admissionId",icu.getId().toString());
		userMap.put("unique_id", icu.getUniqueid());
		
		return userMap;
		
		
	}
	
	
}
