package com.dhanush.infotech.project.GOIICU.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.repository.IcuMPReportRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class MPRDataEntryController 
{
	
	@Autowired 
	IcuMPReportRepository icumpreportRepository;
	
/*	@CrossOrigin
	@RequestMapping(value="/mprdataentry",method=RequestMethod.POST,produces="application/json",consumes="application/json")
	public String mprDataEntry(@RequestBody String req)
	{
		JSONObject obj=new JSONObject(req);
		String state=obj.getString("state");
		String district=obj.getString("district");
		String block=obj.getString("block");
		String facilitytype=obj.getString("facilitytype");
		String facility=obj.getString("facility");
		String month=obj.getString("month");
		String year=obj.getString("year");
		String inpatients=obj.getString("inpatients");
		String referredin=obj.getString("referredin");
		String total=obj.getString("total");
		String aph=obj.getString("aph");
		String pph=obj.getString("pph");
		String sepsis=obj.getString("sepsis");
		String eclampsia_sse=obj.getString("eclampsia_sse");
		String abortion=obj.getString("abortion");
		String obstructedlabour=obj.getString("obstructedlabour");
		String reptureduterus=obj.getString("reptureduterus");
		String severeanemia=obj.getString("severeanemia");
		String cardiacdiseases=obj.getString("cardiacdiseases");
		String jaundice=obj.getString("jaundice");
		String others=obj.getString("others");
		String shiftedtotheword=obj.getString("shiftedtotheword");
		String referredout=obj.getString("referredout");
		String lama=obj.getString("lama");
		String death=obj.getString("death");
		String readmission=obj.getString("readmission");
		String bedoccupancyrate=obj.getString("bedoccupancyrate");
		String averagelengthofstay=obj.getString("averagelengthofstay");
		
		return null;

	}*/
	
	@CrossOrigin
	@RequestMapping(value="/icumpreport",method=RequestMethod.POST,produces="application/json",consumes="application/json")
	public HashMap<String, Object> icuMpreport(@RequestBody String req)
	{
		
		JSONObject obj=new JSONObject(req);
		String state=obj.getString("state");
		String district=obj.getString("district").equalsIgnoreCase("ALL")?"":obj.getString("district");
		String block=obj.getString("block").equalsIgnoreCase("ALL")?"":obj.getString("block");
		String facilitytype=obj.getString("facilitytype").equalsIgnoreCase("ALL")?"":obj.getString("facilitytype");
		String facility=obj.getString("facility").equalsIgnoreCase("ALl")?"":obj.getString("facility");
		String year=obj.getString("year").equalsIgnoreCase("ALL")?"":obj.getString("year")+"-";
		String month=obj.getString("month").equalsIgnoreCase("ALL")?"":obj.getString("month").length()==1?"0"+obj.getString("month")+"-":obj.getString("month")+"-";
		
		String date=year+""+month;
		HashMap<String, Object> hm=new HashMap<>();
		
		hm.put("aph",getAph(state,district,block,facilitytype,facility,year,month,date));
		hm.put("pph",getPph(state,district,block,facilitytype,facility,year,month,date));
		hm.put("sepsis",getSepsis(state,district,block,facilitytype,facility,year,month,date));
		hm.put("Eclampsia",getEclampsia(state,district,block,facilitytype,facility,year,month,date));
		hm.put("Abortion",getAbortion(state,district,block,facilitytype,facility,year,month,date));
		hm.put("ObstructedLabour",getObstructedLabour(state,district,block,facilitytype,facility,year,month,date));
		hm.put("RupturedUterus",getRupturedUterus(state,district,block,facilitytype,facility,year,month,date));
		hm.put("SevereAnaemia",getSevereAnaemia(state,district,block,facilitytype,facility,year,month,date));
		hm.put("CardiacDiseases",getCardiacDiseases(state,district,block,facilitytype,facility,year,month,date));
		hm.put("Jaundice",getJaundice(state,district,block,facilitytype,facility,year,month,date));
		hm.put("Others",getOthers(state,district,block,facilitytype,facility,year,month,date));

		
		
		  
		
		return hm;
		
		
		
	}

	private Long getOthers(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByOthersStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByOthersDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByOthersBlockAdmissionDate(state,district,block,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByOthersFacilityTypeAdmissionDate(state,district,block,facilitytype,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByOthersFacilityAdmissionDate(state,district,block,facilitytype,facility,date);

		}
		
		return total;
	}

	private Long getJaundice(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByJaundiceStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByJaundiceDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByJaundiceBlockAdmissionDate(state,district,block,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByJaundiceFacilityTypeAdmissionDate(state,district,block,facilitytype,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByJaundiceFacilityAdmissionDate(state,district,block,facilitytype,facility,date);

		}
		
		return total;
	}

	private Long getCardiacDiseases(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByCardiacDiseasesStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByCardiacDiseasesDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByCardiacDiseasesBlockAdmissionDate(state,district,block,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByCardiacDiseasesFacilityTypeAdmissionDate(state,district,block,facilitytype,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByCardiacDiseasesFacilityAdmissionDate(state,district,block,facilitytype,facility,date);

		}
		
		return total;
	}

	private Long getSevereAnaemia(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySevereAnaemiaStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySevereAnaemiaDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySevereAnaemiaBlockAdmissionDate(state,district,block,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySevereAnaemiaFacilityTypeAdmissionDate(state,district,block,facilitytype,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findBySevereAnaemiaFacilityAdmissionDate(state,district,block,facilitytype,facility,date);

		}
		
		return total;
	
	}

	private Long getRupturedUterus(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date)
	{
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByRupturedUterusStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByRupturedUterusDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByRupturedUterusBlockAdmissionDate(state,district,block,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByRupturedUterusFacilityTypeAdmissionDate(state,district,block,facilitytype,date);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByRupturedUterusFacilityAdmissionDate(state,district,block,facilitytype,facility,date);

		}
		
		return total;
	}

	private Long getObstructedLabour(String state, String district, String block, String facilitytype,
			String facility, String year, String month, String date)
	{
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByObstructedLabourStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByObstructedLabourDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByObstructedLabourBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByObstructedLabourFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByObstructedLabourFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
		
	
	}

	private Long getAbortion(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date)
	{
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByAbortionStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByAbortionDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByAbortionBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByAbortionFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByAbortionFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
		
	
	}

	private Long getEclampsia(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByEclampsiaStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByEclampsiaDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByEclampsiaBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByEclampsiaFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByEclampsiaFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
	}

	private Long getSepsis(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) {
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySepsisStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySepsisDistrictAdmissionDate(state,district,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySepsisBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findBySepsisFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findBySepsisFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
	}

	private Long getPph(String state, String district, String block, String facilitytype, String facility,
			String year, String month, String date) 
	{
		Long total = null;

		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByPphStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByPphDistrictAdmissionDate(state,district,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByPphBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByPphFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByPphFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
	}
	

	

	private Long getAph(String state, String district, String block, String facilitytype, String facility,
			String year, String month,String date) 
	{
		Long total = null;
		
		if(state.length()>0 && district.length()==0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByStateAdmissionDate(state,date);
		}
		if(state.length()>0 && district.length()>0 && block.length()==0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByDistrictAdmissionDate(state,district,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()==0 && facility.length()==0)
		{
			total=icumpreportRepository.findByBlockAdmissionDate(state,district,block,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()==0)
		{
			total=icumpreportRepository.findByFacilityTypeAdmissionDate(state,district,block,facilitytype,date);
			System.out.println("total----"+total);

		}
		if(state.length()>0 && district.length()>0 && block.length()>0 && facilitytype.length()>0 && facility.length()>0)
		{
			total=icumpreportRepository.findByFacilityAdmissionDate(state,district,block,facilitytype,facility,date);
			System.out.println("total----"+total);

		}
		
		return total;
	}
	
	

}
