package com.dhanush.infotech.project.GOIICU.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "goi_icu")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt" , "updatedAt" }, allowGetters = false)
public class GoiIcu {
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private Laboratory labor;
	
	
	public Laboratory getLabor() {
		return labor;
	}
	public void setLabor(Laboratory labor) {
		this.labor = labor;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
			private Long id;
			private String status;
		    public  String  uniqueid;
		    public  String  state;
		    public String getStatus() {
				return status;
			}
			public void setStatus(String status) {
				this.status = status;
			}
			public String   provisionalDiagnosis ;
			public String   bedNo                ;
			public String   referredIn           ;
			public String	nameOfFacility1      ;
			public String   reasonforReferral    ;
			public String   placeOfDelivery      ;
			 public String    birthDate          ;
			 public String    birthTime          ;
			public String  district;
		    public  String  block;
		    public  String  facility;
		    public  String  facilityType;
		    public  String  mctsrchnumber;
		    public  String  facilityName;
			public  String  facilityTypeName;
		    public  String  stateName;
		    public  String  obsicuregnumber;
		    public  String  blockname;
		    public  String  bplstatus;
		    public  String  districtname;
		    public  String  aadharnumber;
		    public  String  contactnumber;
		    public  String  name;
			public  String  age;
			public  String  wo;
			public  String  address;
			public  String  patientcontactno;
			public  String  caste;
			public  String  religion;
			public  String  admissiondate;
			public  String  admissiontime;
			public  String  deliverydate;
			public  String  obstetricdetails;
			public  String  deliverytime;
		    public  String  gravida;
		    public  String  parity;
		    public  String  abortion;
		    public  String  livingchildren;
		    public  String  lmp;
		    public  String  edd;
		    public  String  numberofanc;
		    public  String  gestationalage;
		    public  String  labour;
		    public  String  presentation;
		    public  String  uterinetenderness;
		    public  String  fhr;
		    public  String  iffhryes;
		    public  String  amnioticfluid;
		    public  String  courseoflabour;
		    public  String  typeofdelivery;
		    public  String  aph;
		    public  String  pph;
		    public  String  pvfoulsmelling;
		    public  String  indiforcaesarean;
		    public  String  delivattendedby;
		    public  String  antenatalsteroids;
		    public  String  ifyes;
		    public  String  noofdoses;
		    public  String  magnesiumsulphate;
		    public  String  ifyesdose;
		    public  String  timeoflastdoses;
		   public   String  hdu_admission;
		   public    String     icu_admission;
		   public    String     hemorrhage_hdu;
		   public    String     hemorrhage_icu;
		   public    String     hemorrhage_others;
		   public    String     hypertensive_disorders_hdu;
		   public    String     hypertensive_disorders_icu;
		   public    String     sepsis_hdu;
		   public    String     sepsis_icu;
		   public    String     sepsis_others;
		   public    String     renal_dysfunction_hdu;
		   public    String     renal_dysfunction_icu;
		   public    String     jaundice_in_pregnancy_hdu;
		   public    String     jaundice_in_pregnancy_icu;
		   public    String     jaundice_in_pregnancy_others;
		   public    String     coagulation_system_hdu;
		   public    String     coagulation_system_icu;
		   public    String     abnormal_vitals_hdu;
		   public    String     abnormal_vitals_icu;
		   public    String     abg_abnormalities_hdu;
		   public    String     abg_abnormalities_icu;
		   public    String     electrolyte_disturbances_hdu;
		   public    String     electrolyte_disturbances_icu;
		   public    String     medical_disorders_hdu;
		   public    String     medical_disorders_icu;
		   public    String     medical_disorders_others10;
		   public    String     cause_of_admission;
		    
		    
		    
		    
		    
		    
		    public  String  causeofadmission;
		    public  String  pastmedicalhistory;
		    public  String  pastsurgicalhistory;
		    public  String  familyhistory;
		    public  String  outcomeofbirth;
		    public  String  birthweight;
			public  String  multiplebirths;
			public  String  ifyesnumber;
			public  String  sexofbaby;
			public  String  resurequired;
		    public  String  weeksofgestation;
		    public  String  sncuadmission;
		    public  String  vitaminKgiven;
		    public  String  bfedwithin1hour;
		    public  String  generalcondition;
		    public  String  height;
		    public  String  weight;
		    public  String  bmi;
		    public  String  heartrate;
		    public  String  bloodpressure;
		    public  String  spo2;
		    public  String  respiratoryrate;
		    public  String  temperature;
		    public  String  jvp;
		    public  String  pallor;
		    public  String  icterus;
		    public  String  cyanosis;
		    public  String  oedema;
		    public  String  gexothers;
		    public  String  respiratorysystem;
		    public  String  cardiovascularsystem;
		    public  String  centralnervoussystem;
		    public  String  perabdomenexamination;
		    public  String  perspeculumvaginal ;
		    public  String  anyothers;
		    public  String  respiratorysystemtext;
		    public  String  cardiosystemtext;
		    public  String  centralsystemtext;
		    public  String  perabdomentext;
		    public  String  perspeculumtext ;
		    public  String  nutrition;
		    public  String  investigations;
		    public  String  medications;
		    public  String  procedures;
		    public  String  monitoringschedule;
		    public  String  multidisciplreview;
		    public  String  planfor24hours;
		    public  String  doctornamesign;
		    public  String  respiration;
		    public  String  coagulation;
		    public  String  liver;
		    public  String  cardiovascular;
		    public  String  cns;
		    public  String  renal;
		    
			public Long getId() {
				return id;
			}
			public String getProvisionalDiagnosis() {
				return provisionalDiagnosis;
			}
			public void setProvisionalDiagnosis(String provisionalDiagnosis) {
				this.provisionalDiagnosis = provisionalDiagnosis;
			}
			public String getBedNo() {
				return bedNo;
			}
			public void setBedNo(String bedNo) {
				this.bedNo = bedNo;
			}
			public String getReferredIn() {
				return referredIn;
			}
			public void setReferredIn(String referredIn) {
				this.referredIn = referredIn;
			}
			public String getNameOfFacility1() {
				return nameOfFacility1;
			}
			public void setNameOfFacility1(String nameOfFacility1) {
				this.nameOfFacility1 = nameOfFacility1;
			}
			public String getReasonforReferral() {
				return reasonforReferral;
			}
			public void setReasonforReferral(String reasonforReferral) {
				this.reasonforReferral = reasonforReferral;
			}
			public String getPlaceOfDelivery() {
				return placeOfDelivery;
			}
			public void setPlaceOfDelivery(String placeOfDelivery) {
				this.placeOfDelivery = placeOfDelivery;
			}
			public String getBirthDate() {
				return birthDate;
			}
			public void setBirthDate(String birthDate) {
				this.birthDate = birthDate;
			}
			public String getBirthTime() {
				return birthTime;
			}
			public void setBirthTime(String birthTime) {
				this.birthTime = birthTime;
			}
			public void setId(Long id) {
				this.id = id;
			}
			public String getUniqueid() {
				return uniqueid;
			}
			public void setUniqueid(String uniqueid) {
				this.uniqueid = uniqueid;
			}
			public String getState() {
				return state;
			}
			public void setState(String state) {
				this.state = state;
			}
			public String getDistrict() {
				return district;
			}
			public void setDistrict(String district) {
				this.district = district;
			}
			public String getBlock() {
				return block;
			}
			public void setBlock(String block) {
				this.block = block;
			}
			public String getFacility() {
				return facility;
			}
			public void setFacility(String facility) {
				this.facility = facility;
			}
			public String getFacilityType() {
				return facilityType;
			}
			public void setFacilityType(String facilityType) {
				this.facilityType = facilityType;
			}
			public String getMctsrchnumber() {
				return mctsrchnumber;
			}
			public void setMctsrchnumber(String mctsrchnumber) {
				this.mctsrchnumber = mctsrchnumber;
			}
			public String getObsicuregnumber() {
				return obsicuregnumber;
			}
			public void setObsicuregnumber(String obsicuregnumber) {
				this.obsicuregnumber = obsicuregnumber;
			}
			public String getBlockname() {
				return blockname;
			}
			public void setBlockname(String blockname) {
				this.blockname = blockname;
			}
			public String getBplstatus() {
				return bplstatus;
			}
			public void setBplstatus(String bplstatus) {
				this.bplstatus = bplstatus;
			}
			public String getDistrictname() {
				return districtname;
			}
			public void setDistrictname(String districtname) {
				this.districtname = districtname;
			}
			public String getAadharnumber() {
				return aadharnumber;
			}
			public void setAadharnumber(String aadharnumber) {
				this.aadharnumber = aadharnumber;
			}
			public String getContactnumber() {
				return contactnumber;
			}
			public void setContactnumber(String contactnumber) {
				this.contactnumber = contactnumber;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public String getAge() {
				return age;
			}
			public void setAge(String age) {
				this.age = age;
			}
			public String getWo() {
				return wo;
			}
			public void setWo(String wo) {
				this.wo = wo;
			}
			public String getAddress() {
				return address;
			}
			public void setAddress(String address) {
				this.address = address;
			}
			public String getPatientcontactno() {
				return patientcontactno;
			}
			public void setPatientcontactno(String patientcontactno) {
				this.patientcontactno = patientcontactno;
			}
			public String getCaste() {
				return caste;
			}
			public void setCaste(String caste) {
				this.caste = caste;
			}
			public String getReligion() {
				return religion;
			}
			public void setReligion(String religion) {
				this.religion = religion;
			}
			public String getAdmissiondate() {
				return admissiondate;
			}
			public void setAdmissiondate(String admissiondate) {
				this.admissiondate = admissiondate;
			}
			public String getAdmissiontime() {
				return admissiontime;
			}
			public void setAdmissiontime(String admissiontime) {
				this.admissiontime = admissiontime;
			}
			public String getDeliverydate() {
				return deliverydate;
			}
			public void setDeliverydate(String deliverydate) {
				this.deliverydate = deliverydate;
			}
			public String getObstetricdetails() {
				return obstetricdetails;
			}
			public void setObstetricdetails(String obstetricdetails) {
				this.obstetricdetails = obstetricdetails;
			}
			public String getDeliverytime() {
				return deliverytime;
			}
			public void setDeliverytime(String deliverytime) {
				this.deliverytime = deliverytime;
			}
			public String getGravida() {
				return gravida;
			}
			public void setGravida(String gravida) {
				this.gravida = gravida;
			}
			public String getParity() {
				return parity;
			}
			public void setParity(String parity) {
				this.parity = parity;
			}
			public String getAbortion() {
				return abortion;
			}
			public void setAbortion(String abortion) {
				this.abortion = abortion;
			}
			public String getLivingchildren() {
				return livingchildren;
			}
			public void setLivingchildren(String livingchildren) {
				this.livingchildren = livingchildren;
			}
			public String getLmp() {
				return lmp;
			}
			public void setLmp(String lmp) {
				this.lmp = lmp;
			}
			public String getEdd() {
				return edd;
			}
			public void setEdd(String edd) {
				this.edd = edd;
			}
			public String getNumberofanc() {
				return numberofanc;
			}
			public void setNumberofanc(String numberofanc) {
				this.numberofanc = numberofanc;
			}
			public String getGestationalage() {
				return gestationalage;
			}
			public void setGestationalage(String gestationalage) {
				this.gestationalage = gestationalage;
			}
			public String getLabour() {
				return labour;
			}
			public void setLabour(String labour) {
				this.labour = labour;
			}
			public String getPresentation() {
				return presentation;
			}
			public void setPresentation(String presentation) {
				this.presentation = presentation;
			}
			public String getUterinetenderness() {
				return uterinetenderness;
			}
			public void setUterinetenderness(String uterinetenderness) {
				this.uterinetenderness = uterinetenderness;
			}
			public String getFhr() {
				return fhr;
			}
			public void setFhr(String fhr) {
				this.fhr = fhr;
			}
			public String getIffhryes() {
				return iffhryes;
			}
			public void setIffhryes(String iffhryes) {
				this.iffhryes = iffhryes;
			}
			public String getAmnioticfluid() {
				return amnioticfluid;
			}
			public void setAmnioticfluid(String amnioticfluid) {
				this.amnioticfluid = amnioticfluid;
			}
			public String getCourseoflabour() {
				return courseoflabour;
			}
			public void setCourseoflabour(String courseoflabour) {
				this.courseoflabour = courseoflabour;
			}
			public String getTypeofdelivery() {
				return typeofdelivery;
			}
			public void setTypeofdelivery(String typeofdelivery) {
				this.typeofdelivery = typeofdelivery;
			}
			public String getAph() {
				return aph;
			}
			public void setAph(String aph) {
				this.aph = aph;
			}
			public String getPph() {
				return pph;
			}
			public void setPph(String pph) {
				this.pph = pph;
			}
			public String getPvfoulsmelling() {
				return pvfoulsmelling;
			}
			public void setPvfoulsmelling(String pvfoulsmelling) {
				this.pvfoulsmelling = pvfoulsmelling;
			}
			public String getIndiforcaesarean() {
				return indiforcaesarean;
			}
			public void setIndiforcaesarean(String indiforcaesarean) {
				this.indiforcaesarean = indiforcaesarean;
			}
			public String getDelivattendedby() {
				return delivattendedby;
			}
			public void setDelivattendedby(String delivattendedby) {
				this.delivattendedby = delivattendedby;
			}
			public String getAntenatalsteroids() {
				return antenatalsteroids;
			}
			public void setAntenatalsteroids(String antenatalsteroids) {
				this.antenatalsteroids = antenatalsteroids;
			}
			public String getIfyes() {
				return ifyes;
			}
			public void setIfyes(String ifyes) {
				this.ifyes = ifyes;
			}
			public String getNoofdoses() {
				return noofdoses;
			}
			public void setNoofdoses(String noofdoses) {
				this.noofdoses = noofdoses;
			}
			public String getMagnesiumsulphate() {
				return magnesiumsulphate;
			}
			public void setMagnesiumsulphate(String magnesiumsulphate) {
				this.magnesiumsulphate = magnesiumsulphate;
			}
			public String getIfyesdose() {
				return ifyesdose;
			}
			public void setIfyesdose(String ifyesdose) {
				this.ifyesdose = ifyesdose;
			}
			public String getTimeoflastdoses() {
				return timeoflastdoses;
			}
			public void setTimeoflastdoses(String timeoflastdoses) {
				this.timeoflastdoses = timeoflastdoses;
			}
			
		
			public String getFacilityName() {
				return facilityName;
			}
			public void setFacilityName(String facilityName) {
				this.facilityName = facilityName;
			}
			public String getFacilityTypeName() {
				return facilityTypeName;
			}
			public void setFacilityTypeName(String facilityTypeName) {
				this.facilityTypeName = facilityTypeName;
			}
			public String getStateName() {
				return stateName;
			}
			public void setStateName(String stateName) {
				this.stateName = stateName;
			}
			
			
			public String getHdu_admission() {
				return hdu_admission;
			}
			public void setHdu_admission(String hdu_admission) {
				this.hdu_admission = hdu_admission;
			}
			public String getIcu_admission() {
				return icu_admission;
			}
			public void setIcu_admission(String icu_admission) {
				this.icu_admission = icu_admission;
			}
			public String getHemorrhage_hdu() {
				return hemorrhage_hdu;
			}
			public void setHemorrhage_hdu(String hemorrhage_hdu) {
				this.hemorrhage_hdu = hemorrhage_hdu;
			}
			public String getHemorrhage_icu() {
				return hemorrhage_icu;
			}
			public void setHemorrhage_icu(String hemorrhage_icu) {
				this.hemorrhage_icu = hemorrhage_icu;
			}
			public String getHemorrhage_others() {
				return hemorrhage_others;
			}
			public void setHemorrhage_others(String hemorrhage_others) {
				this.hemorrhage_others = hemorrhage_others;
			}
			public String getHypertensive_disorders_hdu() {
				return hypertensive_disorders_hdu;
			}
			public void setHypertensive_disorders_hdu(String hypertensive_disorders_hdu) {
				this.hypertensive_disorders_hdu = hypertensive_disorders_hdu;
			}
			public String getHypertensive_disorders_icu() {
				return hypertensive_disorders_icu;
			}
			public void setHypertensive_disorders_icu(String hypertensive_disorders_icu) {
				this.hypertensive_disorders_icu = hypertensive_disorders_icu;
			}
			public String getSepsis_hdu() {
				return sepsis_hdu;
			}
			public void setSepsis_hdu(String sepsis_hdu) {
				this.sepsis_hdu = sepsis_hdu;
			}
			public String getSepsis_icu() {
				return sepsis_icu;
			}
			public void setSepsis_icu(String sepsis_icu) {
				this.sepsis_icu = sepsis_icu;
			}
			public String getSepsis_others() {
				return sepsis_others;
			}
			public void setSepsis_others(String sepsis_others) {
				this.sepsis_others = sepsis_others;
			}
			public String getRenal_dysfunction_hdu() {
				return renal_dysfunction_hdu;
			}
			public void setRenal_dysfunction_hdu(String renal_dysfunction_hdu) {
				this.renal_dysfunction_hdu = renal_dysfunction_hdu;
			}
			public String getRenal_dysfunction_icu() {
				return renal_dysfunction_icu;
			}
			public void setRenal_dysfunction_icu(String renal_dysfunction_icu) {
				this.renal_dysfunction_icu = renal_dysfunction_icu;
			}
			public String getJaundice_in_pregnancy_hdu() {
				return jaundice_in_pregnancy_hdu;
			}
			public void setJaundice_in_pregnancy_hdu(String jaundice_in_pregnancy_hdu) {
				this.jaundice_in_pregnancy_hdu = jaundice_in_pregnancy_hdu;
			}
			public String getJaundice_in_pregnancy_icu() {
				return jaundice_in_pregnancy_icu;
			}
			public void setJaundice_in_pregnancy_icu(String jaundice_in_pregnancy_icu) {
				this.jaundice_in_pregnancy_icu = jaundice_in_pregnancy_icu;
			}
			public String getJaundice_in_pregnancy_others() {
				return jaundice_in_pregnancy_others;
			}
			public void setJaundice_in_pregnancy_others(String jaundice_in_pregnancy_others) {
				this.jaundice_in_pregnancy_others = jaundice_in_pregnancy_others;
			}
			public String getCoagulation_system_hdu() {
				return coagulation_system_hdu;
			}
			public void setCoagulation_system_hdu(String coagulation_system_hdu) {
				this.coagulation_system_hdu = coagulation_system_hdu;
			}
			public String getCoagulation_system_icu() {
				return coagulation_system_icu;
			}
			public void setCoagulation_system_icu(String coagulation_system_icu) {
				this.coagulation_system_icu = coagulation_system_icu;
			}
			public String getAbnormal_vitals_hdu() {
				return abnormal_vitals_hdu;
			}
			public void setAbnormal_vitals_hdu(String abnormal_vitals_hdu) {
				this.abnormal_vitals_hdu = abnormal_vitals_hdu;
			}
			public String getAbnormal_vitals_icu() {
				return abnormal_vitals_icu;
			}
			public void setAbnormal_vitals_icu(String abnormal_vitals_icu) {
				this.abnormal_vitals_icu = abnormal_vitals_icu;
			}
			public String getAbg_abnormalities_hdu() {
				return abg_abnormalities_hdu;
			}
			public void setAbg_abnormalities_hdu(String abg_abnormalities_hdu) {
				this.abg_abnormalities_hdu = abg_abnormalities_hdu;
			}
			public String getAbg_abnormalities_icu() {
				return abg_abnormalities_icu;
			}
			public void setAbg_abnormalities_icu(String abg_abnormalities_icu) {
				this.abg_abnormalities_icu = abg_abnormalities_icu;
			}
			public String getElectrolyte_disturbances_hdu() {
				return electrolyte_disturbances_hdu;
			}
			public void setElectrolyte_disturbances_hdu(String electrolyte_disturbances_hdu) {
				this.electrolyte_disturbances_hdu = electrolyte_disturbances_hdu;
			}
			public String getElectrolyte_disturbances_icu() {
				return electrolyte_disturbances_icu;
			}
			public void setElectrolyte_disturbances_icu(String electrolyte_disturbances_icu) {
				this.electrolyte_disturbances_icu = electrolyte_disturbances_icu;
			}
			public String getMedical_disorders_hdu() {
				return medical_disorders_hdu;
			}
			public void setMedical_disorders_hdu(String medical_disorders_hdu) {
				this.medical_disorders_hdu = medical_disorders_hdu;
			}
			public String getMedical_disorders_icu() {
				return medical_disorders_icu;
			}
			public void setMedical_disorders_icu(String medical_disorders_icu) {
				this.medical_disorders_icu = medical_disorders_icu;
			}
			public String getMedical_disorders_others10() {
				return medical_disorders_others10;
			}
			public void setMedical_disorders_others10(String medical_disorders_others10) {
				this.medical_disorders_others10 = medical_disorders_others10;
			}
			public String getCause_of_admission() {
				return cause_of_admission;
			}
			public void setCause_of_admission(String cause_of_admission) {
				this.cause_of_admission = cause_of_admission;
			}
			public String getCauseofadmission() {
				return causeofadmission;
			}
			public void setCauseofadmission(String causeofadmission) {
				this.causeofadmission = causeofadmission;
			}
			public String getPastmedicalhistory() {
				return pastmedicalhistory;
			}
			public void setPastmedicalhistory(String pastmedicalhistory) {
				this.pastmedicalhistory = pastmedicalhistory;
			}
			public String getPastsurgicalhistory() {
				return pastsurgicalhistory;
			}
			public void setPastsurgicalhistory(String pastsurgicalhistory) {
				this.pastsurgicalhistory = pastsurgicalhistory;
			}
			public String getFamilyhistory() {
				return familyhistory;
			}
			public void setFamilyhistory(String familyhistory) {
				this.familyhistory = familyhistory;
			}
			public String getOutcomeofbirth() {
				return outcomeofbirth;
			}
			public void setOutcomeofbirth(String outcomeofbirth) {
				this.outcomeofbirth = outcomeofbirth;
			}
			public String getBirthweight() {
				return birthweight;
			}
			public void setBirthweight(String birthweight) {
				this.birthweight = birthweight;
			}
			public String getMultiplebirths() {
				return multiplebirths;
			}
			public void setMultiplebirths(String multiplebirths) {
				this.multiplebirths = multiplebirths;
			}
			public String getIfyesnumber() {
				return ifyesnumber;
			}
			public void setIfyesnumber(String ifyesnumber) {
				this.ifyesnumber = ifyesnumber;
			}
			public String getSexofbaby() {
				return sexofbaby;
			}
			public void setSexofbaby(String sexofbaby) {
				this.sexofbaby = sexofbaby;
			}
			public String getResurequired() {
				return resurequired;
			}
			public void setResurequired(String resurequired) {
				this.resurequired = resurequired;
			}
			public String getWeeksofgestation() {
				return weeksofgestation;
			}
			public void setWeeksofgestation(String weeksofgestation) {
				this.weeksofgestation = weeksofgestation;
			}
			public String getSncuadmission() {
				return sncuadmission;
			}
			public void setSncuadmission(String sncuadmission) {
				this.sncuadmission = sncuadmission;
			}
			public String getVitaminKgiven() {
				return vitaminKgiven;
			}
			public void setVitaminKgiven(String vitaminKgiven) {
				this.vitaminKgiven = vitaminKgiven;
			}
			public String getBfedwithin1hour() {
				return bfedwithin1hour;
			}
			public void setBfedwithin1hour(String bfedwithin1hour) {
				this.bfedwithin1hour = bfedwithin1hour;
			}
			public String getGeneralcondition() {
				return generalcondition;
			}
			public void setGeneralcondition(String generalcondition) {
				this.generalcondition = generalcondition;
			}
			public String getHeight() {
				return height;
			}
			public void setHeight(String height) {
				this.height = height;
			}
			public String getWeight() {
				return weight;
			}
			public void setWeight(String weight) {
				this.weight = weight;
			}
			public String getBmi() {
				return bmi;
			}
			public void setBmi(String bmi) {
				this.bmi = bmi;
			}
			public String getHeartrate() {
				return heartrate;
			}
			public void setHeartrate(String heartrate) {
				this.heartrate = heartrate;
			}
			public String getBloodpressure() {
				return bloodpressure;
			}
			public void setBloodpressure(String bloodpressure) {
				this.bloodpressure = bloodpressure;
			}
			public String getSpo2() {
				return spo2;
			}
			public void setSpo2(String spo2) {
				this.spo2 = spo2;
			}
			public String getRespiratoryrate() {
				return respiratoryrate;
			}
			public void setRespiratoryrate(String respiratoryrate) {
				this.respiratoryrate = respiratoryrate;
			}
			public String getTemperature() {
				return temperature;
			}
			public void setTemperature(String temperature) {
				this.temperature = temperature;
			}
			public String getJvp() {
				return jvp;
			}
			public void setJvp(String jvp) {
				this.jvp = jvp;
			}
			public String getPallor() {
				return pallor;
			}
			public void setPallor(String pallor) {
				this.pallor = pallor;
			}
			public String getIcterus() {
				return icterus;
			}
			public void setIcterus(String icterus) {
				this.icterus = icterus;
			}
			public String getCyanosis() {
				return cyanosis;
			}
			public void setCyanosis(String cyanosis) {
				this.cyanosis = cyanosis;
			}
			public String getOedema() {
				return oedema;
			}
			public void setOedema(String oedema) {
				this.oedema = oedema;
			}
			public String getGexothers() {
				return gexothers;
			}
			public void setGexothers(String gexothers) {
				this.gexothers = gexothers;
			}
			public String getRespiratorysystem() {
				return respiratorysystem;
			}
			public void setRespiratorysystem(String respiratorysystem) {
				this.respiratorysystem = respiratorysystem;
			}
			public String getCardiovascularsystem() {
				return cardiovascularsystem;
			}
			public void setCardiovascularsystem(String cardiovascularsystem) {
				this.cardiovascularsystem = cardiovascularsystem;
			}
			public String getCentralnervoussystem() {
				return centralnervoussystem;
			}
			public void setCentralnervoussystem(String centralnervoussystem) {
				this.centralnervoussystem = centralnervoussystem;
			}
			public String getPerabdomenexamination() {
				return perabdomenexamination;
			}
			public void setPerabdomenexamination(String perabdomenexamination) {
				this.perabdomenexamination = perabdomenexamination;
			}
			public String getPerspeculumvaginal() {
				return perspeculumvaginal;
			}
			public void setPerspeculumvaginal(String perspeculumvaginal) {
				this.perspeculumvaginal = perspeculumvaginal;
			}
			public String getAnyothers() {
				return anyothers;
			}
			public void setAnyothers(String anyothers) {
				this.anyothers = anyothers;
			}
			public String getRespiratorysystemtext() {
				return respiratorysystemtext;
			}
			public void setRespiratorysystemtext(String respiratorysystemtext) {
				this.respiratorysystemtext = respiratorysystemtext;
			}
			public String getCardiosystemtext() {
				return cardiosystemtext;
			}
			public void setCardiosystemtext(String cardiosystemtext) {
				this.cardiosystemtext = cardiosystemtext;
			}
			public String getCentralsystemtext() {
				return centralsystemtext;
			}
			public void setCentralsystemtext(String centralsystemtext) {
				this.centralsystemtext = centralsystemtext;
			}
			public String getPerabdomentext() {
				return perabdomentext;
			}
			public void setPerabdomentext(String perabdomentext) {
				this.perabdomentext = perabdomentext;
			}
			public String getPerspeculumtext() {
				return perspeculumtext;
			}
			public void setPerspeculumtext(String perspeculumtext) {
				this.perspeculumtext = perspeculumtext;
			}
			public String getNutrition() {
				return nutrition;
			}
			public void setNutrition(String nutrition) {
				this.nutrition = nutrition;
			}
			public String getInvestigations() {
				return investigations;
			}
			public void setInvestigations(String investigations) {
				this.investigations = investigations;
			}
			public String getMedications() {
				return medications;
			}
			public void setMedications(String medications) {
				this.medications = medications;
			}
			public String getProcedures() {
				return procedures;
			}
			public void setProcedures(String procedures) {
				this.procedures = procedures;
			}
			public String getMonitoringschedule() {
				return monitoringschedule;
			}
			public void setMonitoringschedule(String monitoringschedule) {
				this.monitoringschedule = monitoringschedule;
			}
			public String getMultidisciplreview() {
				return multidisciplreview;
			}
			public void setMultidisciplreview(String multidisciplreview) {
				this.multidisciplreview = multidisciplreview;
			}
			public String getPlanfor24hours() {
				return planfor24hours;
			}
			public void setPlanfor24hours(String planfor24hours) {
				this.planfor24hours = planfor24hours;
			}
			public String getDoctornamesign() {
				return doctornamesign;
			}
			public void setDoctornamesign(String doctornamesign) {
				this.doctornamesign = doctornamesign;
			}
			public String getRespiration() {
				return respiration;
			}
			public void setRespiration(String respiration) {
				this.respiration = respiration;
			}
			public String getCoagulation() {
				return coagulation;
			}
			public void setCoagulation(String coagulation) {
				this.coagulation = coagulation;
			}
			public String getLiver() {
				return liver;
			}
			public void setLiver(String liver) {
				this.liver = liver;
			}
			public String getCardiovascular() {
				return cardiovascular;
			}
			public void setCardiovascular(String cardiovascular) {
				this.cardiovascular = cardiovascular;
			}
			public String getCns() {
				return cns;
			}
			public void setCns(String cns) {
				this.cns = cns;
			}
			public String getRenal() {
				return renal;
			}
			public void setRenal(String renal) {
				this.renal = renal;
			}
			


}
