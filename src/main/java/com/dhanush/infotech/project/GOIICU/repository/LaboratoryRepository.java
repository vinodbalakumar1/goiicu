package com.dhanush.infotech.project.GOIICU.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dhanush.infotech.project.GOIICU.model.Laboratory;

public interface LaboratoryRepository extends JpaRepository<Laboratory, Long>{

}
