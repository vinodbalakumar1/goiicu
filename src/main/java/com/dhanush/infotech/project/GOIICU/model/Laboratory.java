package com.dhanush.infotech.project.GOIICU.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "goi_laborartory")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt" , "updatedAt" }, allowGetters = false)
public class Laboratory {
	
	
	  @OneToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
	    @JoinColumn(name = "admissionId", nullable = false)
	    private GoiIcu admission;   
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
			private Long id;
	
	                                                 
	 public  String  day1hb;                                                   
	 public  String  day1totalwbccount;                         
	 public  String  day1wbccount;                              
	 public  String  day1plateletcount;                         
	 public  String  day1peripheralsmear;                       
	 public  String  day1btctcrt;                               
	 public  String  day1ptapttinr;                             
	 public  String  day1bloodgrouprhtype;                      
	 public  String  day1sicklingtest;                          
	 public  String  day1randombloodglucose;                    
	 public  String  day1bloodureanitrogen;                     
	 public  String  day1serumcreatinine;                       
	 public  String  day1serumcalcium;                          
	 public  String  day1serumsodium;                           
	 public  String  day1serumpotassium;                        
	 public  String  day1serumalbumin;                          
	 public  String  day1serumbiliorubin;                       
	 public  String  day1sgptsgptldh;                           
	 public  String  day1serumfibrinogen;                       
	 public  String  day1ddimer;                                
	 public  String  day1urinerm;                               
	 public  String  day1occultblood;                           
	 public  String  day1bloodgastime;                          
	 public  String  day1bloodgasfio2;                          
	 public  String  day1bloodgasph;                            
	 public  String  day1bloodgaspco2;                          
	 public  String  day1bloodgaspo2;                           
	 public  String  day1bloodgashco3;                          
	 public  String  day1bloodgassatn;                          
	 public  String  day2hb;                                    
	 public  String  day2totalwbccount;                         
	 public  String  day2wbccount;                              
	 public  String  day2plateletcount;                         
	 public  String  day2peripheralsmear;                       
	 public  String  day2btctcrt;                               
	 public  String  day2ptapttinr;                             
	 public  String  day2bloodgrouprhtype;                      
	 public  String  day2sicklingtest;                          
	 public  String  day2randombloodglucose;                    
	 public  String  day2bloodureanitrogen;                     
	 public  String  day2serumcreatinine;                       
	 public  String  day2serumcalcium;                          
	 public  String  day2serumsodium;                           
	 public  String  day2serumpotassium;                        
	 public  String  day2serumalbumin;                          
	 public  String  day2serumbiliorubin;                       
	 public  String  day2sgptsgptldh;                           
	 public  String  day2serumfibrinogen;                       
	 public  String  day2ddimer;                                
	 public  String  day2urinerm;                               
	 public  String  day2occultblood;                           
	 public  String  day2bloodgastime;                          
	 public  String  day2bloodgasfio2;                          
	 public  String  day2bloodgasph;                            
	 public  String  day2bloodgaspco2;                          
	 public  String  day2bloodgaspo2;                           
	 public  String  day2bloodgashco3;                          
	 public  String  day2bloodgassatn;                          
	 public  String  day3hb;                                    
	 public  String  day3totalwbccount;                         
	 public  String  day3wbccount;                              
	 public  String  day3plateletcount;                         
	 public  String  day3peripheralsmear;                       
	 public  String  day3btctcrt;                               
	 public  String  day3ptapttinr;                             
	 public  String  day3rhtype;                                
	 public  String  day3sicklingtest;                          
	 public  String  day3randombloodglucose;                    
	 public  String  day3bloodureanitrogen;                     
	 public  String  day3serumcreatinine;                       
	 public  String  day3serumcalcium;                          
	 public  String  day3serumsodium;                           
	 public  String  day3serumpotassium;                        
	 public  String  day3serumalbumin;                          
	 public  String  day3serumbiliorubin;                       
	 public  String  day3sgptsgptldh;                           
	 public  String  day3serumfibrinogen;                       
	 public  String  day3ddimer;                                
	 public  String  day3urinerm;                               
	 public  String  day3occultblood;                           
	 public  String  day3bloodgastime;                          
	 public  String  day3bloodgasfio2;                          
	 public  String  day3bloodgasph;                            
	 public  String  day3bloodgaspco2;                          
	 public  String  day3bloodgaspo2;                           
	 public  String  day3bloodgashco3;                          
	 public  String  day3bloodgassatn;                          
	 public  String  patientname;                               
	 public  String  regno;                                     
	 public  String  doa;                                       
	 public  String  doctorincharge;                            
	 public  String  urineculture;                              
	 public  String  bloodculture;                              
	 public  String  cervicalvaginalswab;                       
	 public  String  anyother;                                  
	 public  String  hiv;                                       
	 public  String  hbsag;                                     
	 public  String  vdrl;
	public Long getId() {
		return id;
	}
	public GoiIcu getAdmission() {
		return admission;
	}
	public void setAdmission(GoiIcu admission) {
		this.admission = admission;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDay1hb() {
		return day1hb;
	}
	public void setDay1hb(String day1hb) {
		this.day1hb = day1hb;
	}
	public String getDay1totalwbccount() {
		return day1totalwbccount;
	}
	public void setDay1totalwbccount(String day1totalwbccount) {
		this.day1totalwbccount = day1totalwbccount;
	}
	public String getDay1wbccount() {
		return day1wbccount;
	}
	public void setDay1wbccount(String day1wbccount) {
		this.day1wbccount = day1wbccount;
	}
	public String getDay1plateletcount() {
		return day1plateletcount;
	}
	public void setDay1plateletcount(String day1plateletcount) {
		this.day1plateletcount = day1plateletcount;
	}
	public String getDay1peripheralsmear() {
		return day1peripheralsmear;
	}
	public void setDay1peripheralsmear(String day1peripheralsmear) {
		this.day1peripheralsmear = day1peripheralsmear;
	}
	public String getDay1btctcrt() {
		return day1btctcrt;
	}
	public void setDay1btctcrt(String day1btctcrt) {
		this.day1btctcrt = day1btctcrt;
	}
	public String getDay1ptapttinr() {
		return day1ptapttinr;
	}
	public void setDay1ptapttinr(String day1ptapttinr) {
		this.day1ptapttinr = day1ptapttinr;
	}
	public String getDay1bloodgrouprhtype() {
		return day1bloodgrouprhtype;
	}
	public void setDay1bloodgrouprhtype(String day1bloodgrouprhtype) {
		this.day1bloodgrouprhtype = day1bloodgrouprhtype;
	}
	public String getDay1sicklingtest() {
		return day1sicklingtest;
	}
	public void setDay1sicklingtest(String day1sicklingtest) {
		this.day1sicklingtest = day1sicklingtest;
	}
	public String getDay1randombloodglucose() {
		return day1randombloodglucose;
	}
	public void setDay1randombloodglucose(String day1randombloodglucose) {
		this.day1randombloodglucose = day1randombloodglucose;
	}
	public String getDay1bloodureanitrogen() {
		return day1bloodureanitrogen;
	}
	public void setDay1bloodureanitrogen(String day1bloodureanitrogen) {
		this.day1bloodureanitrogen = day1bloodureanitrogen;
	}
	public String getDay1serumcreatinine() {
		return day1serumcreatinine;
	}
	public void setDay1serumcreatinine(String day1serumcreatinine) {
		this.day1serumcreatinine = day1serumcreatinine;
	}
	public String getDay1serumcalcium() {
		return day1serumcalcium;
	}
	public void setDay1serumcalcium(String day1serumcalcium) {
		this.day1serumcalcium = day1serumcalcium;
	}
	public String getDay1serumsodium() {
		return day1serumsodium;
	}
	public void setDay1serumsodium(String day1serumsodium) {
		this.day1serumsodium = day1serumsodium;
	}
	public String getDay1serumpotassium() {
		return day1serumpotassium;
	}
	public void setDay1serumpotassium(String day1serumpotassium) {
		this.day1serumpotassium = day1serumpotassium;
	}
	public String getDay1serumalbumin() {
		return day1serumalbumin;
	}
	public void setDay1serumalbumin(String day1serumalbumin) {
		this.day1serumalbumin = day1serumalbumin;
	}
	public String getDay1serumbiliorubin() {
		return day1serumbiliorubin;
	}
	public void setDay1serumbiliorubin(String day1serumbiliorubin) {
		this.day1serumbiliorubin = day1serumbiliorubin;
	}
	public String getDay1sgptsgptldh() {
		return day1sgptsgptldh;
	}
	public void setDay1sgptsgptldh(String day1sgptsgptldh) {
		this.day1sgptsgptldh = day1sgptsgptldh;
	}
	public String getDay1serumfibrinogen() {
		return day1serumfibrinogen;
	}
	public void setDay1serumfibrinogen(String day1serumfibrinogen) {
		this.day1serumfibrinogen = day1serumfibrinogen;
	}
	public String getDay1ddimer() {
		return day1ddimer;
	}
	public void setDay1ddimer(String day1ddimer) {
		this.day1ddimer = day1ddimer;
	}
	public String getDay1urinerm() {
		return day1urinerm;
	}
	public void setDay1urinerm(String day1urinerm) {
		this.day1urinerm = day1urinerm;
	}
	public String getDay1occultblood() {
		return day1occultblood;
	}
	public void setDay1occultblood(String day1occultblood) {
		this.day1occultblood = day1occultblood;
	}
	public String getDay1bloodgastime() {
		return day1bloodgastime;
	}
	public void setDay1bloodgastime(String day1bloodgastime) {
		this.day1bloodgastime = day1bloodgastime;
	}
	public String getDay1bloodgasfio2() {
		return day1bloodgasfio2;
	}
	public void setDay1bloodgasfio2(String day1bloodgasfio2) {
		this.day1bloodgasfio2 = day1bloodgasfio2;
	}
	public String getDay1bloodgasph() {
		return day1bloodgasph;
	}
	public void setDay1bloodgasph(String day1bloodgasph) {
		this.day1bloodgasph = day1bloodgasph;
	}
	public String getDay1bloodgaspco2() {
		return day1bloodgaspco2;
	}
	public void setDay1bloodgaspco2(String day1bloodgaspco2) {
		this.day1bloodgaspco2 = day1bloodgaspco2;
	}
	public String getDay1bloodgaspo2() {
		return day1bloodgaspo2;
	}
	public void setDay1bloodgaspo2(String day1bloodgaspo2) {
		this.day1bloodgaspo2 = day1bloodgaspo2;
	}
	public String getDay1bloodgashco3() {
		return day1bloodgashco3;
	}
	public void setDay1bloodgashco3(String day1bloodgashco3) {
		this.day1bloodgashco3 = day1bloodgashco3;
	}
	public String getDay1bloodgassatn() {
		return day1bloodgassatn;
	}
	public void setDay1bloodgassatn(String day1bloodgassatn) {
		this.day1bloodgassatn = day1bloodgassatn;
	}
	public String getDay2hb() {
		return day2hb;
	}
	public void setDay2hb(String day2hb) {
		this.day2hb = day2hb;
	}
	public String getDay2totalwbccount() {
		return day2totalwbccount;
	}
	public void setDay2totalwbccount(String day2totalwbccount) {
		this.day2totalwbccount = day2totalwbccount;
	}
	public String getDay2wbccount() {
		return day2wbccount;
	}
	public void setDay2wbccount(String day2wbccount) {
		this.day2wbccount = day2wbccount;
	}
	public String getDay2plateletcount() {
		return day2plateletcount;
	}
	public void setDay2plateletcount(String day2plateletcount) {
		this.day2plateletcount = day2plateletcount;
	}
	public String getDay2peripheralsmear() {
		return day2peripheralsmear;
	}
	public void setDay2peripheralsmear(String day2peripheralsmear) {
		this.day2peripheralsmear = day2peripheralsmear;
	}
	public String getDay2btctcrt() {
		return day2btctcrt;
	}
	public void setDay2btctcrt(String day2btctcrt) {
		this.day2btctcrt = day2btctcrt;
	}
	public String getDay2ptapttinr() {
		return day2ptapttinr;
	}
	public void setDay2ptapttinr(String day2ptapttinr) {
		this.day2ptapttinr = day2ptapttinr;
	}
	public String getDay2bloodgrouprhtype() {
		return day2bloodgrouprhtype;
	}
	public void setDay2bloodgrouprhtype(String day2bloodgrouprhtype) {
		this.day2bloodgrouprhtype = day2bloodgrouprhtype;
	}
	public String getDay2sicklingtest() {
		return day2sicklingtest;
	}
	public void setDay2sicklingtest(String day2sicklingtest) {
		this.day2sicklingtest = day2sicklingtest;
	}
	public String getDay2randombloodglucose() {
		return day2randombloodglucose;
	}
	public void setDay2randombloodglucose(String day2randombloodglucose) {
		this.day2randombloodglucose = day2randombloodglucose;
	}
	public String getDay2bloodureanitrogen() {
		return day2bloodureanitrogen;
	}
	public void setDay2bloodureanitrogen(String day2bloodureanitrogen) {
		this.day2bloodureanitrogen = day2bloodureanitrogen;
	}
	public String getDay2serumcreatinine() {
		return day2serumcreatinine;
	}
	public void setDay2serumcreatinine(String day2serumcreatinine) {
		this.day2serumcreatinine = day2serumcreatinine;
	}
	public String getDay2serumcalcium() {
		return day2serumcalcium;
	}
	public void setDay2serumcalcium(String day2serumcalcium) {
		this.day2serumcalcium = day2serumcalcium;
	}
	public String getDay2serumsodium() {
		return day2serumsodium;
	}
	public void setDay2serumsodium(String day2serumsodium) {
		this.day2serumsodium = day2serumsodium;
	}
	public String getDay2serumpotassium() {
		return day2serumpotassium;
	}
	public void setDay2serumpotassium(String day2serumpotassium) {
		this.day2serumpotassium = day2serumpotassium;
	}
	public String getDay2serumalbumin() {
		return day2serumalbumin;
	}
	public void setDay2serumalbumin(String day2serumalbumin) {
		this.day2serumalbumin = day2serumalbumin;
	}
	public String getDay2serumbiliorubin() {
		return day2serumbiliorubin;
	}
	public void setDay2serumbiliorubin(String day2serumbiliorubin) {
		this.day2serumbiliorubin = day2serumbiliorubin;
	}
	public String getDay2sgptsgptldh() {
		return day2sgptsgptldh;
	}
	public void setDay2sgptsgptldh(String day2sgptsgptldh) {
		this.day2sgptsgptldh = day2sgptsgptldh;
	}
	public String getDay2serumfibrinogen() {
		return day2serumfibrinogen;
	}
	public void setDay2serumfibrinogen(String day2serumfibrinogen) {
		this.day2serumfibrinogen = day2serumfibrinogen;
	}
	public String getDay2ddimer() {
		return day2ddimer;
	}
	public void setDay2ddimer(String day2ddimer) {
		this.day2ddimer = day2ddimer;
	}
	public String getDay2urinerm() {
		return day2urinerm;
	}
	public void setDay2urinerm(String day2urinerm) {
		this.day2urinerm = day2urinerm;
	}
	public String getDay2occultblood() {
		return day2occultblood;
	}
	public void setDay2occultblood(String day2occultblood) {
		this.day2occultblood = day2occultblood;
	}
	public String getDay2bloodgastime() {
		return day2bloodgastime;
	}
	public void setDay2bloodgastime(String day2bloodgastime) {
		this.day2bloodgastime = day2bloodgastime;
	}
	public String getDay2bloodgasfio2() {
		return day2bloodgasfio2;
	}
	public void setDay2bloodgasfio2(String day2bloodgasfio2) {
		this.day2bloodgasfio2 = day2bloodgasfio2;
	}
	public String getDay2bloodgasph() {
		return day2bloodgasph;
	}
	public void setDay2bloodgasph(String day2bloodgasph) {
		this.day2bloodgasph = day2bloodgasph;
	}
	public String getDay2bloodgaspco2() {
		return day2bloodgaspco2;
	}
	public void setDay2bloodgaspco2(String day2bloodgaspco2) {
		this.day2bloodgaspco2 = day2bloodgaspco2;
	}
	public String getDay2bloodgaspo2() {
		return day2bloodgaspo2;
	}
	public void setDay2bloodgaspo2(String day2bloodgaspo2) {
		this.day2bloodgaspo2 = day2bloodgaspo2;
	}
	public String getDay2bloodgashco3() {
		return day2bloodgashco3;
	}
	public void setDay2bloodgashco3(String day2bloodgashco3) {
		this.day2bloodgashco3 = day2bloodgashco3;
	}
	public String getDay2bloodgassatn() {
		return day2bloodgassatn;
	}
	public void setDay2bloodgassatn(String day2bloodgassatn) {
		this.day2bloodgassatn = day2bloodgassatn;
	}
	public String getDay3hb() {
		return day3hb;
	}
	public void setDay3hb(String day3hb) {
		this.day3hb = day3hb;
	}
	public String getDay3totalwbccount() {
		return day3totalwbccount;
	}
	public void setDay3totalwbccount(String day3totalwbccount) {
		this.day3totalwbccount = day3totalwbccount;
	}
	public String getDay3wbccount() {
		return day3wbccount;
	}
	public void setDay3wbccount(String day3wbccount) {
		this.day3wbccount = day3wbccount;
	}
	public String getDay3plateletcount() {
		return day3plateletcount;
	}
	public void setDay3plateletcount(String day3plateletcount) {
		this.day3plateletcount = day3plateletcount;
	}
	public String getDay3peripheralsmear() {
		return day3peripheralsmear;
	}
	public void setDay3peripheralsmear(String day3peripheralsmear) {
		this.day3peripheralsmear = day3peripheralsmear;
	}
	public String getDay3btctcrt() {
		return day3btctcrt;
	}
	public void setDay3btctcrt(String day3btctcrt) {
		this.day3btctcrt = day3btctcrt;
	}
	public String getDay3ptapttinr() {
		return day3ptapttinr;
	}
	public void setDay3ptapttinr(String day3ptapttinr) {
		this.day3ptapttinr = day3ptapttinr;
	}
	public String getDay3rhtype() {
		return day3rhtype;
	}
	public void setDay3rhtype(String day3rhtype) {
		this.day3rhtype = day3rhtype;
	}
	public String getDay3sicklingtest() {
		return day3sicklingtest;
	}
	public void setDay3sicklingtest(String day3sicklingtest) {
		this.day3sicklingtest = day3sicklingtest;
	}
	public String getDay3randombloodglucose() {
		return day3randombloodglucose;
	}
	public void setDay3randombloodglucose(String day3randombloodglucose) {
		this.day3randombloodglucose = day3randombloodglucose;
	}
	public String getDay3bloodureanitrogen() {
		return day3bloodureanitrogen;
	}
	public void setDay3bloodureanitrogen(String day3bloodureanitrogen) {
		this.day3bloodureanitrogen = day3bloodureanitrogen;
	}
	public String getDay3serumcreatinine() {
		return day3serumcreatinine;
	}
	public void setDay3serumcreatinine(String day3serumcreatinine) {
		this.day3serumcreatinine = day3serumcreatinine;
	}
	public String getDay3serumcalcium() {
		return day3serumcalcium;
	}
	public void setDay3serumcalcium(String day3serumcalcium) {
		this.day3serumcalcium = day3serumcalcium;
	}
	public String getDay3serumsodium() {
		return day3serumsodium;
	}
	public void setDay3serumsodium(String day3serumsodium) {
		this.day3serumsodium = day3serumsodium;
	}
	public String getDay3serumpotassium() {
		return day3serumpotassium;
	}
	public void setDay3serumpotassium(String day3serumpotassium) {
		this.day3serumpotassium = day3serumpotassium;
	}
	public String getDay3serumalbumin() {
		return day3serumalbumin;
	}
	public void setDay3serumalbumin(String day3serumalbumin) {
		this.day3serumalbumin = day3serumalbumin;
	}
	public String getDay3serumbiliorubin() {
		return day3serumbiliorubin;
	}
	public void setDay3serumbiliorubin(String day3serumbiliorubin) {
		this.day3serumbiliorubin = day3serumbiliorubin;
	}
	public String getDay3sgptsgptldh() {
		return day3sgptsgptldh;
	}
	public void setDay3sgptsgptldh(String day3sgptsgptldh) {
		this.day3sgptsgptldh = day3sgptsgptldh;
	}
	public String getDay3serumfibrinogen() {
		return day3serumfibrinogen;
	}
	public void setDay3serumfibrinogen(String day3serumfibrinogen) {
		this.day3serumfibrinogen = day3serumfibrinogen;
	}
	public String getDay3ddimer() {
		return day3ddimer;
	}
	public void setDay3ddimer(String day3ddimer) {
		this.day3ddimer = day3ddimer;
	}
	public String getDay3urinerm() {
		return day3urinerm;
	}
	public void setDay3urinerm(String day3urinerm) {
		this.day3urinerm = day3urinerm;
	}
	public String getDay3occultblood() {
		return day3occultblood;
	}
	public void setDay3occultblood(String day3occultblood) {
		this.day3occultblood = day3occultblood;
	}
	public String getDay3bloodgastime() {
		return day3bloodgastime;
	}
	public void setDay3bloodgastime(String day3bloodgastime) {
		this.day3bloodgastime = day3bloodgastime;
	}
	public String getDay3bloodgasfio2() {
		return day3bloodgasfio2;
	}
	public void setDay3bloodgasfio2(String day3bloodgasfio2) {
		this.day3bloodgasfio2 = day3bloodgasfio2;
	}
	public String getDay3bloodgasph() {
		return day3bloodgasph;
	}
	public void setDay3bloodgasph(String day3bloodgasph) {
		this.day3bloodgasph = day3bloodgasph;
	}
	public String getDay3bloodgaspco2() {
		return day3bloodgaspco2;
	}
	public void setDay3bloodgaspco2(String day3bloodgaspco2) {
		this.day3bloodgaspco2 = day3bloodgaspco2;
	}
	public String getDay3bloodgaspo2() {
		return day3bloodgaspo2;
	}
	public void setDay3bloodgaspo2(String day3bloodgaspo2) {
		this.day3bloodgaspo2 = day3bloodgaspo2;
	}
	public String getDay3bloodgashco3() {
		return day3bloodgashco3;
	}
	public void setDay3bloodgashco3(String day3bloodgashco3) {
		this.day3bloodgashco3 = day3bloodgashco3;
	}
	public String getDay3bloodgassatn() {
		return day3bloodgassatn;
	}
	public void setDay3bloodgassatn(String day3bloodgassatn) {
		this.day3bloodgassatn = day3bloodgassatn;
	}
	public String getPatientname() {
		return patientname;
	}
	public void setPatientname(String patientname) {
		this.patientname = patientname;
	}
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public String getDoa() {
		return doa;
	}
	public void setDoa(String doa) {
		this.doa = doa;
	}
	public String getDoctorincharge() {
		return doctorincharge;
	}
	public void setDoctorincharge(String doctorincharge) {
		this.doctorincharge = doctorincharge;
	}
	public String getUrineculture() {
		return urineculture;
	}
	public void setUrineculture(String urineculture) {
		this.urineculture = urineculture;
	}
	public String getBloodculture() {
		return bloodculture;
	}
	public void setBloodculture(String bloodculture) {
		this.bloodculture = bloodculture;
	}
	public String getCervicalvaginalswab() {
		return cervicalvaginalswab;
	}
	public void setCervicalvaginalswab(String cervicalvaginalswab) {
		this.cervicalvaginalswab = cervicalvaginalswab;
	}
	public String getAnyother() {
		return anyother;
	}
	public void setAnyother(String anyother) {
		this.anyother = anyother;
	}
	public String getHiv() {
		return hiv;
	}
	public void setHiv(String hiv) {
		this.hiv = hiv;
	}
	public String getHbsag() {
		return hbsag;
	}
	public void setHbsag(String hbsag) {
		this.hbsag = hbsag;
	}
	public String getVdrl() {
		return vdrl;
	}
	public void setVdrl(String vdrl) {
		this.vdrl = vdrl;
	}                                                     

}
