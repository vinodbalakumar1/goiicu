package com.dhanush.infotech.project.GOIICU.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dhanush.infotech.project.GOIICU.model.GoiIcu;

public interface IcuRepository extends JpaRepository<GoiIcu, Long> {



	List<GoiIcu> findByUniqueid(String uniqueNo);

	List<GoiIcu> findByMctsrchnumber(String mctsno);

	List<GoiIcu> findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndNameLike(String state, String district,
			String block, String facilityType, String facility, String beneficary);

	List<GoiIcu> findByStateAndDistrictAndBlockAndFacilityTypeAndNameLike(String state, String district, String block,
			String facilityType, String beneficary);

	List<GoiIcu> findByStateAndDistrictAndBlockAndNameLike(String state, String district, String block,
			String beneficary);

	List<GoiIcu> findByStateAndDistrictAndNameLike(String state, String district, String beneficary);

	List<GoiIcu> findByStateAndNameLike(String state, String beneficary);

	List<GoiIcu> findByStateAndAdmissiondateLike(String string, String date);

	List<GoiIcu> findByStateAndDistrictAndAdmissiondateLike(String string, String string2, String date);

	List<GoiIcu> findByStateAndDistrictAndBlockAndAdmissiondateLike(String string, String string2, String string3,
			String date);

	List<GoiIcu> findByStateAndDistrictAndBlockAndFacilityTypeAndAdmissiondateLike(String string, String string2,
			String string3, String string4, String date);

	List<GoiIcu> findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndAdmissiondateLike(String string,
			String string2, String string3, String string4, String string5, String date);
	
	
	
	
	
	
	

}
