package com.dhanush.infotech.project.GOIICU.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIICU.model.GoiIcu;
import com.dhanush.infotech.project.GOIICU.repository.IcuRepository;
import com.dhanush.infotech.project.GOIICU.repository.LaboratoryRepository;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class GetIcuDetailsController {

	@Autowired
	IcuRepository icuRepository;
	
	@Autowired
	LaboratoryRepository laboratoryRepository;
	
	@CrossOrigin
	@RequestMapping(value = "/geticudetails/{id}", method = RequestMethod.GET, produces = {
			"application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
		GoiIcu icu = icuRepository.findById(UserId).orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		JSONObject map = new JSONObject();
		map.put("state"       ,icu.getState		 ());
		map.put("district"    ,icu.getDistrict		 ());
		map.put( "block"      ,icu.getBlock		 ());
		map.put( "facility"   ,icu.getFacility		 ());
		map.put("facilityType",icu.getFacilityType	 ());
		map.put("admissionId", icu.getId	 ());
		
		org.json.JSONObject admission = new org.json.JSONObject();  
		
		admission.put("district_name",                     icu.getDistrictname      ());
		 admission.put("name_of_facility",                  icu.getFacilityName    ());
		 admission.put("block_name",                        icu.getBlockname         ());
		admission.put("mcts_rch_number",                   icu.getMctsrchnumber     ());
		 admission.put("obs_icu_reg_number",                icu.getObsicuregnumber   ());
		 admission.put("bpl_status",                        icu.getBplstatus         ());
		 admission.put("aadhar_number",                     icu.getAadharnumber      ());
		 admission.put("contact_number",                    icu.getContactnumber     ());
		 admission.put("name",                              icu.getName              ());
		 admission.put("age",                               icu.getAge               ());
		 admission.put("w_o",                               icu.getWo                ());
		 admission.put("address",                           icu.getAddress           ());
		 admission.put("patient_contact_no",                icu.getPatientcontactno  ());
		 admission.put("caste",                             icu.getCaste             ());
		 admission.put("religion",                          icu.getReligion          ());
		 admission.put("admission_date",                    icu.getAdmissiondate     ());
		 admission.put("admission_time",                    icu.getAdmissiontime     ());
		 admission.put("delivery_date",                     icu.getDeliverydate      ());
		 admission.put("obstetric_etails_at_admission",     icu.getObstetricdetails  ());
		 admission.put("delivery_time",                     icu.getDeliverytime      ());
		 admission.put("gravida",                           icu.getGravida           ());
		 admission.put("parity",                            icu.getParity            ());
		 admission.put("abortion",                          icu.getAbortion          ());
		 admission.put("living_children",                   icu.getLivingchildren    ());
		 
		 
		 	 admission.put("bedNo",                	icu.getBedNo());
			 admission.put("provisionalDiagnosis",  icu.getProvisionalDiagnosis());
			 admission.put("referredIn",      		icu.getReferredIn());
			 admission.put("nameOfFacility1",    	icu.getNameOfFacility1());
			 admission.put("reasonforReferral",     icu.getReasonforReferral());
			 admission.put("placeOfDelivery",       icu.getPlaceOfDelivery());
			 admission.put("birthDate", 			icu.getBirthDate());
			 admission.put("birthTime",             icu.getBirthTime());
		 
		 
		 
		 
		 
		 admission.put("lmp",                               icu.getLmp               ());
		 admission.put("edd",                               icu.getEdd               ());
		 admission.put("number_of_anc",                     icu.getNumberofanc       ());
		 admission.put("gestational_age",                   icu.getGestationalage    ());
		 admission.put("labour",                            icu.getLabour            ());
		 admission.put("presentation",                      icu.getPresentation      ());
		 admission.put("uterine_tenderness",                icu.getUterinetenderness ());
		 admission.put("fhr",                               icu.getFhr               ());
		 admission.put("if_fhr_yes",                        icu.getIffhryes          ());
		 admission.put("amniotic_fluid",                    icu.getAmnioticfluid     ());
		 admission.put("course_of_labour",                  icu.getCourseoflabour    ());
		 admission.put("type_of_delivery",                  icu.getTypeofdelivery    ());
		 admission.put("aph",                               icu.getAph               ());
		 admission.put("pph",                               icu.getPph               ());
		 admission.put("p_v_foul_smelling",                 icu.getPvfoulsmelling    ());
		 admission.put("indi_for_caesarean",                icu.getIndiforcaesarean  ());
		 admission.put("deliv_attended_by",                 icu.getDelivattendedby   ());
		 admission.put("antenatal_steroids",                icu.getAntenatalsteroids ());
		 admission.put("if_yes",                            icu.getIfyes             ());
		 admission.put("no_of_doses",                       icu.getNoofdoses         ());
		 admission.put("magnesium_sulphate",                icu.getMagnesiumsulphate ());
		 admission.put("if_yes_dose",                       icu.getIfyesdose         ());
		 admission.put("time_of_last_doses",                icu.getTimeoflastdoses   ());
		 
		   org.json.JSONObject indication = new     org.json.JSONObject();
		 
		       indication.put("icu_admission",                 icu.getIcu_admission                ());
		       indication.put("hdu_admission",                 icu.getHdu_admission                ());
		       indication.put("hemorrhage_hdu",                icu.getHemorrhage_hdu               ());
		       indication.put("hemorrhage_icu",                icu.getHemorrhage_icu               ());
		       indication.put("hemorrhage_others",             icu.getHemorrhage_others            ());
		       indication.put("hypertensive_disorders_hdu",    icu.getHypertensive_disorders_hdu   ());
		       indication.put("hypertensive_disorders_icu",    icu.getHypertensive_disorders_icu   ());
		       indication.put("sepsis_hdu",                    icu.getSepsis_hdu                   ());
		       indication.put("sepsis_icu",                    icu.getSepsis_icu                   ());
		       indication.put("sepsis_others",                 icu.getSepsis_others                ());
		       indication.put("renal_dysfunction_hdu",         icu.getRenal_dysfunction_hdu        ());
		       indication.put("renal_dysfunction_icu",         icu.getRenal_dysfunction_icu        ());
		       indication.put("jaundice_in_pregnancy_hdu",     icu.getJaundice_in_pregnancy_hdu    ());
		       indication.put("jaundice_in_pregnancy_icu",     icu.getJaundice_in_pregnancy_icu    ());
		       indication.put("jaundice_in_pregnancy_others",  icu.getJaundice_in_pregnancy_others ());
		       indication.put("coagulation_system_hdu",        icu.getCoagulation_system_hdu       ());
		       indication.put("coagulation_system_icu",        icu.getCoagulation_system_icu       ());
		       indication.put("abnormal_vitals_hdu",           icu.getAbnormal_vitals_hdu          ());
		       indication.put("abnormal_vitals_icu",           icu.getAbnormal_vitals_icu          ());
		       indication.put("abg_abnormalities_hdu",         icu.getAbg_abnormalities_hdu        ());
		       indication.put("abg_abnormalities_icu",         icu.getAbg_abnormalities_icu        ());
		       indication.put("electrolyte_disturbances_hdu",  icu.getElectrolyte_disturbances_hdu ());
		       indication.put("electrolyte_disturbances_icu",  icu.getElectrolyte_disturbances_icu ());
		       indication.put("medical_disorders_hdu",         icu.getMedical_disorders_hdu        ());
		       indication.put("medical_disorders_icu",         icu.getMedical_disorders_icu        ());
		       indication.put("medical_disorders_others10",    icu.getMedical_disorders_others10   ());
		       indication.put("cause_of_admission",            icu.getCause_of_admission           ());
		 
		            org.json.JSONObject history = new org.json.JSONObject();    
		                 history.put("past_medical_history",    icu.getPastmedicalhistory  ()); 
		                 history.put("past_surgical_history",   icu.getPastsurgicalhistory ()); 
		                 history.put("family_history",          icu.getFamilyhistory       ()); 
		 
		                 
		                 org.json.JSONObject baby = new org.json.JSONObject(); 
		                 
		                   baby.put("outcome_of_birth" ,   icu.getOutcomeofbirth  ());           
		                   baby.put("birth_weight",        icu.getBirthweight     ());           
		                   baby.put("multiple_births",     icu.getMultiplebirths  ());           
		                   baby.put("if_yes_number",       icu.getIfyesnumber     ());           
		                   baby.put("sexofbaby",           icu.getSexofbaby       ());           
		                   baby.put("resu_required",       icu.getResurequired    ());           
		                   baby.put("weeks_of_gestation",  icu.getWeeksofgestation());           
		                   baby.put("sncu_admission",      icu.getSncuadmission   ());           
		                   baby.put("vitamin_K_given",     icu.getVitaminKgiven   ());           
		                   baby.put("b_fed_within_1hour",  icu.getBfedwithin1hour ());           
		                 
		                    org.json.JSONObject general = new org.json.JSONObject(); 
		                 
		                  general.put("general_condition",   icu.getGeneralcondition());       
		                  general.put("height",              icu.getHeight          ());       
		                  general.put("weight",              icu.getWeight          ());       
		                  general.put("bmi",                 icu.getBmi             ());       
		                  general.put("heart_rate",          icu.getHeartrate       ());       
		                  general.put("blood_pressure",      icu.getBloodpressure   ());       
		                  general.put("spo2",                icu.getSpo2            ());       
		                  general.put("respiratory_rate",    icu.getRespiratoryrate ());       
		                  general.put("temperature",         icu.getTemperature     ());       
		                  general.put("jvp",                 icu.getJvp             ());       
		                  general.put("pallor",              icu.getPallor          ());       
		                  general.put("icterus",             icu.getIcterus         ());       
		                  general.put("cyanosis",            icu.getCyanosis        ());       
		                  general.put("oedema",              icu.getOedema          ());       
		                  general.put("g_ex_others",         icu.getGexothers       ());       
		                                                             
		                    
		                     org.json.JSONObject systemic = new org.json.JSONObject(); 
		                    
		                        systemic.put("respiratory_system",           icu.getRespiratorysystem      ());
		                        systemic.put("cardio_vascular_system",       icu.getCardiovascularsystem   ());
		                        systemic.put("central_nervous_system",       icu.getCentralnervoussystem   ());
		                        systemic.put("per_abdomen_examination",      icu.getPerabdomenexamination  ());
		                        systemic.put("per_speculum_vaginal",         icu.getPerspeculumvaginal	   ());
		                        systemic.put("any_others",                   icu.getAnyothers		       ());
		                        systemic.put("respiratory_system_text",      icu.getRespiratorysystemtext  ());
		                        systemic.put("cardio_vascular_system_text",  icu.getCardiosystemtext       ());
		                        systemic.put("central_nervous_system_text",  icu.getCentralsystemtext      ());
		                        systemic.put("per_abdomen_examination_text", icu.getPerabdomentext         ());
		                        systemic.put("per_speculum_vaginal_text",    icu.getPerspeculumtext        ());
		                     
		                      org.json.JSONObject orders = new  org.json.JSONObject();
		                     
		                       orders.put("nutrition",             icu.getNutrition           ());
		                       orders.put("investigations",        icu.getInvestigations      ());
		                       orders.put("medications",           icu.getMedications         ());
		                       orders.put("procedures",            icu.getProcedures          ());
		                       orders.put("monitoring_schedule",   icu.getMonitoringschedule  ());
		                       orders.put("multidiscipl_review",   icu.getMultidisciplreview  ());
		                       orders.put("plan_for_next_24_hours",icu.getPlanfor24hours      ());
		                       orders.put("doctor_name_and_sign",  icu.getDoctornamesign      ());
		                      
		                       org.json.JSONObject sequentialorgan  = new   org.json.JSONObject();
		                       
		                       
		                        sequentialorgan.put("respiration",    icu.getRespiration      ());
		                        sequentialorgan.put("coagulation",    icu.getCoagulation      ());
		                        sequentialorgan.put("liver",          icu.getLiver            ());
		                        sequentialorgan.put("cardiovascular", icu.getCardiovascular   ());
		                        sequentialorgan.put("cns",            icu.getCns              ());
		                        sequentialorgan.put("renal",          icu.getRenal            ());
		                       
		                         org.json.JSONObject laboratory = new org.json.JSONObject();
		                                                                              
		                  laboratory.put("day1_hb",                          icu.getLabor().getDay1hb                   ( ));   
		                  laboratory.put("day1_total_wbc_count",             icu.getLabor().getDay1totalwbccount        ( ));   
		                  laboratory.put("day1_differential_wbc_count",      icu.getLabor().getDay1wbccount             ( ));   
		                  laboratory.put("day1_platelet_count",              icu.getLabor().getDay1plateletcount        ( ));  
		                  laboratory.put("day1_peripheral_smear",            icu.getLabor().getDay1peripheralsmear      ( ));  
		                  laboratory.put("day1_bt_ct_crt_",                  icu.getLabor().getDay1btctcrt              ( ));  
		                  laboratory.put("day1_pt_aptt_inr",                 icu.getLabor().getDay1ptapttinr            ( ));  
		                  laboratory.put("day1_blood_group_rh_type",         icu.getLabor().getDay1bloodgrouprhtype     ( ));  
		                  laboratory.put("day1_sickling_test",               icu.getLabor().getDay1sicklingtest         ( ));  
		                  laboratory.put("day1_random_blood_glucose",        icu.getLabor().getDay1randombloodglucose   ( ));  
		                  laboratory.put("day1_blood_urea_nitrogen",         icu.getLabor().getDay1bloodureanitrogen    ( ));  
		                  laboratory.put("day1_serum_creatinine",            icu.getLabor().getDay1serumcreatinine      ( ));  
		                  laboratory.put("day1_serum_calcium",               icu.getLabor().getDay1serumcalcium         ( ));  
		                  laboratory.put("day1_serum_sodium",                icu.getLabor().getDay1serumsodium          ( ));  
		                  laboratory.put("day1_serum_potassium",             icu.getLabor().getDay1serumpotassium       ( ));  
		                  laboratory.put("day1_total_protein_serum_albumin", icu.getLabor().getDay1serumalbumin         ( ));   
		                  laboratory.put("day1_serum_biliorubin",            icu.getLabor().getDay1serumbiliorubin      ( ));  
		                  laboratory.put("day1_sgpt_sgpt_ldh",               icu.getLabor().getDay1sgptsgptldh          ( ));  
		                  laboratory.put("day1_serum_fibrinogen",            icu.getLabor().getDay1serumfibrinogen      ( ));  
		                  laboratory.put("day1_d_dimer",                     icu.getLabor().getDay1ddimer               ( ));  
		                  laboratory.put("day1_urine_rm",                    icu.getLabor().getDay1urinerm              ( ));  
		                  laboratory.put("day1_stool_for_occult_blood",      icu.getLabor().getDay1occultblood          ( ));  
		                  laboratory.put("day1_blood_gas_time",              icu.getLabor().getDay1bloodgastime         ( ));  
		                  laboratory.put("day1_blood_gas_fio2",              icu.getLabor().getDay1bloodgasfio2         ( ));  
		                  laboratory.put("day1_blood_gas_ph",                icu.getLabor().getDay1bloodgasph           ( ));  
		                  laboratory.put("day1_blood_gas_pco2",              icu.getLabor().getDay1bloodgaspco2         ( ));  
		                  laboratory.put("day1_blood_gas_po2",               icu.getLabor().getDay1bloodgaspo2          ( ));  
		                  laboratory.put("day1_blood_gas_hco3",              icu.getLabor().getDay1bloodgashco3         ( ));  
		                  laboratory.put("day1_blood_gas_satn",              icu.getLabor().getDay1bloodgassatn         ( ));  
		                  laboratory.put("day2_hb",                          icu.getLabor().getDay2hb                   ( ));  
		                  laboratory.put("day2_total_wbc_count",             icu.getLabor().getDay2totalwbccount        ( ));  
		                  laboratory.put("day2_differential_wbc_count",      icu.getLabor().getDay2wbccount             ( ));  
		                  laboratory.put("day2_platelet_count",              icu.getLabor().getDay2plateletcount        ( ));  
		                  laboratory.put("day2_peripheral_smear",            icu.getLabor().getDay2peripheralsmear      ( ));  
		                  laboratory.put("day2_bt_ct_crt_",                  icu.getLabor().getDay2btctcrt              ( ));  
		                  laboratory.put("day2_pt_aptt_inr",                 icu.getLabor().getDay2ptapttinr            ( ));  
		                  laboratory.put("day2_blood_group_rh_type",         icu.getLabor().getDay2bloodgrouprhtype     ( ));  
		                  laboratory.put("day2_sickling_test",               icu.getLabor().getDay2sicklingtest         ( ));  
		                  laboratory.put("day2_random_blood_glucose",        icu.getLabor().getDay2randombloodglucose   ( ));  
		                  laboratory.put("day2_blood_urea_nitrogen",         icu.getLabor().getDay2bloodureanitrogen    ( ));  
		                  laboratory.put("day2_serum_creatinine",            icu.getLabor().getDay2serumcreatinine      ( ));  
		                  laboratory.put("day2_serum_calcium",               icu.getLabor().getDay2serumcalcium         ( ));  
		                  laboratory.put("day2_serum_sodium",                icu.getLabor().getDay2serumsodium          ( ));  
		                  laboratory.put("day2_serum_potassium",             icu.getLabor().getDay2serumpotassium       ( ));  
		                  laboratory.put("day2_total_protein_serum_albumin", icu.getLabor().getDay2serumalbumin         ( ));   
		                  laboratory.put("day2_serum_biliorubin",            icu.getLabor().getDay2serumbiliorubin      ( ));  
		                  laboratory.put("day2_sgpt_sgpt_ldh",               icu.getLabor().getDay2sgptsgptldh          ( ));  
		                  laboratory.put("day2_serum_fibrinogen",            icu.getLabor().getDay2serumfibrinogen      ( ));  
		                  laboratory.put("day2_d_dimer",                     icu.getLabor().getDay2ddimer                ());  
		                  laboratory.put("day2_urine_rm",                    icu.getLabor().getDay2urinerm               ());  
		                  laboratory.put("day2_stool_for_occult_blood",      icu.getLabor().getDay2occultblood           ());  
		                  laboratory.put("day2_blood_gas_time",              icu.getLabor().getDay2bloodgastime          ());  
		                  laboratory.put("day2_blood_gas_fio2",              icu.getLabor().getDay2bloodgasfio2         ( ));  
		                  laboratory.put("day2_blood_gas_ph",                icu.getLabor().getDay2bloodgasph            ());  
		                  laboratory.put("day2_blood_gas_pco2",              icu.getLabor().getDay2bloodgaspco2          ());  
		                  laboratory.put("day2_blood_gas_po2",               icu.getLabor().getDay2bloodgaspo2           ());  
		                  laboratory.put("day2_blood_gas_hco3",              icu.getLabor().getDay2bloodgashco3          ());  
		                  laboratory.put("day2_blood_gas_satn",              icu.getLabor().getDay2bloodgassatn          ());  
		                  laboratory.put("day3_hb",                          icu.getLabor().getDay3hb                    ());  
		                  laboratory.put("day3_total_wbc_count",             icu.getLabor().getDay3totalwbccount         ());  
		                  laboratory.put("day3_differential_wbc_count",      icu.getLabor().getDay3wbccount              ());  
		                  laboratory.put("day3_platelet_count",              icu.getLabor().getDay3plateletcount         ());  
		                  laboratory.put("day3_peripheral_smear",            icu.getLabor().getDay3peripheralsmear       ());  
		                  laboratory.put("day3_bt_ct_crt_",                  icu.getLabor().getDay3btctcrt               ());  
		                  laboratory.put("day3_pt_aptt_inr",                 icu.getLabor().getDay3ptapttinr             ());  
		                  laboratory.put("day3_blood_group_rh_type",         icu.getLabor().getDay3rhtype                ());  
		                  laboratory.put("day3_sickling_test",               icu.getLabor().getDay3sicklingtest          ());  
		                  laboratory.put("day3_random_blood_glucose",        icu.getLabor().getDay3randombloodglucose   ( ));  
		                  laboratory.put("day3_blood_urea_nitrogen",         icu.getLabor().getDay3bloodureanitrogen     ());  
		                  laboratory.put("day3_serum_creatinine",            icu.getLabor().getDay3serumcreatinine       ());  
		                  laboratory.put("day3_serum_calcium",               icu.getLabor().getDay3serumcalcium          ());  
		                  laboratory.put("day3_serum_sodium",                icu.getLabor().getDay3serumsodium           ());  
		                  laboratory.put("day3_serum_potassium",             icu.getLabor().getDay3serumpotassium        ());  
		                  laboratory.put("day3_total_protein_serum_albumin", icu.getLabor().getDay3serumalbumin          ());   
		                  laboratory.put("day3_serum_biliorubin",            icu.getLabor().getDay3serumbiliorubin       ());  
		                  laboratory.put("day3_sgpt_sgpt_ldh",               icu.getLabor().getDay3sgptsgptldh           ());  
		                  laboratory.put("day3_serum_fibrinogen",            icu.getLabor().getDay3serumfibrinogen       ());  
		                  laboratory.put("day3_d_dimer",                     icu.getLabor().getDay3ddimer                ());  
		                  laboratory.put("day3_urine_rm",                    icu.getLabor().getDay3urinerm               ());  
		                  laboratory.put("day3_stool_for_occult_blood",      icu.getLabor().getDay3occultblood           ());  
		                  laboratory.put("day3_blood_gas_time",              icu.getLabor().getDay3bloodgastime          ());  
		                  laboratory.put("day3_blood_gas_fio2",              icu.getLabor().getDay3bloodgasfio2          ());  
		                  laboratory.put("day3_blood_gas_ph",                icu.getLabor().getDay3bloodgasph            ());  
		                  laboratory.put("day3_blood_gas_pco2",              icu.getLabor().getDay3bloodgaspco2          ());  
		                  laboratory.put("day3_blood_gas_po2",               icu.getLabor().getDay3bloodgaspo2           ());  
		                  laboratory.put("day3_blood_gas_hco3",              icu.getLabor().getDay3bloodgashco3          ());  
		                  laboratory.put("day3_blood_gas_satn",              icu.getLabor().getDay3bloodgassatn          ());  
		                  laboratory.put("patient_name",                     icu.getLabor().getPatientname               ());  
		                  laboratory.put("reg_no",                           icu.getLabor().getRegno                     ());  
		                  laboratory.put("doa",                              icu.getLabor().getDoa                       ());  
		                  laboratory.put("doctor_in_charge",                 icu.getLabor().getDoctorincharge            ());  
		                  laboratory.put("urine_culture",                    icu.getLabor().getUrineculture              ());  
		                  laboratory.put("blood_culture",                    icu.getLabor().getBloodculture              ());  
		                  laboratory.put("cervical_vaginal_swab",            icu.getLabor().getCervicalvaginalswab       ());  
		                  laboratory.put("any_other",                        icu.getLabor().getAnyother                  ());  
		                  laboratory.put("hiv",                              icu.getLabor().getHiv                       ());  
		                  laboratory.put("hbsag",                            icu.getLabor().getHbsag                     ());  
		                  laboratory.put("vdrl",                             icu.getLabor().getVdrl                      ());  
		                     
		                     
		                     
		
		map.put("admission", admission);
		map.put("indication", indication);
		map.put("history", history);
		map.put("baby_information_at_birth", baby);
		map.put("general_examination", general); 
		map.put("systemic_examination", systemic);
		map.put("orders", orders);
		map.put("sequentialorgan", sequentialorgan);
		map.put("laboratory_investigation_sheet", laboratory);
		return map.toString();
		
		
		
	}
	
	
	
	
	
	
	
	
}
