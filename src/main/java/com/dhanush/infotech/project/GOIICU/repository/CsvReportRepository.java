package com.dhanush.infotech.project.GOIICU.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIICU.model.CsvReport;


@Repository
public interface CsvReportRepository extends  JpaRepository<CsvReport, Long> {
	
	
	

 	@Query(value = "select a from CsvReport a where a.state= :state and a.admissiondate like :date% ")
	List<CsvReport> getStateDetails(@Param("state")String state,@Param("date") String date);

 	@Query(value = "select a from CsvReport a where a.state= :state and  a.district= :district and a.admissiondate like :date% ")
	List<CsvReport> getDistrictDetails(@Param("state")String state,@Param("district")String district,@Param("date") String date);

 	@Query(value = "select a from CsvReport a where a.state= :state and  a.district= :district and a.block= :block and  a.admissiondate like :date% ")
	List<CsvReport> getBlockDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("date") String date);

 	
 	@Query(value = "select a from CsvReport a where a.state= :state and  a.district= :district and a.block= :block and a.facility_type= :facilityType and a.admissiondate like :date% ")
	List<CsvReport> getFacilityTypeDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("date") String date);

		
 	@Query(value = "select a from CsvReport a where a.state= :state and  a.district= :district and a.block= :block and a.facility_type= :facilityType and a.facility= :facility and a.admissiondate like :date% ")
	List<CsvReport> getFacilityDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("facility")String facility,@Param("date") String date);

}
