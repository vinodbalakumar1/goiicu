package com.dhanush.infotech.project.GOIICU.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIICU.model.LocationMasters;

public interface LocationMastersRepository extends JpaRepository<LocationMasters, Long> {

	@Query(value = "SELECT a FROM LocationMasters a where a.stateCode = :state and a.district =:district and a.blockCode = :block  and a.facilityCode = :facility_code and a.facilityTypeId = :facilityTypeId")
	 List<LocationMasters> findByFacility(@Param("state")int state,@Param("district") int district,@Param("block") int block,@Param("facility_code") int facility_code,@Param("facilityTypeId") String facilityTypeId);
}
