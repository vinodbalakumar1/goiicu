package com.dhanush.infotech.project.GOIICU.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.model.GoiIcu;
import com.dhanush.infotech.project.GOIICU.repository.IcuRepository;


@RestController
@RequestMapping("/api")
public class GetBeneficarySearchController {
	
	@Autowired
	IcuRepository icuRepository;
	
	
	@CrossOrigin
	@RequestMapping(value = "/getbyuniqueid", method = RequestMethod.POST, produces = { "application/json" })
	public String getUserById(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String uniqueNo = openJson.getString("uniqueId");
		String mctsno = openJson.getString("mctsno");
		
		org.json.JSONObject jo = new org.json.JSONObject();
		org.json.JSONArray jo1 = new org.json.JSONArray();
		if(uniqueNo.length() > 0) {
		List<GoiIcu> admission = icuRepository.findByUniqueid(uniqueNo);
		for(GoiIcu admission11 : admission) {
	    	jo.put("nameOfFacility", admission11.getFacilityName());
	    	jo.put("caseNo", admission11.getUniqueid());
	    	jo.put("nameOfWomen", admission11.getName());
	    	jo.put("mobileNo", admission11.getContactnumber());
	    	jo.put("district", admission11.getDistrictname());
	    	jo.put("admission_time", admission11.getAdmissiontime());
	    	jo.put("admission_date", admission11.getAdmissiondate());
	    	jo.put("id", ""+admission11.getId());
	    	jo.put("mctsno", admission11.getMctsrchnumber());
	    	jo.put("status", admission11.getStatus());
	    	jo1.put(jo);
		}
		}
		else {
			List<GoiIcu> admission = icuRepository.findByMctsrchnumber(mctsno);
			for(GoiIcu admission11 : admission) {
		    	jo.put("nameOfFacility", admission11.getFacilityName());
		    	jo.put("caseNo", admission11.getUniqueid());
		    	jo.put("nameOfWomen", admission11.getName());
		    	jo.put("mobileNo", admission11.getContactnumber());
		    	jo.put("district", admission11.getDistrictname());
		    	jo.put("admission_time", admission11.getAdmissiontime());
		    	jo.put("admission_date", admission11.getAdmissiondate());
		    	jo.put("id",""+ admission11.getId());
		    	jo.put("mctsno", admission11.getMctsrchnumber());
		    	jo.put("status", admission11.getStatus());
		    	jo1.put(jo);
			}
			}
			if(jo1.length()==0)
				jo1.put(jo);
			
		return jo1.toString();
	}

	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getdetailsbyname", method = RequestMethod.POST, produces = { "application/json" })
	public String getBeneficaryBy(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String beneficary = openJson.getString("beneficary");
		String mobileno = openJson.getString("mobileno");
		String state = openJson.getString("state");
		String district = openJson.getString("district");
		String block = openJson.getString("block");
		String facilityType = openJson.getString("facilityType");
		String facility = openJson.getString("facility");
		
		org.json.JSONArray jo1 = new org.json.JSONArray();
		List<GoiIcu> admission = null;
		if(mobileno.isEmpty())
		{
		if(!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = icuRepository.findByStateAndNameLike(state,beneficary);
			
		}
		if(!state.equalsIgnoreCase("ALL")&& !district.equalsIgnoreCase("ALL")&& block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = icuRepository.findByStateAndDistrictAndNameLike(state,district,beneficary);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")&& facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = icuRepository.findByStateAndDistrictAndBlockAndNameLike(state,district,block,beneficary);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL")&& !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
		{
			admission = icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndNameLike(state,district,block,facilityType,beneficary);

		}
		if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))
		{
			admission = icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndNameLike(state,district,block,facilityType,facility,beneficary);

		}
		
		for(GoiIcu admission11 : admission) {
		org.json.JSONObject jo = new org.json.JSONObject();
	    	jo.put("nameOfFacility", admission11.getFacilityName());
	    	jo.put("caseNo", admission11.getUniqueid());
	    	jo.put("nameOfWomen", admission11.getName());
	    	jo.put("mobileNo", admission11.getContactnumber());
	    	jo.put("district", admission11.getDistrictname());
	    	jo.put("admission_time", admission11.getAdmissiontime());
	    	jo.put("admission_date", admission11.getAdmissiondate());
	    	jo.put("id", admission11.getId());
	    	jo.put("mctsno", admission11.getMctsrchnumber());
	    	jo.put("status", admission11.getStatus());
	    	jo1.put(jo);
		}
		}
		else if(mobileno.length() > 0)
		{
			if(mobileno.isEmpty())
			{
			if(!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
			{
				admission = icuRepository.findByStateAndNameLike(state,beneficary);
				
			}
			if(!state.equalsIgnoreCase("ALL")&& !district.equalsIgnoreCase("ALL")&& block.equalsIgnoreCase("ALL") && facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
			{
				admission = icuRepository.findByStateAndDistrictAndNameLike(state,district,beneficary);

			}
			if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")&& facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
			{
				admission = icuRepository.findByStateAndDistrictAndBlockAndNameLike(state,district,block,beneficary);

			}
			if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL")&& !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))
			{
				admission = icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndNameLike(state,district,block,facilityType,beneficary);

			}
			if(!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")  && !facilityType.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))
			{
				admission = icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndNameLike(state,district,block,facilityType,facility,beneficary);

			}
			
			for(GoiIcu admission11 : admission) {
				org.json.JSONObject jo = new org.json.JSONObject();
			    	jo.put("nameOfFacility", admission11.getFacilityName());
			    	jo.put("caseNo", admission11.getUniqueid());
			    	jo.put("nameOfWomen", admission11.getName());
			    	jo.put("mobileNo", admission11.getContactnumber());
			    	jo.put("district", admission11.getDistrictname());
			    	jo.put("admission_time", admission11.getAdmissiontime());
			    	jo.put("admission_date", admission11.getAdmissiondate());
			    	jo.put("id", admission11.getId());
			    	jo.put("mctsno", admission11.getMctsrchnumber());
			    	jo.put("status", admission11.getStatus());
			    	jo1.put(jo);
				}
		}
		
		
	}
	
		return jo1.toString();
	
	
	}	
}
