package com.dhanush.infotech.project.GOIICU.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "csv_report")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(allowGetters = true)
public class CsvReport {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="state")
	private String state;
	
	@Column(name="district")
	private String district;
	
	@Column(name="block")
	private String block;
	
	@Column(name="facility_type")
	private String facility_type;
	
	@Column(name="facility")
	private String facility;
	
	
	@Column(name="registration_number")
	private String registration_number;
	
	
	@Column(name="mctsrchnumber")
	private String mctsrchnumber;
	
	@Column(name="aadharnumber")
	private String aadharnumber;
	
	@Column(name="admissiondate")
	private String admissiondate;
	
	@Column(name="admissiontime")
	private String admissiontime;
	
	@Column(name="patient_name")
	private String patient_name;
	
	@Column(name="age")
	private String age;
	
	@Column(name="wo")
	private String wo;
	
	
	@Column(name="religion")
	private String religion;
	
	@Column(name="caste")
	private String caste;
	
	@Column(name="BPLholder")
	private String BPLholder;	
	
	@Column(name="address")
	private String address;
	
	
	@Column(name="contactnumber")
	private String contactnumber;
	
	
	@Column(name="live_still")
	private String live_still;
	
	
	@Column(name="sex")
	private String sex;
	
	
	@Column(name="birthweight")
	private String birthweight;
	
	
	@Column(name="deliverydate")
	private String deliverydate;
	
	@Column(name="deliverytime")
	private String deliverytime;
	
	
	@Column(name="NameandsignatureofSN")
	private String NameandsignatureofSN;




	public String getRegistrationNumber() {
		return registration_number;
	}


	public void setRegistrationNumber(String registrationNumber) {
		registration_number = registrationNumber;
	}


	public String getRegistration_number() {
		return registration_number;
	}


	public void setRegistration_number(String registration_number) {
		this.registration_number = registration_number;
	}


	public String getMctsrchnumber() {
		return mctsrchnumber;
	}


	public void setMctsrchnumber(String mctsrchnumber) {
		this.mctsrchnumber = mctsrchnumber;
	}


	public String getAadharnumber() {
		return aadharnumber;
	}


	public void setAadharnumber(String aadharnumber) {
		this.aadharnumber = aadharnumber;
	}


	public String getAdmissiondate() {
		return admissiondate;
	}


	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}


	public String getAdmissiontime() {
		return admissiontime;
	}


	public String getPatient_name() {
		return patient_name;
	}


	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}


	public void setAdmissiontime(String admissiontime) {
		this.admissiontime = admissiontime;
	}


	public String getPatientName() {
		return patient_name;
	}


	public void setPatientName(String patientName) {
		patient_name = patientName;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getWo() {
		return wo;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getDistrict() {
		return district;
	}


	public void setDistrict(String district) {
		this.district = district;
	}


	public String getBlock() {
		return block;
	}


	public void setBlock(String block) {
		this.block = block;
	}


	public String getFacility_type() {
		return facility_type;
	}


	public void setFacility_type(String facility_type) {
		this.facility_type = facility_type;
	}


	public String getFacility() {
		return facility;
	}


	public void setFacility(String facility) {
		this.facility = facility;
	}


	public void setWo(String wo) {
		this.wo = wo;
	}


	public String getReligion() {
		return religion;
	}


	public void setReligion(String religion) {
		this.religion = religion;
	}


	public String getCaste() {
		return caste;
	}


	public void setCaste(String caste) {
		this.caste = caste;
	}


	public String getBPLholder() {
		return BPLholder;
	}


	public void setBPLholder(String bPLholder) {
		BPLholder = bPLholder;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getContactnumber() {
		return contactnumber;
	}


	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}


	public String getLive_still() {
		return live_still;
	}


	public void setLive_still(String live_still) {
		this.live_still = live_still;
	}


	public String getSex() {
		return sex;
	}


	public void setSex(String sex) {
		this.sex = sex;
	}


	public String getBirthweight() {
		return birthweight;
	}


	public void setBirthweight(String birthweight) {
		this.birthweight = birthweight;
	}


	public String getDeliverydate() {
		return deliverydate;
	}


	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}


	public String getDeliverytime() {
		return deliverytime;
	}


	public void setDeliverytime(String deliverytime) {
		this.deliverytime = deliverytime;
	}


	public String getNameandsignatureofSN() {
		return NameandsignatureofSN;
	}


	public void setNameandsignatureofSN(String nameandsignatureofSN) {
		NameandsignatureofSN = nameandsignatureofSN;
	}
	

}
