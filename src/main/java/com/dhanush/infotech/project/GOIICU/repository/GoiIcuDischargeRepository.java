package com.dhanush.infotech.project.GOIICU.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dhanush.infotech.project.GOIICU.model.GoiIcuDischarge;

public interface GoiIcuDischargeRepository extends JpaRepository<GoiIcuDischarge, Long>{

}
