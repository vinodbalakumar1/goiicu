package com.dhanush.infotech.project.GOIICU.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




/**
 * Created by vinod on 06/07/17.
 */
@Entity
@Table(name = "locationMasters")
public class LocationMasters  implements Serializable{
    /**
	 *  
	 */
	
	public LocationMasters()
	{
		
	}
	
	
	private static final long serialVersionUID = 2556974999573810247L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
    	
        this.id = id;
    }

  private Integer blockCode;
  private String block;
  private String districtCode;
  private int district ;
  private Integer facilityCode;
  private String facility;
  private String facilityTypeId;
  private String facilityType;
  private int stateCode;
  private String state;
  private Integer subFacilityCode;
  private String subFacility;
  private String villageCode;
  private String village;
	
  
  public Integer getBlockCode() {
	return blockCode;
}

public void setBlockCode(int blockCode) {
	this.blockCode = blockCode;
}

public String getBlock() {
	return block;
}

public void setBlock(String block) {
	this.block = block;
}


public int getDistrict() {
	return district;
}

public void setDistrict(int district) {
	this.district = district;
}

public String getDistrictCode() {
	return districtCode;
}

public void setDistrictCode(String districtCode) {
	this.districtCode = districtCode;
}

public void setBlockCode(Integer blockCode) {
	this.blockCode = blockCode;
}

public Integer getFacilityCode() {
	return facilityCode;
}

public void setFacilityCode(Integer facilityCode) {
	this.facilityCode = facilityCode;
}

public String getFacility() {
	return facility;
}

public void setFacility(String facility) {
	this.facility = facility;
}

public String getFacilityTypeId() {
	return facilityTypeId;
}

public void setFacilityTypeId(String facilityTypeId) {
	this.facilityTypeId = facilityTypeId;
}

public String getFacilityType() {
	return facilityType;
}

public void setFacilityType(String facilityType) {
	this.facilityType = facilityType;
}

public int getStateCode() {
	return stateCode;
}

public void setStateCode(int stateCode) {
	this.stateCode = stateCode;
}

public String getState() {
	return state;
}

public void setState(String state) {
	this.state = state;
}

public Integer getSubFacilityCode() {
	return subFacilityCode;
}

public void setSubFacilityCode(Integer subFacilityCode) {
	this.subFacilityCode = subFacilityCode;
}

public String getSubFacility() {
	return subFacility;
}

public void setSubFacility(String subFacility) {
	this.subFacility = subFacility;
}

public String getVillageCode() {
	return villageCode;
}

public void setVillageCode(String villageCode) {
	this.villageCode = villageCode;
}

public String getVillage() {
	return village;
}

public void setVillage(String village) {
	this.village = village;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}


   
}
