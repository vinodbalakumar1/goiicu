package com.dhanush.infotech.project.GOIICU.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIICU.model.GoiIcu;

@Repository
public interface IcuMPReportRepository extends JpaRepository<GoiIcu, Long>
{
	
	//for aph

	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%1%'")
	Long findByStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%1%'")
	Long findByDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%1%'")
	Long findByBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%1%'")
	Long findByFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and  facility= :facility and admissiondate like :date% and causeofadmission like '%1%'")
	Long findByFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


	// for pph
	
	

	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%2%'")
	Long findByPphStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%2%'")
	Long findByPphDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%2%'")
	Long findByPphBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%2%'")
	Long findByPphFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%2%'")
	Long findByPphFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);



//for sepsis
	
	


	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%3%'")
	Long findBySepsisStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%3%'")
	Long findBySepsisDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%3%'")
	Long findBySepsisBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%3%'")
	Long findBySepsisFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%3%'")
	Long findBySepsisFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


//for Eclampsia
	
	
	



	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%4%'")
	Long findByEclampsiaStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%4%'")
	Long findByEclampsiaDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%4%'")
	Long findByEclampsiaBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%4%'")
	Long findByEclampsiaFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%4%'")
	Long findByEclampsiaFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);




	// for Abortion
	
	



	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%5%'")
	Long findByAbortionStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%5%'")
	Long findByAbortionDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%5%'")
	Long findByAbortionBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%5%'")
	Long findByAbortionFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%5%'")
	Long findByAbortionFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);



	
	
	// for ObstructedLabour
	
	



	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%6%'")
	Long findByObstructedLabourStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%6%'")
	Long findByObstructedLabourDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%6%'")
	Long findByObstructedLabourBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%6%'")
	Long findByObstructedLabourFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%6%'")
	Long findByObstructedLabourFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


	


	// for RupturedUterus
	



	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%7%'")
	Long findByRupturedUterusStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%7%'")
	Long findByRupturedUterusDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%7%'")
	Long findByRupturedUterusBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%7%'")
	Long findByRupturedUterusFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%7%'")
	Long findByRupturedUterusFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


// for SevereAnaemia
	
	


	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%8%'")
	Long findBySevereAnaemiaStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%8%'")
	Long findBySevereAnaemiaDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%8%'")
	Long findBySevereAnaemiaBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%8%'")
	Long findBySevereAnaemiaFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%8%'")
	Long findBySevereAnaemiaFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


	
	//for CardiacDiseases
	
	


	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%9%'")
	Long findByCardiacDiseasesStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%9%'")
	Long findByCardiacDiseasesDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%9%'")
	Long findByCardiacDiseasesBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%9%'")
	Long findByCardiacDiseasesFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%9%'")
	Long findByCardiacDiseasesFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);



	// for Jaundice




	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%10%'")
	Long findByJaundiceStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%10%'")
	Long findByJaundiceDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%10%'")
	Long findByJaundiceBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%10%'")
	Long findByJaundiceFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%10%'")
	Long findByJaundiceFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);


	
	// for Others
	


	@Query(value=" select count(1) from GoiIcu where state = :state and admissiondate like :date% and causeofadmission like '%11%'")
	Long findByOthersStateAdmissionDate(@Param("state") String state,@Param("date")  String date);

	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and admissiondate like :date% and causeofadmission like '%11%'")
	Long findByOthersDistrictAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("date")  String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and admissiondate like :date% and causeofadmission like '%11%'")
	Long findByOthersBlockAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("date") String date);


	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and admissiondate like :date% and causeofadmission like '%11%'")
	Long findByOthersFacilityTypeAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("date") String date);


	
	@Query(value=" select count(1) from GoiIcu where state = :state and district= :district and block= :block and facilityType= :facilitytype and facility= :facility and admissiondate like :date% and causeofadmission like '%11%'")
	Long findByOthersFacilityAdmissionDate(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype")String facilitytype,@Param("facility") String facility,@Param("date") String date);

	
	

}
