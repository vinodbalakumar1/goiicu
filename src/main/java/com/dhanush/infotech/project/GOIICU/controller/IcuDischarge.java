package com.dhanush.infotech.project.GOIICU.controller;

import java.util.HashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.model.GoiIcuDischarge;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class IcuDischarge {
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/icu", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> createUser(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("admissionId");
		HashMap<String, Object> status = null;
		if(id.equalsIgnoreCase(""))
		{
			status = insertDicharge(userJsonReq);
		String statu = status.get("data") == "Beneficary Added Successfull" ? "OK" :  "ERROR";
		userMap.put("status",statu);
		userMap.put("data", status.get("data"));
		userMap.put("uniqueId",status.get("unique_id"));
		userMap.put("admissionId", status.get("admissionId"));
		}
		else 
		{
	//		status = updateDicharge(userJsonReq, Long.valueOf(id));
			String statu = status.get("data") == "Beneficary Updated Successfull" ? "OK" :  "ERROR";
			userMap.put("status",statu);
			userMap.put("data", status.get("data"));
			userMap.put("uniqueId",status.get("unique_id"));
			userMap.put("admissionId", status.get("admissionId"));
		}
		return userMap;
	
	}
	
	public HashMap<String, Object> insertDicharge(String userJsonReq) {
		HashMap<String, Object> userMap = new HashMap<>();
	//	try {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String admissionId = openJson.getString("admissionId");
		org.json.JSONObject delivery = openJson.getJSONObject("discharge");
		String mctsrch_number		               =   delivery.getString("mctsrch_number");		          
		String nameoffacility		               =   delivery.getString("nameoffacility");		          
		String booked_		                       =   delivery.getString("booked_");		                  
		String obs_icu_reg_number                  =   delivery.getString("obs_icu_reg_number");             
		String block			                   =   delivery.getString("block");			              
		String bpl_status		                   =   delivery.getString("bpl_status");		              
		String district			                   =   delivery.getString("district");			              
		String bed_no			                   =   delivery.getString("bed_no");			              
		String aadhar_number	                   =   delivery.getString("aadhar_number");	              
		String contact_number	                   =   delivery.getString("contact_number");	              
		String name				                   =   delivery.getString("name");				              
		String age                                 =   delivery.getString("age");                            
		String wo_or_do                            =   delivery.getString("wo_or_do");                       
		String address                             =   delivery.getString("address");                        
		String contact_no                          =   delivery.getString("contact_no");                     
		String caste                               =   delivery.getString("caste");                          
		String religion                            =   delivery.getString("religion");                       
		String admission_date                      =   delivery.getString("admission_date");                 
		String admission_time                      =   delivery.getString("admission_time");                 
		String delivery_date                       =   delivery.getString("delivery_date");                  
		String delivery_time                       =   delivery.getString("delivery_time");                  
		String discharge_date                      =   delivery.getString("discharge_date");                 
		String discharge_time                      =   delivery.getString("discharge_time");                 
		String referred_in                         =   delivery.getString("referred_in");                    
		String if_yes_name_of_facility             =   delivery.getString("if_yes_name_of_facility");        
		String obstetric_details_at_admission      =   delivery.getString("obstetric_details_at_admission"); 
		String gravida                             =   delivery.getString("gravida");                        
		String parity                              =   delivery.getString("parity");                         
		String abortion                            =   delivery.getString("abortion");                       
		String living_children                     =   delivery.getString("living_children");                
		String lmp                                 =   delivery.getString("lmp");                            
		String edd_                                =   delivery.getString("edd_");                           
		String number_of_anc                       =   delivery.getString("number_of_anc");                  
		String gestational_age                     =   delivery.getString("gestational_age");                
		String provisional_diagnosis               =   delivery.getString("provisional_diagnosis");          
		String final_diagnosis                     =   delivery.getString("final_diagnosis");                
		String labour                              =   delivery.getString("labour");                         
		String presentation                        =   delivery.getString("presentation");                   
		String uterine_tenderness                  =   delivery.getString("uterine_tenderness");             
		String fhr                                 =   delivery.getString("fhr");                            
		String if_fhr_yes                          =   delivery.getString("if_fhr_yes");                     
		String amniotic_fluid                      =   delivery.getString("amniotic_fluid");                 
		String course_of_labour                    =   delivery.getString("course_of_labour");               
		String mode_of_delivery                    =   delivery.getString("mode_of_delivery");               
		String aph_                                =   delivery.getString("aph_");                           
		String pph_                                =   delivery.getString("pph_");                           
		String pv_foul_smelling_discharge          =   delivery.getString("pv_foul_smelling_discharge");     
		String indication_for_caesarean_section_   =   delivery.getString("indication_for_caesarean_section_");
		String delivery_attended_by                =   delivery.getString("delivery_attended_by");           
		String antenatal_steroids                  =   delivery.getString("antenatal_steroids");             
		String if_yes                              =   delivery.getString("if_yes");                         
		String no_of_doses                         =   delivery.getString("no_of_doses");                    
		String magnesium_sulphate                  =   delivery.getString("magnesium_sulphate");             
		String if_yes_dose                         =   delivery.getString("if_yes_dose");                    
		String time_of_last_doses                  =   delivery.getString("time_of_last_doses");             
		String other_drugs_                        =   delivery.getString("other_drugs_");                   
		String outcome_of_delivery                 =   delivery.getString("outcome_of_delivery");            
		String if_live_birth_weight                =   delivery.getString("if_live_birth_weight");           
		String condition_of_discharge              =   delivery.getString("condition_of_discharge");         
		String multiple_birth                      =   delivery.getString("multiple_birth");                 
		String if_yes_number                       =   delivery.getString("if_yes_number");                  
		String sex_                                =   delivery.getString("sex_");                           
		String weeks_of_gestation                  =   delivery.getString("weeks_of_gestation");             
		String resuscitation_required              =   delivery.getString("resuscitation_required");         
		String sncu_admission                      =   delivery.getString("sncu_admission");                 
		String sncu_if_yes                         =   delivery.getString("sncu_if_yes");                    
		String vitamin_k_given                     =   delivery.getString("vitamin_k_given");                
		String breastfed_within_one_hour           =   delivery.getString("breastfed_within_one_hour");      
		String breastfed_final_diagnosis           =   delivery.getString("breastfed_final_diagnosis");      
		String others                              =   delivery.getString("others");                         
		String final_outcome                       =   delivery.getString("final_outcome");                  
		String final_others                        =   delivery.getString("final_others");                   
		String treatment_given                     =   delivery.getString("treatment_given");                
		String condition_on_discharge              =   delivery.getString("condition_on_discharge");         
		String advice_on_discharge                 =   delivery.getString("advice_on_discharge");            
		String followup_date                       =   delivery.getString("followup_date");                  
		String signature_of_doctor                 =   delivery.getString("signature_of_doctor");            
		String informed_asha                       =   delivery.getString("informed_asha");      
		
		GoiIcuDischarge discharge = new GoiIcuDischarge();                                          
		discharge.setMctsrch_number		                (mctsrch_number		             );
		discharge.setNameoffacility		                (nameoffacility		             );
		discharge.setBooked_				            (booked_		                     );
		discharge.setObs_icu_reg_number	                (obs_icu_reg_number                );
		discharge.setBlock				                (block			                 );
		discharge.setBpl_status			                (bpl_status		                 );
		discharge.setDistrict			                (district			                 );
		discharge.setBed_no				                (bed_no			                 );
		discharge.setAadhar_number		                (aadhar_number	                 );
		discharge.setContact_number		                (contact_number	                 );
		discharge.setName				                (name				                 );
		discharge.setAge                                (age                               );
		discharge.setWo_or_do                           (wo_or_do                          );
		discharge.setAddress                            (address                           );
		discharge.setContact_no                         (contact_no                        );
		discharge.setCaste                              (caste                             );
		discharge.setReligion                           (religion                          );
		discharge.setAdmission_date                     (admission_date                    );
		discharge.setAdmission_time                     (admission_time                    );
		discharge.setDelivery_date                      (delivery_date                     );
		discharge.setDelivery_time                      (delivery_time                     );
		discharge.setDischarge_date                     (discharge_date                    );
		discharge.setDischarge_time                     (discharge_time                    );
		discharge.setReferred_in                        (referred_in                       );
		discharge.setIf_yes_name_of_facility            (if_yes_name_of_facility           );
		discharge.setObstetric_details_at_admission     (obstetric_details_at_admission    );
		discharge.setGravida                            (gravida                           );
		discharge.setParity                             (parity                            );
		discharge.setAbortion                           (abortion                          );
		discharge.setLiving_children                    (living_children                   );
		discharge.setLmp                                (lmp                               );
		discharge.setEdd_                               (edd_                              );
		discharge.setNumber_of_anc                      (number_of_anc                     );
		discharge.setGestational_age                    (gestational_age                   );
		discharge.setProvisional_diagnosis              (provisional_diagnosis             );
		discharge.setFinal_diagnosis                    (final_diagnosis                   );
		discharge.setLabour                             (labour                            );
		discharge.setPresentation                       (presentation                      );
		discharge.setUterine_tenderness                 (uterine_tenderness                );
		discharge.setFhr                                (fhr                               );
		discharge.setIf_fhr_yes                         (if_fhr_yes                        );
		discharge.setAmniotic_fluid                     (amniotic_fluid                    );
		discharge.setCourse_of_labour                   (course_of_labour                  );
		discharge.setMode_of_delivery                   (mode_of_delivery                  );
		discharge.setAph_                               (aph_                              );
		discharge.setPph_                               (pph_                              );
		discharge.setPv_foul_smelling_discharge         (pv_foul_smelling_discharge        );
		discharge.setIndication_for_caesarean_section_  (indication_for_caesarean_section_ );
		discharge.setDelivery_attended_by               (delivery_attended_by              );
		discharge.setAntenatal_steroids                 (antenatal_steroids                );
		discharge.setIf_yes                             (if_yes                            );
		discharge.setNo_of_doses                        (no_of_doses                       );
		discharge.setMagnesium_sulphate                 (magnesium_sulphate                );
		discharge.setIf_yes_dose                        (if_yes_dose                       );
		discharge.setTime_of_last_doses                 (time_of_last_doses                );
		discharge.setOther_drugs_                       (other_drugs_                      );
		discharge.setOutcome_of_delivery                (outcome_of_delivery               );
		discharge.setIf_live_birth_weight              (if_live_birth_weight              );
		discharge.setCondition_of_discharge             (condition_of_discharge            );
		discharge.setMultiple_birth                     (multiple_birth                    );
		discharge.setIf_yes_number                      (if_yes_number                     );
		discharge.setSex_                               (sex_                              );
		discharge.setWeeks_of_gestation                 (weeks_of_gestation                );
		discharge.setResuscitation_required             (resuscitation_required            );
		discharge.setSncu_admission                     (sncu_admission                    );
		discharge.setSncu_if_yes                        (sncu_if_yes                       );
		discharge.setVitamin_k_given                    (vitamin_k_given                   );
		discharge.setBreastfed_within_one_hour          (breastfed_within_one_hour         );
		discharge.setBreastfed_final_diagnosis          (breastfed_final_diagnosis         );
		discharge.setOthers                             (others                            );
		discharge.setFinal_outcome                      (final_outcome                     );
		discharge.setFinal_others                       (final_others                      );
		discharge.setTreatment_given                    (treatment_given                   );
		 discharge.setCondition_on_discharge            (condition_on_discharge            );
		discharge.setAdvice_on_discharge                (advice_on_discharge               );
		discharge.setFollowup_date                      (followup_date                     );
		discharge.setSignature_of_doctor                (signature_of_doctor               );
		discharge.setInformed_asha                      (informed_asha                     );
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	return null;
	}
}
