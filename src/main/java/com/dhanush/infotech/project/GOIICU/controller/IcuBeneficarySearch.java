package com.dhanush.infotech.project.GOIICU.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIICU.model.GoiIcu;
import com.dhanush.infotech.project.GOIICU.repository.IcuRepository;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class IcuBeneficarySearch {
	
	@Autowired
	IcuRepository icuRepository;
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/getbeneficiaryreport", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Object> getChildIdByUniqueID(@RequestBody String userJsonReq) {
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		Map<String, Object> error = new HashMap<>(); 
		Map<String, Object> data = new HashMap<>();
		List<Map<String,Object>> arr = new ArrayList<>();
		try {
			int i = 0;
			i+=  Integer.parseInt(openJson.getString("state_code").length() > 0 ? "1" : "0");
			i+=   Integer.parseInt(!openJson.getString("district_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
			i+=  Integer.parseInt(!openJson.getString("block_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
			i+=   Integer.parseInt(!openJson.getString("facility_type_id").toString().equalsIgnoreCase("ALL") ? "1" :"0");
			i+=   Integer.parseInt(!openJson.getString("facility_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
			
	 	String year=openJson.getString("input_year").equalsIgnoreCase("ALL") ?"%" :openJson.getString("input_year")+"-" ;
	 	String month=openJson.getString("input_month").equalsIgnoreCase("ALL")?"%":openJson.getString("input_month").length()==1 ? "0"+openJson.getString("input_month")+"%" :openJson.getString("input_month") +"%";
	 	String date=year+""+month;
		List<GoiIcu> admission = null;
	 	switch(i) {
	 	case 1 :
		 	admission = icuRepository.findByStateAndAdmissiondateLike(openJson.getString("state_code"),date);
		 	break;
	 	case 2 :
	 		admission=icuRepository.findByStateAndDistrictAndAdmissiondateLike(openJson.getString("state_code"),openJson.getString("district_code"),date);
	 	break;
	 	case 3 :
	 		admission=icuRepository.findByStateAndDistrictAndBlockAndAdmissiondateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),date);
	 	break;
	 	case 4 :
	 		admission=icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndAdmissiondateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),date);
	 	break;
	 	case 5 :
	 		admission=icuRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndAdmissiondateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),openJson.getString("facility_code"),date);
	 	break;
	 	default:
	 		break;
	 	}
		for(GoiIcu admission1 : admission)
		{
			Map<String , Object> map  = new HashMap<>();
			map.put("case_no", admission1.getUniqueid());
			map.put("admDate", admission1.getAdmissiondate());
			map.put("beneficiary_name", admission1.getName());
			map.put("mobile_no", admission1.getPatientcontactno());
			map.put("mcts_no", admission1.getMctsrchnumber());
			arr.add(map);
		}
		data.put("message", arr.size()+" Records Found");
		data.put("result", arr);
		data.put("total", arr.size());
		data.put("success",true);
		data.put("error", error);
		}
		catch (Exception e) {
			error.put("error", e.getMessage());
			data.put("message", arr.size()+" Records Found");
			data.put("result", arr);
			data.put("total", arr.size());
			data.put("success",true);
			data.put("error", error);
		}
		return data;
	}
}
