package com.dhanush.infotech.project.GOIICU.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="mprdataentry")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt" , "updatedAt" }, allowGetters = false)
public class MPRDataEntryModel implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String inpatients;
	private String referredin;
	private String total;
	private String aph;
	private String pph;
	private String sepsis;
	private String eclampsia_sse;
	private String abortion;
	private String obstructedlabour;
	private String reptureduterus;
	private String severeanemia;
	private String cardiacdiseases;
	private String jaundice;
	private String others;
	private String shiftedtotheword;
	private String referredout;
	private String lama;
	private String death;
	private String readmission;
	private String bedoccupancyrate;
	private String averagelengthofstay;
	@Column(nullable = false, updatable = false) 
	@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
	@Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInpatients() {
		return inpatients;
	}
	public void setInpatients(String inpatients) {
		this.inpatients = inpatients;
	}
	public String getReferredin() {
		return referredin;
	}
	public void setReferredin(String referredin) {
		this.referredin = referredin;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getAph() {
		return aph;
	}
	public void setAph(String aph) {
		this.aph = aph;
	}
	public String getPph() {
		return pph;
	}
	public void setPph(String pph) {
		this.pph = pph;
	}
	public String getSepsis() {
		return sepsis;
	}
	public void setSepsis(String sepsis) {
		this.sepsis = sepsis;
	}
	public String getEclampsia_sse() {
		return eclampsia_sse;
	}
	public void setEclampsia_sse(String eclampsia_sse) {
		this.eclampsia_sse = eclampsia_sse;
	}
	public String getAbortion() {
		return abortion;
	}
	public void setAbortion(String abortion) {
		this.abortion = abortion;
	}
	public String getObstructedlabour() {
		return obstructedlabour;
	}
	public void setObstructedlabour(String obstructedlabour) {
		this.obstructedlabour = obstructedlabour;
	}
	public String getReptureduterus() {
		return reptureduterus;
	}
	public void setReptureduterus(String reptureduterus) {
		this.reptureduterus = reptureduterus;
	}
	public String getSevereanemia() {
		return severeanemia;
	}
	public void setSevereanemia(String severeanemia) {
		this.severeanemia = severeanemia;
	}
	public String getCardiacdiseases() {
		return cardiacdiseases;
	}
	public void setCardiacdiseases(String cardiacdiseases) {
		this.cardiacdiseases = cardiacdiseases;
	}
	public String getJaundice() {
		return jaundice;
	}
	public void setJaundice(String jaundice) {
		this.jaundice = jaundice;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public String getShiftedtotheword() {
		return shiftedtotheword;
	}
	public void setShiftedtotheword(String shiftedtotheword) {
		this.shiftedtotheword = shiftedtotheword;
	}
	public String getReferredout() {
		return referredout;
	}
	public void setReferredout(String referredout) {
		this.referredout = referredout;
	}
	public String getLama() {
		return lama;
	}
	public void setLama(String lama) {
		this.lama = lama;
	}
	public String getDeath() {
		return death;
	}
	public void setDeath(String death) {
		this.death = death;
	}
	public String getReadmission() {
		return readmission;
	}
	public void setReadmission(String readmission) {
		this.readmission = readmission;
	}
	public String getBedoccupancyrate() {
		return bedoccupancyrate;
	}
	public void setBedoccupancyrate(String bedoccupancyrate) {
		this.bedoccupancyrate = bedoccupancyrate;
	}
	public String getAveragelengthofstay() {
		return averagelengthofstay;
	}
	public void setAveragelengthofstay(String averagelengthofstay) {
		this.averagelengthofstay = averagelengthofstay;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



}
