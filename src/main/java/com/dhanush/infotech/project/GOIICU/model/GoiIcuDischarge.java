package com.dhanush.infotech.project.GOIICU.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "goi_icu_discharge")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt" , "updatedAt" }, allowGetters = false)
public class GoiIcuDischarge {
	
	
	  @OneToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
	    @JoinColumn(name = "admissionId", nullable = false)
	    private GoiIcu admission; 

	   public String mctsrch_number		;
	   public String nameoffacility		;
	   public String booked_			;	
	   public String obs_icu_reg_number	;
	   public String block				;
	   public String bpl_status			;
	   public String district			;
	   public String bed_no				;
	   public String aadhar_number		;
	   public String contact_number		;
	   public String name				;
	   public String age                ; 
	   public String wo_or_do           ; 
	   public String address            ; 
	   public String contact_no         ; 
	   public String caste              ;
	   public String religion           ;
	   public String admission_date     ;
	   public String admission_time  ;
	   public String delivery_date   ;
	   public String delivery_time   ;
	   public String discharge_date  ;
	   public String discharge_time  ;
	  public  String  referred_in                       ;
	  public  String  if_yes_name_of_facility           ;
	  public  String  obstetric_details_at_admission    ;
	  public  String  gravida                           ;
	  public  String  parity                            ;
	  public  String  abortion                          ;
	  public  String  living_children                   ;
	  public  String  lmp                               ;
	  public  String  edd_                              ;
	  public  String  number_of_anc                     ;
	  public  String  gestational_age                   ;
	  public  String  provisional_diagnosis             ;
	  public  String  final_diagnosis                   ;
	  public  String  labour                            ;
	  public  String  presentation                      ;
	  public  String  uterine_tenderness                ;
	  public  String  fhr                               ;
	  public  String  if_fhr_yes                        ;
	  public  String  amniotic_fluid                    ;
	  public  String  course_of_labour                  ;
	  public  String  mode_of_delivery                  ;
	  public  String  aph_                              ;
	  public  String  pph_                              ;
	  public  String  pv_foul_smelling_discharge        ;
	  public  String  indication_for_caesarean_section_ ;
	  public  String  delivery_attended_by              ;
	  public  String  antenatal_steroids                ;
	  public  String  if_yes                            ;
	  public  String  no_of_doses                       ;
	  public  String  magnesium_sulphate                ;
	  public  String  if_yes_dose                       ;
	  public  String  time_of_last_doses                ;
	  public  String  other_drugs_                      ;
	  public  String  outcome_of_delivery               ;
	  public  String  if_live_birth_weight              ;
	  public  String  condition_of_discharge            ;
	  public  String  multiple_birth                    ;
	  public  String  if_yes_number                     ;
	  public  String  sex_                              ;
	  public  String  weeks_of_gestation                ;
	  public  String  resuscitation_required            ;
	  public  String  sncu_admission                    ;
	  public  String  sncu_if_yes                       ;
	  public  String  vitamin_k_given                   ;
	  public  String  breastfed_within_one_hour         ;
	  public  String  breastfed_final_diagnosis         ;
	  public  String  others                            ;
	  public  String  final_outcome                     ;
	  public  String  final_others                      ;
	  public  String  treatment_given                   ;
	  public  String  condition_on_discharge            ;
	  public  String  advice_on_discharge               ;
	  public  String  followup_date                     ;
	  public  String  signature_of_doctor               ;
	  public  String  informed_asha                     ;
	public GoiIcu getAdmission() {
		return admission;
	}
	public void setAdmission(GoiIcu admission) {
		this.admission = admission;
	}
	public String getMctsrch_number() {
		return mctsrch_number;
	}
	public void setMctsrch_number(String mctsrch_number) {
		this.mctsrch_number = mctsrch_number;
	}
	public String getNameoffacility() {
		return nameoffacility;
	}
	public void setNameoffacility(String nameoffacility) {
		this.nameoffacility = nameoffacility;
	}
	public String getBooked_() {
		return booked_;
	}
	public void setBooked_(String booked_) {
		this.booked_ = booked_;
	}
	public String getObs_icu_reg_number() {
		return obs_icu_reg_number;
	}
	public void setObs_icu_reg_number(String obs_icu_reg_number) {
		this.obs_icu_reg_number = obs_icu_reg_number;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getBpl_status() {
		return bpl_status;
	}
	public void setBpl_status(String bpl_status) {
		this.bpl_status = bpl_status;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBed_no() {
		return bed_no;
	}
	public void setBed_no(String bed_no) {
		this.bed_no = bed_no;
	}
	public String getAadhar_number() {
		return aadhar_number;
	}
	public void setAadhar_number(String aadhar_number) {
		this.aadhar_number = aadhar_number;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getWo_or_do() {
		return wo_or_do;
	}
	public void setWo_or_do(String wo_or_do) {
		this.wo_or_do = wo_or_do;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getAdmission_date() {
		return admission_date;
	}
	public void setAdmission_date(String admission_date) {
		this.admission_date = admission_date;
	}
	public String getAdmission_time() {
		return admission_time;
	}
	public void setAdmission_time(String admission_time) {
		this.admission_time = admission_time;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	public String getDelivery_time() {
		return delivery_time;
	}
	public void setDelivery_time(String delivery_time) {
		this.delivery_time = delivery_time;
	}
	public String getDischarge_date() {
		return discharge_date;
	}
	public void setDischarge_date(String discharge_date) {
		this.discharge_date = discharge_date;
	}
	public String getDischarge_time() {
		return discharge_time;
	}
	public void setDischarge_time(String discharge_time) {
		this.discharge_time = discharge_time;
	}
	public String getReferred_in() {
		return referred_in;
	}
	public void setReferred_in(String referred_in) {
		this.referred_in = referred_in;
	}
	public String getIf_yes_name_of_facility() {
		return if_yes_name_of_facility;
	}
	public void setIf_yes_name_of_facility(String if_yes_name_of_facility) {
		this.if_yes_name_of_facility = if_yes_name_of_facility;
	}
	public String getObstetric_details_at_admission() {
		return obstetric_details_at_admission;
	}
	public void setObstetric_details_at_admission(String obstetric_details_at_admission) {
		this.obstetric_details_at_admission = obstetric_details_at_admission;
	}
	public String getGravida() {
		return gravida;
	}
	public void setGravida(String gravida) {
		this.gravida = gravida;
	}
	public String getParity() {
		return parity;
	}
	public void setParity(String parity) {
		this.parity = parity;
	}
	public String getAbortion() {
		return abortion;
	}
	public void setAbortion(String abortion) {
		this.abortion = abortion;
	}
	public String getLiving_children() {
		return living_children;
	}
	public void setLiving_children(String living_children) {
		this.living_children = living_children;
	}
	public String getLmp() {
		return lmp;
	}
	public void setLmp(String lmp) {
		this.lmp = lmp;
	}
	public String getEdd_() {
		return edd_;
	}
	public void setEdd_(String edd_) {
		this.edd_ = edd_;
	}
	public String getNumber_of_anc() {
		return number_of_anc;
	}
	public void setNumber_of_anc(String number_of_anc) {
		this.number_of_anc = number_of_anc;
	}
	public String getGestational_age() {
		return gestational_age;
	}
	public void setGestational_age(String gestational_age) {
		this.gestational_age = gestational_age;
	}
	public String getProvisional_diagnosis() {
		return provisional_diagnosis;
	}
	public void setProvisional_diagnosis(String provisional_diagnosis) {
		this.provisional_diagnosis = provisional_diagnosis;
	}
	public String getFinal_diagnosis() {
		return final_diagnosis;
	}
	public void setFinal_diagnosis(String final_diagnosis) {
		this.final_diagnosis = final_diagnosis;
	}
	public String getLabour() {
		return labour;
	}
	public void setLabour(String labour) {
		this.labour = labour;
	}
	public String getPresentation() {
		return presentation;
	}
	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}
	public String getUterine_tenderness() {
		return uterine_tenderness;
	}
	public void setUterine_tenderness(String uterine_tenderness) {
		this.uterine_tenderness = uterine_tenderness;
	}
	public String getFhr() {
		return fhr;
	}
	public void setFhr(String fhr) {
		this.fhr = fhr;
	}
	public String getIf_fhr_yes() {
		return if_fhr_yes;
	}
	public void setIf_fhr_yes(String if_fhr_yes) {
		this.if_fhr_yes = if_fhr_yes;
	}
	public String getAmniotic_fluid() {
		return amniotic_fluid;
	}
	public void setAmniotic_fluid(String amniotic_fluid) {
		this.amniotic_fluid = amniotic_fluid;
	}
	public String getCourse_of_labour() {
		return course_of_labour;
	}
	public void setCourse_of_labour(String course_of_labour) {
		this.course_of_labour = course_of_labour;
	}
	public String getMode_of_delivery() {
		return mode_of_delivery;
	}
	public void setMode_of_delivery(String mode_of_delivery) {
		this.mode_of_delivery = mode_of_delivery;
	}
	public String getAph_() {
		return aph_;
	}
	public void setAph_(String aph_) {
		this.aph_ = aph_;
	}
	public String getPph_() {
		return pph_;
	}
	public void setPph_(String pph_) {
		this.pph_ = pph_;
	}
	public String getPv_foul_smelling_discharge() {
		return pv_foul_smelling_discharge;
	}
	public void setPv_foul_smelling_discharge(String pv_foul_smelling_discharge) {
		this.pv_foul_smelling_discharge = pv_foul_smelling_discharge;
	}
	public String getIndication_for_caesarean_section_() {
		return indication_for_caesarean_section_;
	}
	public void setIndication_for_caesarean_section_(String indication_for_caesarean_section_) {
		this.indication_for_caesarean_section_ = indication_for_caesarean_section_;
	}
	public String getDelivery_attended_by() {
		return delivery_attended_by;
	}
	public void setDelivery_attended_by(String delivery_attended_by) {
		this.delivery_attended_by = delivery_attended_by;
	}
	public String getAntenatal_steroids() {
		return antenatal_steroids;
	}
	public void setAntenatal_steroids(String antenatal_steroids) {
		this.antenatal_steroids = antenatal_steroids;
	}
	public String getIf_yes() {
		return if_yes;
	}
	public void setIf_yes(String if_yes) {
		this.if_yes = if_yes;
	}
	public String getNo_of_doses() {
		return no_of_doses;
	}
	public void setNo_of_doses(String no_of_doses) {
		this.no_of_doses = no_of_doses;
	}
	public String getMagnesium_sulphate() {
		return magnesium_sulphate;
	}
	public void setMagnesium_sulphate(String magnesium_sulphate) {
		this.magnesium_sulphate = magnesium_sulphate;
	}
	public String getIf_yes_dose() {
		return if_yes_dose;
	}
	public void setIf_yes_dose(String if_yes_dose) {
		this.if_yes_dose = if_yes_dose;
	}
	public String getTime_of_last_doses() {
		return time_of_last_doses;
	}
	public void setTime_of_last_doses(String time_of_last_doses) {
		this.time_of_last_doses = time_of_last_doses;
	}
	public String getOther_drugs_() {
		return other_drugs_;
	}
	public void setOther_drugs_(String other_drugs_) {
		this.other_drugs_ = other_drugs_;
	}
	public String getOutcome_of_delivery() {
		return outcome_of_delivery;
	}
	public void setOutcome_of_delivery(String outcome_of_delivery) {
		this.outcome_of_delivery = outcome_of_delivery;
	}
	public String getIf_live_birth_weight() {
		return if_live_birth_weight;
	}
	public void setIf_live_birth_weight(String if_live_birth_weight) {
		this.if_live_birth_weight = if_live_birth_weight;
	}
	public String getCondition_of_discharge() {
		return condition_of_discharge;
	}
	public void setCondition_of_discharge(String condition_of_discharge) {
		this.condition_of_discharge = condition_of_discharge;
	}
	public String getMultiple_birth() {
		return multiple_birth;
	}
	public void setMultiple_birth(String multiple_birth) {
		this.multiple_birth = multiple_birth;
	}
	public String getIf_yes_number() {
		return if_yes_number;
	}
	public void setIf_yes_number(String if_yes_number) {
		this.if_yes_number = if_yes_number;
	}
	public String getSex_() {
		return sex_;
	}
	public void setSex_(String sex_) {
		this.sex_ = sex_;
	}
	public String getWeeks_of_gestation() {
		return weeks_of_gestation;
	}
	public void setWeeks_of_gestation(String weeks_of_gestation) {
		this.weeks_of_gestation = weeks_of_gestation;
	}
	public String getResuscitation_required() {
		return resuscitation_required;
	}
	public void setResuscitation_required(String resuscitation_required) {
		this.resuscitation_required = resuscitation_required;
	}
	public String getSncu_admission() {
		return sncu_admission;
	}
	public void setSncu_admission(String sncu_admission) {
		this.sncu_admission = sncu_admission;
	}
	public String getSncu_if_yes() {
		return sncu_if_yes;
	}
	public void setSncu_if_yes(String sncu_if_yes) {
		this.sncu_if_yes = sncu_if_yes;
	}
	public String getVitamin_k_given() {
		return vitamin_k_given;
	}
	public void setVitamin_k_given(String vitamin_k_given) {
		this.vitamin_k_given = vitamin_k_given;
	}
	public String getBreastfed_within_one_hour() {
		return breastfed_within_one_hour;
	}
	public void setBreastfed_within_one_hour(String breastfed_within_one_hour) {
		this.breastfed_within_one_hour = breastfed_within_one_hour;
	}
	public String getBreastfed_final_diagnosis() {
		return breastfed_final_diagnosis;
	}
	public void setBreastfed_final_diagnosis(String breastfed_final_diagnosis) {
		this.breastfed_final_diagnosis = breastfed_final_diagnosis;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public String getFinal_outcome() {
		return final_outcome;
	}
	public void setFinal_outcome(String final_outcome) {
		this.final_outcome = final_outcome;
	}
	public String getFinal_others() {
		return final_others;
	}
	public void setFinal_others(String final_others) {
		this.final_others = final_others;
	}
	public String getTreatment_given() {
		return treatment_given;
	}
	public void setTreatment_given(String treatment_given) {
		this.treatment_given = treatment_given;
	}
	public String getCondition_on_discharge() {
		return condition_on_discharge;
	}
	public void setCondition_on_discharge(String condition_on_discharge) {
		this.condition_on_discharge = condition_on_discharge;
	}
	public String getAdvice_on_discharge() {
		return advice_on_discharge;
	}
	public void setAdvice_on_discharge(String advice_on_discharge) {
		this.advice_on_discharge = advice_on_discharge;
	}
	public String getFollowup_date() {
		return followup_date;
	}
	public void setFollowup_date(String followup_date) {
		this.followup_date = followup_date;
	}
	public String getSignature_of_doctor() {
		return signature_of_doctor;
	}
	public void setSignature_of_doctor(String signature_of_doctor) {
		this.signature_of_doctor = signature_of_doctor;
	}
	public String getInformed_asha() {
		return informed_asha;
	}
	public void setInformed_asha(String informed_asha) {
		this.informed_asha = informed_asha;
	}

	  
	  
	  
}
